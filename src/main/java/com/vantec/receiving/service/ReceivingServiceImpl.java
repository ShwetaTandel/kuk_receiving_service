package com.vantec.receiving.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.receiving.business.RDTInfo;
import com.vantec.receiving.business.ReceiptInfo;
import com.vantec.receiving.dao.ReceivingDAO;
import com.vantec.receiving.entity.DocumentStatus;
import com.vantec.receiving.entity.InventoryMaster;
import com.vantec.receiving.entity.Part;
import com.vantec.receiving.entity.ReceiptBody;
import com.vantec.receiving.entity.ReceiptDetail;
import com.vantec.receiving.entity.ReceiptHeader;
import com.vantec.receiving.entity.RelocHeader;
import com.vantec.receiving.entity.RtsBody;
import com.vantec.receiving.entity.RtsDetail;
import com.vantec.receiving.entity.RtsHeader;
import com.vantec.receiving.entity.Vendor;
import com.vantec.receiving.exception.VantecReceivingException;
import com.vantec.receiving.model.ChangePnoDTO;
import com.vantec.receiving.model.DeleteDocumentDetailRequest;
import com.vantec.receiving.model.HaulierReferenceDTO;
import com.vantec.receiving.model.ReceiptBodyDTO;
import com.vantec.receiving.model.ReceiptHeaderDTO;
import com.vantec.receiving.model.RelocTaskDTO;
import com.vantec.receiving.model.RtsBodyDTO;
import com.vantec.receiving.model.RtsDetailDTO;
import com.vantec.receiving.model.RtsHeaderDTO;
import com.vantec.receiving.model.TransactionDetailRequest;
import com.vantec.receiving.model.UnplannedRTSDTO;
import com.vantec.receiving.model.VendorDTO;
import com.vantec.receiving.repository.ChargeRepository;
import com.vantec.receiving.repository.DocumentStatusRepository;
import com.vantec.receiving.repository.InventoryMasterRepository;
import com.vantec.receiving.repository.InventoryStatusRepository;
import com.vantec.receiving.repository.LocationRepository;
import com.vantec.receiving.repository.PartRepository;
import com.vantec.receiving.repository.ReceiptBodyRepository;
import com.vantec.receiving.repository.ReceiptDetailRepository;
import com.vantec.receiving.repository.ReceiptHeaderRepository;
import com.vantec.receiving.repository.RtsBodyRepository;
import com.vantec.receiving.repository.RtsDetailRepository;
import com.vantec.receiving.repository.RtsHeaderRepository;
import com.vantec.receiving.repository.TransactionHistoryRepository;
import com.vantec.receiving.repository.VendorRepository;
import com.vantec.receiving.util.ConstantHelper;
import com.vantec.receiving.util.RecievingServiceHelper;

@Service("receivingService")
public class ReceivingServiceImpl implements ReceivingService {

	private static Logger logger = LoggerFactory.getLogger(ReceivingServiceImpl.class);

	@Autowired
	private ReceivingDAO receivingDAO;

	@Autowired
	private ReceiptDetailRepository receiptDetailRepository;

	@Autowired
	private ReceiptBodyRepository receiptBodyRepository;

	@Autowired
	private ReceiptHeaderRepository receiptHeaderRepository;

	@Autowired
	private VendorRepository vendorRepository;

	@Autowired
	private PartRepository partRepository;

	@Autowired
	private DocumentStatusRepository documentStatusRepository;

	@Autowired
	private ChargeRepository chargeRepository;

	@Autowired
	InventoryMasterRepository inventoryMasterRepository;

	@Autowired
	RDTInfo rdtInfo;

	@Autowired
	ReceiptInfo receiptInfo;

	@Autowired
	ReceiptHeaderRepository receiptHeaderRepositiory;

	@Autowired
	RtsHeaderRepository rtsHeaderRepo;

	@Autowired
	RtsDetailRepository rtsDetailRepo;

	@Autowired
	RtsBodyRepository rtsBodyRepo;

	@Autowired
	TransactionHistoryRepository transHistRepo;

	@Autowired
	LocationRepository locRepo;

	@Autowired
	InventoryStatusRepository invStatusRepo;
	

	@Transactional
	public boolean saveRecievingRequest(ReceiptHeader receivingRequests) {
		return false;
		// ReceiptHeader receiptHeader =
		// RecievingServiceHelper.convertViewToEntity(receivingRequests);
		// boolean requestCompleted = receivingDAO.createRequest(receiptHeader);
		// return requestCompleted;
	}

	@Transactional
	public List<ReceiptHeader> getAllReceiptDocs(int pageNumber, int size) {
		List<ReceiptHeader> receipts = receivingDAO.getAllReceiptDocs(pageNumber, size);
		return receipts;
	}

	@Transactional
	@Override
	public boolean saveRecievingRequest(ReceiptHeaderDTO receivingRequests) {
		ReceiptHeader receiptHeader = RecievingServiceHelper.convertViewToEntity(receivingRequests);
		boolean requestCompleted = receivingDAO.createRequest(receiptHeader);
		return requestCompleted;
	}

	@Transactional
	@Override
	public boolean createVendor(VendorDTO vendorDTO) throws ParseException {
		vendorDTO.setStorageCharge(chargeRepository.findById(vendorDTO.getStorageChargeId()));
		vendorDTO.setReceiptCharge(chargeRepository.findById(vendorDTO.getReceiptChargeId()));
		vendorDTO.setDespatchCharge(chargeRepository.findById(vendorDTO.getDespatchChargeId()));
		vendorDTO.setDecantCharge(chargeRepository.findById(vendorDTO.getDecantChargeId()));
		vendorDTO.setInspectCharge(chargeRepository.findById(vendorDTO.getInspectChargeId()));
		vendorDTO.setTransportCharge(chargeRepository.findById(vendorDTO.getTransportChargeId()));
		Vendor vendor = RecievingServiceHelper.convertVendorViewToEntity(vendorDTO, null);
		boolean requestCompleted = receivingDAO.addVendor(vendor);
		return requestCompleted;
	}

	@Transactional
	@Override
	public Boolean editVendor(VendorDTO vendorDTO) throws ParseException {
		vendorDTO.setStorageCharge(chargeRepository.findById(vendorDTO.getStorageChargeId()));
		vendorDTO.setReceiptCharge(chargeRepository.findById(vendorDTO.getReceiptChargeId()));
		vendorDTO.setDespatchCharge(chargeRepository.findById(vendorDTO.getDespatchChargeId()));
		vendorDTO.setDecantCharge(chargeRepository.findById(vendorDTO.getDecantChargeId()));
		vendorDTO.setInspectCharge(chargeRepository.findById(vendorDTO.getInspectChargeId()));
		vendorDTO.setTransportCharge(chargeRepository.findById(vendorDTO.getTransportChargeId()));
		Vendor vendor = vendorRepository.findByVendorReferenceCode(vendorDTO.getVendorCode());
		vendor = RecievingServiceHelper.convertVendorViewToEntity(vendorDTO, vendor);
		boolean requestCompleted = receivingDAO.addVendor(vendor);
		return requestCompleted;
	}

	@Transactional
	@Override
	public ReceiptHeader createReceipt(ReceiptHeaderDTO receiptHeaderDTO) {

		List<ReceiptBodyDTO> receiptBodyDTOs = receiptHeaderDTO.getReceiptBodyDTO();

		ReceiptHeader receiptHeader = receiptInfo.createReceiptHeader(receiptHeaderDTO);
		boolean bodiesAdded = false;
		for (ReceiptBodyDTO receiptBodyDTO : receiptBodyDTOs) {
			ReceiptBody receiptBody  = receiptInfo.createReceiptBody(receiptHeader, receiptBodyDTO);
			if (receiptBody != null) {
				bodiesAdded = true;
				Vendor vendor = vendorRepository.findByVendorReferenceCode(receiptBody.getVendorCode());
				if (vendor != null && vendor.getOffsiteStorage() != null && vendor.getOffsiteStorage() == true) {
					logger.info("Offsite storage case for GRN-" + receiptHeaderDTO.getGrnNumber()
							+ " and vendor ref code " + vendor.getVendorReferenceCode());
					receiptInfo.createDetailandInventoryForOffsiteStorage(receiptBody, vendor);
				}
			}
		}
		if(!bodiesAdded){
			logger.info("Deleting receipt - No Bodies added for the - "+receiptHeaderDTO.getGrnNumber());
			receiptHeaderRepositiory.delete(receiptHeader);
			return null;
			
		}
		Integer openBodiesCnt = receiptBodyRepository.findCountOfAllBodiesWithStatus(receiptHeader.getDocumentReference(),documentStatusRepository.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_OPEN));
		if(openBodiesCnt == 0){
			logger.info("Marking receipt Completed for GRN-"+receiptHeaderDTO.getGrnNumber());
			receiptHeader.setDocumentStatus(documentStatusRepository.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_COMPLETED));
			receiptHeaderRepository.save(receiptHeader);
		}
		Integer qBodiesCnt = receiptBodyRepository.findCountOfQuarantinedBodies(receiptHeader.getDocumentReference());
		Integer bodiesCnt = receiptBodyRepository.findCountOfAllBodies(receiptHeader.getDocumentReference());
		if (bodiesCnt.intValue() == qBodiesCnt.intValue()) {
			// Mark the header quarantine
			logger.info("Marking receipt Quarantine for GRN-"+receiptHeaderDTO.getGrnNumber());
			receiptHeader.setDocumentStatus(
					documentStatusRepository.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_QUARANTINE));
			receiptHeaderRepository.save(receiptHeader);
		}

		return receiptHeader;
	}
	
	@Transactional
	@Override
	public List<String> createRelocTask(RelocTaskDTO relocTaskDTO) throws IOException, VantecReceivingException{

		List<String> parts = null;
		RelocHeader relocHeader = receiptInfo.createRelocHeader(relocTaskDTO);
		if(relocHeader != null){
			logger.info("RELOC tasks created for"+relocTaskDTO.getGrnReference());
			relocHeader = receiptInfo.createRelocBody(relocHeader);
			//Process the task of its Quarantine to store
			if (relocHeader.getToLocation().equalsIgnoreCase(ConstantHelper.CUSTOMER_lOCATION_AREA_STOCK)) {
				logger.info("Quarantine to store -> processing the task - "+relocTaskDTO.getGrnReference());
				parts = receiptInfo.processRelocTasks(relocHeader);
				}
		}else{
			logger.error("GRN Not found for this RELOC tasks "+relocTaskDTO.getGrnReference());
			throw new VantecReceivingException("GRN Missing", new Error());
		}
	
		
		return parts;
	}
	
	
	@Transactional
	@Override
	public boolean processChangePartNo(ChangePnoDTO changePno) {

		ReceiptHeader header  = receiptHeaderRepositiory.findByCustomerReference(changePno.getGrnReference());
		if (header != null && header.getDocumentStatus().getDocumentStatusCode()
				.equalsIgnoreCase(ConstantHelper.DOCUMENT_STATUS_OPEN)) {
			
			ReceiptBody oldBody = receiptBodyRepository.findByReceiptHeaderAndLineNo(header, changePno.getLineNo());
			oldBody.setDocumentStatus(documentStatusRepository.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_DELETED));
			oldBody.setUpdatedBy(ConstantHelper.CHGPNO_FILE_TYPE);
			oldBody.setDateUpdated(new Date());
			receiptBodyRepository.save(oldBody);
			
			
			ReceiptBodyDTO bodydto = new ReceiptBodyDTO();
			bodydto.setSequenceNumber(changePno.getNewLineNo());
			bodydto.setQuantityAdvised(oldBody.getAdvisedQty()+"");
			bodydto.setQtyToQuarantine(oldBody.getQuarantinedQty()+"");
			bodydto.setQtyAccepted(oldBody.getQtyAccepted()+"");
			bodydto.setPartNumber(changePno.getNewPartNumber());
			String inspectFlag = oldBody.getInspect() == true ? "Y" : "N";
			bodydto.setInspectFlag(inspectFlag);
			String reqCount = oldBody.getCountRequiredflag() == true ? "Y" : "N";
			bodydto.setRequiredCountFlag(reqCount);
			ReceiptBody receiptBody = receiptInfo.createReceiptBody(header, bodydto);
			if(receiptBody != null){
				logger.info("Receipt Body Added for Change PNO Id is "+receiptBody.getId());
			}
			
			
		}else{
			if(header == null){
				logger.info("GRN not found. Ignoring file ");
			}else{
				logger.info("Header is processed ");
			}
			return false;
		}
		
		return true;
	}

	@Transactional
	@Override
	public RtsHeader createRtsTask(RtsHeaderDTO rtsHeaderDTO) {

		List<RtsBodyDTO> rtsBodyDTOs = rtsHeaderDTO.getRtsBodies();
		RtsHeader rtsHeader = receiptInfo.createRtsHeader(rtsHeaderDTO);
		boolean isExpedite=false, isExpEmerg=false, isEmerg = false;
		for (RtsBodyDTO rtsBodyDTO : rtsBodyDTOs) {
			RtsBody rtsBody = receiptInfo.createRtsBody(rtsHeader, rtsBodyDTO);
			if (!rtsHeader.getOrderType().equalsIgnoreCase(ConstantHelper.FAB_RTS)
					&& !rtsHeader.getOrderType().equalsIgnoreCase(ConstantHelper.UNPLANNED_RTS)) {
				//This logic was to check if pick exists initially // sarah told me to change this to checking if order exists one fine afternoon of 21 July 2021
				if (rtsBody!=null && receiptInfo.checkOrderExistsForCustomerReferenceAndPart(rtsHeaderDTO.getOrderReference(),rtsBody.getPartNumber())) {
					isExpedite = true;
					if (receiptInfo.checkAnyShortagesForPart(rtsBody.getPartNumber())) {
						isExpEmerg = true;
					}
				} else if (rtsBody!=null &&  receiptInfo.checkAnyShortagesForPart(rtsBody.getPartNumber())) {
					isEmerg = true;
				}
			}
			if (rtsBodyDTO.getRtsDetails() != null && rtsBodyDTO.getRtsDetails().size() > 0) {
				for (RtsDetailDTO rtsdetailDTO : rtsBodyDTO.getRtsDetails()) {
					int cntDetail = 1;
					if (rtsBody!=null &&  rtsBody.getPart().getIsBigPart()) {
						cntDetail = rtsdetailDTO.getReceivedQty().intValue();
					}
					for (int i = 0; i < cntDetail; i++) {
						if (rtsBody!=null && rtsBody.getPart().getIsBigPart()) {
							rtsdetailDTO.setReceivedQty((double)1);
						}
						if(rtsBody!=null ){
							RtsDetail savedDrt = receiptInfo.createRtsDetail(rtsBody, rtsdetailDTO);
							rtsBody.getRtsDetails().add(savedDrt);
						}
					}

				}
				if(rtsBody!=null ){
					rtsHeader.getRtsBodies().add(rtsBody);
				}
			}
		}
		if (!rtsHeader.getOrderType().equalsIgnoreCase(ConstantHelper.FAB_RTS)
				&& !rtsHeader.getOrderType().equalsIgnoreCase(ConstantHelper.UNPLANNED_RTS)) {
			if (isExpEmerg ) {
				rtsHeader.setOrderType(ConstantHelper.EXPEDITE_EMERG_RTS);
			}else if (isExpedite){
				rtsHeader.setOrderType(ConstantHelper.EXPEDITE_RTS);
			}else if(isEmerg){
				rtsHeader.setOrderType(ConstantHelper.REWORK_EMERG_RTS);
			}else{
				rtsHeader.setOrderType(ConstantHelper.REWORK_RTS);
			}
		}
		RtsHeader savedRTSHeader = rtsHeaderRepo.save(rtsHeader);
		return savedRTSHeader;
	}

	@Transactional
	@Override
	public void deleteReceipt(String grnNumber) {
		logger.info("grn is -->" + grnNumber);
		ReceiptHeader receiptHeader = receiptHeaderRepository.findByCustomerReference(grnNumber);
		DocumentStatus documentStatus = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_DELETED);
		if (receiptHeader != null) {
			List<ReceiptBody> receiptBodies = receiptBodyRepository.findByReceiptHeader(receiptHeader);
			for (ReceiptBody receiptBody : receiptBodies) {
				if (receiptBody != null) {
					List<ReceiptDetail> receiptDetails = receiptDetailRepository.findByReceiptBody(receiptBody);
					for (ReceiptDetail receiptDetail : receiptDetails) {
						if (receiptDetail.getSerialReference() != null) {
							// We need to find inventory and record a SA_OUT
							// transaction
							receiptInfo.stockOutInventoryForDeletedReceipt(receiptDetail, grnNumber);
						}
						receiptDetail.setDocumentStatus(documentStatus);
						receiptDetail.setDocumentStatus(documentStatus);
						receiptDetail.setUpdatedBy(ConstantHelper.RVS_FILE_JOB_USER);
						receiptDetail.setDateUpdated(new Date());
						receiptDetailRepository.save(receiptDetail);
					}
					receiptBody.setDocumentStatus(documentStatus);
					receiptBody.setUpdatedBy(ConstantHelper.RVS_FILE_JOB_USER);
					receiptBody.setDateUpdated(new Date());
					receiptBodyRepository.save(receiptBody);
				}
			}
			receiptHeader.setDocumentStatus(documentStatus);
			receiptHeader.setUpdatedBy(ConstantHelper.RVS_FILE_JOB_USER);
			receiptHeader.setDateUpdated(new Date());
			receiptHeaderRepository.save(receiptHeader);

		}
	}
	/***
	 *	Called from Screen and RDT F2-
	 * from screen force close is true, from rdt only close if no discrepancies
	 * 
	 * 
	 */
	@Override
	public Boolean closeReceipt(String documentReference, String userId, boolean forceClose) throws IOException {
		// This method is called from screen - In this case we force close the
		// Receipt Even in case of discrepancies
		Boolean isClosed = false;
		int cnt = receiptBodyRepository.findBodiesWithDiscrepancies(documentReference);
		logger.info("Force Close is "+ forceClose + " And discrepansies are "+cnt);
		if (forceClose || cnt == 0) {
			List<ReceiptDetail> receiptDetails = rdtInfo.findReceiptDetails(documentReference);
			for (ReceiptDetail receiptDetail : receiptDetails) {
				InventoryMaster inventory = rdtInfo.findInventory(receiptDetail.getSerialReference(),
						receiptDetail.getPartNumber());
				if (inventory != null) {
					rdtInfo.updateInventoryAndDetail(inventory, receiptDetail, userId);
				}
			}
			isClosed = rdtInfo.sendReceiptMessage(documentReference, userId);
		}
		return isClosed;
	}

	@Transactional
	@Override
	public Boolean releaseReceipt(String documentReference, String user) {

		ReceiptHeader receiptHeader = receiptHeaderRepositiory.findByDocumentReference(documentReference);
		DocumentStatus documentStatus = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_OPEN);
		receiptHeader.setDocumentStatus(documentStatus);
		receiptHeader.setUpdatedBy(user);
		receiptHeader.setDateUpdated(new Date());
		return receiptHeaderRepositiory.save(receiptHeader) != null;
	}

	@Transactional
	@Override
	public Boolean processRTS(String headerIds, String user, boolean notReturned) {
		// Set Transacted qty 0 for the selected body and selected records &
		// marked processes true if all RTS bodies are processed
		if (headerIds != null && !headerIds.isEmpty()) {
			String[] ids = headerIds.split(",");
			List<String> idList = Arrays.asList(ids);
			ArrayList<String> headerIdList = new ArrayList<String>(idList);
			for (String headerId : headerIdList) {
				String[] idPart = headerId.split("-");
				logger.info("Processing Header " + headerId + "part ->" + idPart[1]);
				RtsHeader rtsHeader = rtsHeaderRepo.findOne(Long.valueOf(idPart[0]));
				if(!notReturned){
					receiptInfo.createInventoryAndTransactionHistoryForAutoRTSTasks(rtsHeader,
							ConstantHelper.RTS_DEFAULT_LOCATION);
				}
				receiptInfo.processRTSBodiesAndDetails(rtsHeader, user, idPart[1], rtsHeader.getOrderType(), true,
						notReturned);
			}
		}
		return true;
	}

	@Transactional
	@Override
	public Boolean addUnplannedRTS(UnplannedRTSDTO unplannedRTS) {
		boolean setQtyZero = false;
		//Integer maxId = rtsHeaderRepo.findMaxIDForUnplannedRTS();
		//Integer maxSerialCnt = rtsDetailRepo.findMaxIDForUnplannedRTSSerial();
		RtsHeaderDTO rtsHeaderDTO = RecievingServiceHelper.convertUnplannedRTSToRTSTask(unplannedRTS);
		RtsHeader rts = createRtsTask(rtsHeaderDTO);

		receiptInfo.createInventoryAndTransactionHistoryForAutoRTSTasks(rts,
				ConstantHelper.RTS_DEFAULT_LOCATION);
		receiptInfo.processRTSBodiesAndDetails(rts, unplannedRTS.getUser(), unplannedRTS.getPartNumber(),
				ConstantHelper.UNPLANNED_RTS, unplannedRTS.getNotifyKUK(), setQtyZero);
		
		return (rts != null);
	}

	/*****
	 * Create new details records with unique serial reference for odette
	 * printing
	 */
	@Transactional
	@Override
	public String addDetailsForOdetteLabel(String grnReference, String user) {

		ReceiptHeader receiptHeader = receiptHeaderRepositiory.findByCustomerReference(grnReference);
		List<ReceiptDetail> dets = receiptDetailRepository
				.findByDocumentReference(receiptHeader.getDocumentReference());
		if (dets != null && dets.size() > 0) {
			return "true";
		} else {
			receiptInfo.createReceiptDetailsForOdetteLabels(receiptHeader, user);
		}

		return "true";
	}

	@Transactional
	@Override
	public void addHaulierReferenceToMultipleReceipt(HaulierReferenceDTO haulierReferenceDTO) {
		ReceiptHeader receiptHeader = null;
		for (String documentReference : haulierReferenceDTO.getDocumentReferences()) {
			receiptHeader = receiptHeaderRepositiory.findByDocumentReference(documentReference);
			receiptHeader.setHaulierReference(haulierReferenceDTO.getHaulierReference());
			receiptHeaderRepositiory.save(receiptHeader);
		}

	}

	@Transactional
	@Override
	public void deleteHaulierReferenceToMultipleReceipt(HaulierReferenceDTO haulierReferenceDTO) {
		ReceiptHeader receiptHeader = null;
		for (String documentReference : haulierReferenceDTO.getDocumentReferences()) {
			receiptHeader = receiptHeaderRepositiory.findByDocumentReference(documentReference);
			receiptHeader.setHaulierReference("");
			receiptHeaderRepositiory.save(receiptHeader);
		}

	}

	@Transactional
	@Override
	public Boolean addHaulierRefernecToSingleReceipt(String haulierReference, String documentReference) {
		ReceiptHeader receiptHeader = receiptHeaderRepositiory.findByDocumentReference(documentReference);
		receiptHeader.setHaulierReference(haulierReference);
		return receiptHeaderRepositiory.save(receiptHeader) != null;
	}

	@Transactional
	@Override
	public Boolean deleteDocumentDetail(DeleteDocumentDetailRequest delReq) {

		Boolean isDeleted = false;
		TransactionDetailRequest txn = new TransactionDetailRequest();
		ReceiptDetail receiptDetail = receiptDetailRepository.findByDocumentReferenceAndPartNumberAndSerialReference(
				delReq.getDocumentReference(), delReq.getPartNumber(), delReq.getSerialReference());

		ReceiptHeader receiptHeader = receiptHeaderRepository.findByDocumentReference(delReq.getDocumentReference());

		Part part = partRepository.findByPartNumber(delReq.getPartNumber());

		txn.setReceiptHeader(receiptHeader);
		txn.setPart(part);
		txn.setPartNumber(delReq.getPartNumber());
		txn.setSerialReference(delReq.getSerialReference());

		if (receiptHeader.getDocumentStatus().getDocumentStatusCode()
				.equalsIgnoreCase(ConstantHelper.DOCUMENT_STATUS_CLOSED)
				|| receiptHeader.getDocumentStatus().getDocumentStatusCode()
						.equalsIgnoreCase(ConstantHelper.DOCUMENT_STATUS_TRANSMITTED)) {
			System.err.println("RETURN");
			return isDeleted;
		}

		Double quantity = receiptDetail.getReceivedQty();

		// update receipt detail
		if (receiptDetail.getAdvisedQty() == 0) {
			logger.info("advised qty is zero");
			// isDeleted =
			// receivingDAO.deleteReceiptDetail(delReq.getDocumentReference(),delReq.getSerialReference());
			receiptDetailRepository.delete(receiptDetail);
		} else {
			logger.info("advised qty is not zero");
			receiptDetail.setReceivedQty(0.00);
			receiptDetail.setQuarantinedQty(0.00);
			receiptDetail.setDifference(receiptDetail.getReceivedQty() - receiptDetail.getAdvisedQty());
			receiptDetail.setDateUpdated(new Date());
			receiptDetail.setUpdatedBy(delReq.getCurrentUser());
			receiptDetailRepository.save(receiptDetail);
		}

		// update receipt body
		ReceiptBody receiptBody = receiptBodyRepository.findById(receiptDetail.getReceiptBody().getId());
		receiptBody.setReceivedQty(receiptBody.getReceivedQty() - quantity);

		if (receiptBody.getAdvisedQty().compareTo((double) 0) == 0
				&& receiptBody.getReceivedQty().compareTo((double) 0) == 0) {

			receiptBodyRepository.delete(receiptBody);
		} else {

			receiptBody.setDifference(receiptBody.getReceivedQty() - receiptBody.getAdvisedQty());
			receiptDetail.setDateUpdated(new Date());
			receiptDetail.setUpdatedBy(delReq.getCurrentUser());
			receiptBodyRepository.save(receiptBody);
		}

		// record transaction and delete inventory

		InventoryMaster inventory = inventoryMasterRepository
				.findBySerialReferenceAndPartNumber(delReq.getSerialReference(), delReq.getPartNumber());
		if (inventory != null) {
			txn.setTransactionTypeId(ConstantHelper.RECEIPT_OUT_SHORT_CODE);
			txn.setQuantity(quantity);
			txn.setFromLocation(inventory.getCurrentLocation());
			txn.setFromPlt(inventory.getTagReference());
			txn.setCurrentUser(delReq.getCurrentUser());
			rdtInfo.recordTransaction(txn);

			inventoryMasterRepository.delete(inventory);
		}
		isDeleted = true;
		return isDeleted;

	}

	@Transactional
	@Override
	public Boolean deleteDocument(String documentReference) {
		Boolean isdeleted = false;
		List<ReceiptDetail> receiptDetail = receiptDetailRepository.findByDocumentReference(documentReference);
		if (receiptDetail.size() == 0) {
			Boolean isDelBody = receivingDAO.deleteReceiptBody(documentReference);
			Boolean isDelHeader = receivingDAO.deleteReceiptHeader(documentReference);
			isdeleted = isDelHeader;
		}
		return isdeleted;
	}

	@Transactional
	@Override
	public Boolean validateHaulierReference(String haulierReference) {

		Boolean isExist = false;
		List<ReceiptHeader> receiptHeaders = receiptHeaderRepository.findByHaulierReference(haulierReference);
		if (receiptHeaders.size() > 0) {
			isExist = true;
		}
		return isExist;
	}

	public Double setConversionFactor(Double qty, Integer conversionFactor) {
		return qty * conversionFactor;
	}

	public Double getConversionFactor(Double qty, Integer conversionFactor) {
		return qty / conversionFactor;
	}

	public static void main(String args[]) {
		Double a = (double) 10;
		if (a.compareTo((double) 0) == 0) {
			// ("Its empty");
		} else {
			// ("its not empty");
		}
	}

}
