package com.vantec.receiving.service;

import java.io.IOException;

import com.vantec.receiving.model.OffsiteReturnMessageDTO;
import com.vantec.receiving.model.RTSReturnMessageDTO;

public interface MessageService {
	
	public Boolean writeCompleteReceiptMessage(String documentReference, String userId) throws IOException;
	
	public Boolean writeCompleteCaseMessage(String caseReference, String documentReference, String userId) throws IOException;
	
	public Boolean writeRTSReturnMessage(RTSReturnMessageDTO request) throws IOException;
	
	public Boolean writeSTKMVOffsiteReturnMessage(OffsiteReturnMessageDTO request) throws IOException ;
	public Boolean writeRelocReturnMessage( String documentReference,String userId) throws IOException ;
}
