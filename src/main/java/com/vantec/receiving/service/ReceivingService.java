package com.vantec.receiving.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import com.vantec.receiving.entity.ReceiptHeader;
import com.vantec.receiving.entity.RtsHeader;
import com.vantec.receiving.exception.VantecReceivingException;
import com.vantec.receiving.model.ChangePnoDTO;
import com.vantec.receiving.model.DeleteDocumentDetailRequest;
import com.vantec.receiving.model.HaulierReferenceDTO;
import com.vantec.receiving.model.ReceiptHeaderDTO;
import com.vantec.receiving.model.RelocTaskDTO;
import com.vantec.receiving.model.RtsHeaderDTO;
import com.vantec.receiving.model.UnplannedRTSDTO;
import com.vantec.receiving.model.VendorDTO;

public interface ReceivingService {
	
	ReceiptHeader createReceipt(ReceiptHeaderDTO receiptHeaderDTO);
	List<String> createRelocTask(RelocTaskDTO relocTaskDTO) throws IOException,VantecReceivingException;
	
	void deleteReceipt(String grnNumber);
	
	boolean saveRecievingRequest(ReceiptHeaderDTO receivingRequests);

	List<ReceiptHeader> getAllReceiptDocs(int pageNumber, int size);

	void addHaulierReferenceToMultipleReceipt(HaulierReferenceDTO haulierReferenceDTO);

	Boolean addHaulierRefernecToSingleReceipt(String haulierReference, String documentReference);

	void deleteHaulierReferenceToMultipleReceipt(HaulierReferenceDTO haulierReferenceDTO);

	Boolean deleteDocumentDetail(DeleteDocumentDetailRequest delReq);

	Boolean deleteDocument(String documentReference);

	Boolean validateHaulierReference(String haulierReference);

	boolean createVendor(VendorDTO vendorDTO) throws ParseException;

	Boolean editVendor(VendorDTO vendorDTO) throws ParseException;

	RtsHeader createRtsTask(RtsHeaderDTO rtsHeaderDTO);

	Boolean releaseReceipt(String documentReference,String user);
	
	Boolean processRTS(String headerIds, String user, boolean notReturned);
	
	Boolean addUnplannedRTS(UnplannedRTSDTO unplannedRTS);
	
	String addDetailsForOdetteLabel(String grnReference, String user);
	boolean processChangePartNo(ChangePnoDTO changePno);
	
	
	Boolean closeReceipt(String documentReference, String userId, boolean forceClose) throws IOException ;

}
