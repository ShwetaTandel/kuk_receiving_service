package com.vantec.receiving.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.receiving.business.RDTInfo;
import com.vantec.receiving.entity.InventoryMaster;
import com.vantec.receiving.entity.ReceiptDetail;
import com.vantec.receiving.entity.ReceiptHeader;
import com.vantec.receiving.exception.VantecReceivingException;
import com.vantec.receiving.model.DefaultSettingDTO;
import com.vantec.receiving.model.PartDTO;
import com.vantec.receiving.model.TransactionDetailRequest;
import com.vantec.receiving.model.ValidateResponse;

@Service("rtdService")
public class RDTServiceImpl implements RDTService {

	@Autowired
	RDTInfo rdtInfo;

	@Autowired
	MessageService messageService;

	@Override
	public ValidateResponse validateRDTField(String fieldName, String fieldValue, String transactionType)
			throws VantecReceivingException {

		ValidateResponse validateResponse = null;

		if (fieldName.equals("haulierReference")) {
			validateResponse = rdtInfo.validateHaulierReference(fieldValue);
		}
		if (fieldName.equals("fromLocationHashCode")) {
			validateResponse = rdtInfo.validateFromLocation(fieldValue, transactionType);
		}
		if (fieldName.equals("toLocationHashCode")) {
			validateResponse = rdtInfo.validateToLocation(fieldValue, transactionType);
		}
		if (fieldName.equals("plt")) {
			validateResponse = rdtInfo.validatePlt(fieldValue);
		}
		if (fieldName.equals("partNumber")) {
			validateResponse = rdtInfo.validatePart(fieldValue);
		}
		if (fieldName == "serialNumber") {
			validateResponse = rdtInfo.validateSerial(fieldValue);
		}

		if (fieldName == "ranOrder") {
			validateResponse = rdtInfo.validateRanOrder(fieldValue);
		}
		if (fieldName == "vendorCode") {
			validateResponse = rdtInfo.validateVendorCode(fieldValue);
		}

		return validateResponse;
	}

	@Transactional
	@Override
	public Boolean processTransaction(TransactionDetailRequest txn) throws VantecReceivingException {

		rdtInfo.updateDocument(txn);

		rdtInfo.updateInventory(txn);

		rdtInfo.recordTransaction(txn);

		return true;
	}

	@Override
	public ValidateResponse validatePartSerial(String partNumber, String serialNumber) throws VantecReceivingException {
		ValidateResponse validateResponse = null;
		validateResponse = rdtInfo.validatePartSerial(partNumber, serialNumber);
		return validateResponse;
	}

	@Override
	public List<DefaultSettingDTO> getDefaultSettings(String companyName) throws VantecReceivingException {
		List<DefaultSettingDTO> settings = null;
		settings = rdtInfo.getDefaultSettings(companyName);
		return settings;
	}

	@Override
	public PartDTO findPartDetails(String partNumber) throws VantecReceivingException {
		PartDTO partDTO = new PartDTO();
		partDTO = rdtInfo.findPartDetails(partNumber);
		return partDTO;
	}

	@Override
	public String createVirtualPlt(String transactionTypeId) {
		return rdtInfo.createVirtualPlt(transactionTypeId);
	}

	@Override
	public String createPlt(String palletCode) {
		return rdtInfo.createPlt(palletCode);
	}

	@Override
	public Boolean completeReceipts(String haulierReference) throws VantecReceivingException {
		Boolean isCompleted = false;
		isCompleted = rdtInfo.completeReceipts(haulierReference);
		return isCompleted;
	}

	

	
}
