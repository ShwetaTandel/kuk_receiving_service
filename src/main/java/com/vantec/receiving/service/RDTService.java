package com.vantec.receiving.service;

import java.util.List;

import com.vantec.receiving.exception.VantecReceivingException;
import com.vantec.receiving.model.DefaultSettingDTO;
import com.vantec.receiving.model.PartDTO;
import com.vantec.receiving.model.TransactionDetailRequest;
import com.vantec.receiving.model.ValidateResponse;

public interface RDTService {
	
	ValidateResponse validateRDTField(String fieldName,String fieldValue,String transactionType) throws VantecReceivingException;
	
	Boolean processTransaction( TransactionDetailRequest txn) throws VantecReceivingException;
	
	ValidateResponse validatePartSerial(String partNumber,String serialNumber) throws VantecReceivingException;
	
	List<DefaultSettingDTO> getDefaultSettings(String company) throws VantecReceivingException;

	String createPlt(String palletCode);
	
	PartDTO findPartDetails(String partNumber) throws VantecReceivingException ;

	Boolean completeReceipts(String haulierReference) throws VantecReceivingException;

	String createVirtualPlt(String transactionTypeid);

	//Boolean closeReceipt(String documentReference,String userId) throws IOException;
	
	//Boolean closeReceipts(String haulierReference,String userId) throws IOException;
}
