package com.vantec.receiving.service;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

public interface ReceiptService {

	void readReceipt(File file) throws IOException, ParseException;
	void deleteReceipt(File file) throws IOException, ParseException;
	void returnToStore(File file) throws IOException, ParseException;
	void readReloc(File file) throws IOException, ParseException;
	void readChgPno(File file) throws IOException, ParseException;
	
	

}
