package com.vantec.receiving.service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.receiving.entity.Company;
import com.vantec.receiving.entity.DocumentStatus;
import com.vantec.receiving.entity.ReceiptBody;
import com.vantec.receiving.entity.ReceiptHeader;
import com.vantec.receiving.entity.RelocHeader;
import com.vantec.receiving.entity.RtsBody;
import com.vantec.receiving.entity.RtsHeader;
import com.vantec.receiving.entity.UploadFile;
import com.vantec.receiving.model.OffsiteReturnMessageDTO;
import com.vantec.receiving.model.RTSReturnMessageDTO;
import com.vantec.receiving.repository.CompanyRepository;
import com.vantec.receiving.repository.DocumentStatusRepository;
import com.vantec.receiving.repository.InventoryMasterRepository;
import com.vantec.receiving.repository.ReceiptBodyRepository;
import com.vantec.receiving.repository.ReceiptDetailRepository;
import com.vantec.receiving.repository.ReceiptHeaderRepository;
import com.vantec.receiving.repository.RelocHeaderRepository;
import com.vantec.receiving.repository.RtsBodyRepository;
import com.vantec.receiving.repository.RtsDetailRepository;
import com.vantec.receiving.repository.RtsHeaderRepository;
import com.vantec.receiving.repository.UploadFileRepository;
import com.vantec.receiving.util.ConstantHelper;

@Service("messageService")
public class MessageServiceImpl implements MessageService {

	
	private static Logger logger = LoggerFactory.getLogger(MessageServiceImpl.class);
	@Autowired
	ReceiptHeaderRepository receiptHeaderRepository;

	@Autowired
	ReceiptBodyRepository receiptBodyRepository;

	@Autowired
	ReceiptDetailRepository receiptDetailRepository;

	@Autowired
	CompanyRepository companyRepository;

	@Autowired
	UploadFileRepository uploadFileRepository;

	@Autowired
	InventoryMasterRepository inventoryMasterRepository;

	@Autowired
	DocumentStatusRepository documentStatusRepository;

	@Autowired
	RtsHeaderRepository rtsHeaderRepo;

	@Autowired
	RtsBodyRepository rtsBodyRepo;

	@Autowired
	RtsDetailRepository rtsDetailRepo;
	
	@Autowired
	RelocHeaderRepository relocHeaderRepo;

	/**
	 * Called from Screen and RDT F2-
	 * from screen force close, from rdt only close if no discrepancies
	 */
	@Transactional
	public Boolean writeCompleteReceiptMessage(String documentReference, String userId) throws IOException {

		Company company = companyRepository.findAll().get(0);
		DocumentStatus status = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_COMPLETED);
		ReceiptHeader receiptHeader = receiptHeaderRepository
				.findByDocumentReferenceAndDocumentStatus(documentReference, status);
		logger.info("Doc is - >" + documentReference);
		if (receiptHeader != null) {

			DocumentStatus transmittedDocumentStatus = documentStatusRepository
					.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_TRANSMITTED);
			List<ReceiptBody> receiptBodies = receiptBodyRepository
					.findByDocumentReferenceAndStatusNotTransmitted(documentReference, transmittedDocumentStatus);
			logger.info("Header found - >");
			if (receiptBodies != null && !receiptBodies.isEmpty()) {
				logger.info("Bodies found ->");

				UploadFile savedFile = createFileRecord(userId, company, ConstantHelper.RETURN_FILE_TYPE_OFFLOD);

				String filename = ConstantHelper.RETURN_FILE_TYPE_OFFLOD + String.format("%09d", savedFile.getId());

				Path tempPath = Paths.get(company.getInterfaceOutput() + "\\" + filename + ConstantHelper.TMP_FILE_EXT);
				Path textPath = Paths
						.get(company.getInterfaceOutput() + "\\" + filename + ConstantHelper.TEXT_FILE_EXT);

				// Write in temp file
				// No discrepancies - 115180|1|0|||
				// Unadvised Parts - 115030|15|2|2A706K1470|15|JDR008
				// ---->GRN|Seqn|Received|Part|Seqn|Location
				// Over Delivered - 115030|8|1|||JDR008 --->
				// GRN|Seqn|Received|||Location
				// Shortages - 115020|14|-1||| ----> GRN|Seqn|Received|||

				BufferedWriter writer = Files.newBufferedWriter(tempPath);
				writer.write(ConstantHelper.BAAN_FILE_START_FILE_RECORD);
				// Body
				int lineCnt = 0;
				for (ReceiptBody receiptBody : receiptBodies) {
					writeReceiptBodyLine(receiptHeader.getCustomerReference(), writer, receiptBody);
					receiptDetailRepository.updateStatusByBody(receiptBody, userId, transmittedDocumentStatus);
					lineCnt++;
				}
				writer.write("\n");
				writer.write(ConstantHelper.BAAN_FILE_END_FILE_RECORD + " - " + lineCnt + " records written");
				// Copy data from temp to txt file and delete temp file
				writer.close();
				Files.copy(tempPath, textPath, StandardCopyOption.REPLACE_EXISTING);
				Files.delete(tempPath);

				receiptBodyRepository.updateStatusByDoc(documentReference, userId, transmittedDocumentStatus);

				savedFile.setFilename(filename + ConstantHelper.TEXT_FILE_EXT);
				uploadFileRepository.save(savedFile);
				logger.info("Message sent" + filename);

			}
			// Instead of close , Its transmitted because messsage send
			// consecutively
			receiptHeader.setDocumentStatus(transmittedDocumentStatus);
			receiptHeader.setUpdatedBy(userId);
			receiptHeader.setDateUpdated(new Date());
			receiptHeaderRepository.save(receiptHeader);
			return true;
		}
		return false;

	}

	/***
	 * 
	 * 
	 * 
	 */
	@Transactional
	public Boolean writeCompleteCaseMessage(String caseReference, String documentReference, String userId)
			throws IOException {

		Company company = companyRepository.findAll().get(0);
		DocumentStatus completeStatus = documentStatusRepository.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_COMPLETED);
		ReceiptHeader receiptHeader = receiptHeaderRepository.findByDocumentReference(documentReference);
		List<ReceiptBody> receiptBodies = receiptBodyRepository
				.findByCaseReferenceAndDocumentReferenceAndDocumentStatus(caseReference, documentReference,
						completeStatus);
		logger.info("Doc ->" + documentReference + " case ref -> " + caseReference);
		if (receiptBodies != null && !receiptBodies.isEmpty()) {
			logger.info("Bodies found");
			DocumentStatus transmittedStatus = documentStatusRepository.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_TRANSMITTED);
			UploadFile savedFile = createFileRecord(userId, company, ConstantHelper.RETURN_FILE_TYPE_OFFLOD);

			String filename = ConstantHelper.RETURN_FILE_TYPE_OFFLOD + String.format("%09d", savedFile.getId());

			Path tempPath = Paths.get(company.getInterfaceOutput() + "\\" + filename + ConstantHelper.TMP_FILE_EXT);
			Path textPath = Paths.get(company.getInterfaceOutput() + "\\" + filename + ConstantHelper.TEXT_FILE_EXT);

			// Write in temp file
			// No discrepancies - 115180|1|0|||
			// Unadvised Parts - 115030|15|2|2A706K1470|15|JDR008
			// ---->GRN|Seqn|Received|Part|Seqn|Location
			// Over Delivered - 115030|8|1|||JDR008 --->
			// GRN|Seqn|Received|||Location
			// Shortages - 115020|14|-1||| ----> GRN|Seqn|Received|||

			BufferedWriter writer = Files.newBufferedWriter(tempPath);
			writer.write(ConstantHelper.BAAN_FILE_START_FILE_RECORD);

			int lineCnt = 0;
			for (ReceiptBody receiptBody : receiptBodies) {
				writeReceiptBodyLine(receiptHeader.getCustomerReference(), writer, receiptBody);
				receiptDetailRepository.updateStatusByBody(receiptBody, userId, transmittedStatus);
				lineCnt++;
			}

			writer.write("\n");
			writer.write(ConstantHelper.BAAN_FILE_END_FILE_RECORD+ " - " + lineCnt + " record written");
			// Copy data from temp to txt file and delete temp file
			writer.close();
			Files.copy(tempPath, textPath, StandardCopyOption.REPLACE_EXISTING);
			Files.delete(tempPath);

			receiptBodyRepository.updateStatusByCase(documentReference, caseReference, userId, transmittedStatus);

			savedFile.setFilename(filename +ConstantHelper.TEXT_FILE_EXT);
			uploadFileRepository.save(savedFile);
			return true;
		}
		return false;

	}

	private UploadFile createFileRecord(String userId, Company company,String type) {

		UploadFile receiptMessageFile = new UploadFile();
		receiptMessageFile.setFilename(type);
		
		receiptMessageFile.setType(type);
		receiptMessageFile.setFullPath(company.getInterfaceOutput());
		receiptMessageFile.setUpdatedBy(userId);
		receiptMessageFile.setDateCreated(new Date());
		receiptMessageFile.setLastUpdated(new Date());
		UploadFile savedFile = uploadFileRepository.save(receiptMessageFile);
		return savedFile;

	}

	private void writeReceiptBodyLine(String custRef, BufferedWriter writer, ReceiptBody receiptBody)
			throws IOException {
		// Body

		writer.write("\n");
		if (receiptBody.getLineNo() == null) {
			receiptBody.setLineNo(0);
		}
		if (receiptBody.getAdvisedQty() > receiptBody.getReceivedQty()) {
			// Case of shortage
			writer.write(custRef + "|" + receiptBody.getLineNo() + "|"
					+ (int) (receiptBody.getReceivedQty() - receiptBody.getAdvisedQty()) + "|||");
		} else if (receiptBody.getAdvisedQty() < receiptBody.getReceivedQty()) {
			String serialRef = receiptDetailRepository
					.findByPartNumberAndReceiptBodyAndReceivedQtyGreaterThanZero(receiptBody.getPartNumber(), receiptBody).get(0)
					.getSerialReference();
			String location = inventoryMasterRepository
					.findBySerialReferenceAndPartNumber(serialRef, receiptBody.getPartNumber()).getCurrentLocation()
					.getLocationCode();
			// Case of unadvised parts
			if (receiptBody.getAdvisedQty() == 0) {
				writer.write(custRef + "|" + receiptBody.getLineNo() + "|" + receiptBody.getReceivedQty().intValue()
						+ "|" + receiptBody.getPartNumber() + "|" + receiptBody.getLineNo() + "|" + location);
			} else {
				// Case of over delivered
				writer.write(custRef + "|" + receiptBody.getLineNo() + "|"
						+ (int) (receiptBody.getReceivedQty() - receiptBody.getAdvisedQty()) + "|||" + location);
			}
		} else {
			// No discrepancies
			writer.write(custRef + "|" + receiptBody.getLineNo() + "|0|||");
		}

	}

	/***
	 * Write return RTS messages
	 */
	@Transactional
	public Boolean writeRTSReturnMessage(RTSReturnMessageDTO request) throws IOException {
		Company company = companyRepository.findAll().get(0);

		RtsHeader rtsHeader = rtsHeaderRepo.findByOrderReference(request.getOrderReference());

		List<RtsBody> rtsBodies = rtsBodyRepo.findByRtsHeaderAndPartNumber(rtsHeader, request.getPartNumber());

		logger.info(
				"Order reference  ->" + request.getOrderReference() + "part number -> " + request.getPartNumber());
		if (rtsBodies != null && !rtsBodies.isEmpty()) {
			logger.info("Bodies found");
			DocumentStatus transmittedStatus = documentStatusRepository.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_TRANSMITTED);
			String filetypename = request.getType().equalsIgnoreCase(ConstantHelper.UNPLANNED_RTS)? ConstantHelper.RETURN_FILE_TYPE_UNPLANNED_RTS:ConstantHelper.RETURN_FILE_TYPE_PLANNED_RTS;
			UploadFile savedFile = createFileRecord(request.getUser(), company, filetypename );
			logger.info("File records create for "+savedFile.getFilename() + " ->" + savedFile.getType() + "->"+savedFile.getId());
			String filename = filetypename + String.format("%09d", savedFile.getId());

			Path tempPath = Paths.get(company.getInterfaceOutput() + "\\" + filename + ConstantHelper.TMP_FILE_EXT);
			Path textPath = Paths.get(company.getInterfaceOutput() + "\\" + filename + ConstantHelper.TEXT_FILE_EXT);

			//PLANNED ------------->
			//4	EF0000721	9895	20862K1840NK	-2
			//4	EF0000721	9897	20862K1840NK	-2
			//4	MR0037674	0	2084621503	-1
			//Origin	External Order	Position	Part 	Quantity
							
			//Origin	"Fixed needs to match what was sent originally for BaaN Order
			//Not used by WMS 1,2,4,22"			
			//External Order	"BaaN Order Number
			//Used by BaaN to reference WMS"			
			//Position	Set to what was sent in Planned RTS (can be 0)			
			//Part	WMS Part Number			
			//Qty	Quantity (can be negative as being removed from order)			

			//UNPLANNED ------------>
			//WA	UNRTS	2079797961	8
			//WA	RTSCC	2070372651	8
			//OORG	ORNO	PNO	QTY
			//Origin	External Order	Part No	Quantity
			//Origin	Fixed set to 'WA'			
			//External Order	"UNRTS for Unplanned RTS,
			//RTSCC for Unplanned RTS with Reason code 18"			
			//Part	WMS Part Number			
			//Qty	Quantity			



			BufferedWriter writer = Files.newBufferedWriter(tempPath);
			writer.write(ConstantHelper.BAAN_FILE_START_FILE_RECORD);
			writer.write("\n");
			int lineCnt = 0;
			for (RtsBody rtsBody : rtsBodies) {
				String origin = rtsHeader.getOrigin() !=null ? rtsHeader.getOrigin() : "";
				String pos = rtsHeader.getPosition()!=null ? rtsHeader.getPosition() : "";
				if (request.getType().equalsIgnoreCase(ConstantHelper.UNPLANNED_RTS)) {
					String exOrder = ConstantHelper.UNPLANNED_RTS_FILE_RETURN_TYPE_GENERAL;
					if (rtsHeader.getReasonCode().equalsIgnoreCase(ConstantHelper.UNPLANNED_REASON_CODE_RTS_PROJ)) {
						exOrder = ConstantHelper.UNPLANNED_RTS_FILE_RETURN_TYPE_FOR_REASON_CODE_18;
					}
					writer.write(ConstantHelper.UNPLANNED_RTS_FILE_RETURN_ORIGIN +"|" + exOrder + "|" + rtsBody.getPartNumber() + "|-" + rtsBody.getQtyTransacted().intValue());
				}else {
					writer.write(origin + "|" + rtsHeader.getOrderReference() + "|"
							+ pos+ "|" + rtsBody.getPartNumber() + "|-" + rtsBody.getQtyTransacted().intValue());
				}

				rtsDetailRepo.updateStatusByBody(rtsBody, request.getUser(), transmittedStatus);
				lineCnt++;
			}

			writer.write("\n");
			writer.write(ConstantHelper.BAAN_FILE_END_FILE_RECORD+" - " + lineCnt + " record written");
			// Copy data from temp to txt file and delete temp file
			writer.close();
			Files.copy(tempPath, textPath, StandardCopyOption.REPLACE_EXISTING);
			Files.delete(tempPath);

			rtsBodyRepo.updateStatusByHeader(rtsHeader, request.getUser(), transmittedStatus);

			savedFile.setFilename(filename + ".txt");
			uploadFileRepository.save(savedFile);
			return true;
		}
		return false;

	}
	
	/***
	 * Write return STKMV  messages
	 */
	@Transactional
	public Boolean writeSTKMVOffsiteReturnMessage(OffsiteReturnMessageDTO request) throws IOException {
		/*
		 * Orginal Part | New Part (BLANK) | Quantity | From Offsite | To WMS
			Currently Offsite is either KTX or POT.
			However the program caters for 'NFS STOCK' or 'RITESPACE' (CENG) . Don't ask :)

			Start of file
			2084621A21KD||2.00000|KTX|WMS
			2084621A31KB||2.00000|KTX|WMS
			End of file - 2 records written
			*/
		
		
		Company company = companyRepository.findAll().get(0);
		if (request != null ) {
			logger.info(
					"STKMV request for part NUmber  reference  ->" + request.getPartNumber() + "Qty -> " + request.getQuantity() + "   from location -> "+request.getFromLocation());
			String filetypename = ConstantHelper.RETURN_FILE_TYPE_STKMV;
			UploadFile savedFile = createFileRecord(request.getUser(), company, filetypename );
			logger.info("File records create for "+savedFile.getFilename() + " ->" + savedFile.getType() + "->"+savedFile.getId());
			String filename = filetypename + String.format("%09d", savedFile.getId());

			Path tempPath = Paths.get(company.getInterfaceOutput() + "\\" + filename + ConstantHelper.TMP_FILE_EXT);
			Path textPath = Paths.get(company.getInterfaceOutput() + "\\" + filename + ConstantHelper.TEXT_FILE_EXT);

			BufferedWriter writer = Files.newBufferedWriter(tempPath);
			writer.write(ConstantHelper.BAAN_FILE_START_FILE_RECORD);
			writer.write("\n");
			int lineCnt = 0;
			writer.write(request.getPartNumber() + "||" + getDecimalFormat(request.getQuantity()) + "|"
							+ request.getFromLocation()+ "|WMS");
			lineCnt++;

			writer.write("\n");
			writer.write(ConstantHelper.BAAN_FILE_END_FILE_RECORD+" - " + lineCnt + " record written");
			// Copy data from temp to txt file and delete temp file
			writer.close();
			Files.copy(tempPath, textPath, StandardCopyOption.REPLACE_EXISTING);
			Files.delete(tempPath);
			savedFile.setFilename(filename + ".txt");
			uploadFileRepository.save(savedFile);
			return true;
		}
		return false;

		
	}
	/***
	 * 
	 * return RELOC messages for lost serials
	 * @return
	 */
	public Boolean writeRelocReturnMessage(String documentReference, String userId) throws IOException {

		// 127235|1|2|quarantine|lost|0
		// GRN!line no|Qty | from loc | lost |0
		/*
		 * Start of file 
		 * 127235|1|2|quarantine|lost|0 End of file - 1 records
		 * written
		 */

		Company company = companyRepository.findAll().get(0);
		RelocHeader relocHeader = relocHeaderRepo.findByDocumentReference(documentReference);
		if (relocHeader != null) {
			UploadFile savedFile = createFileRecord(userId, company, ConstantHelper.RETURN_FILE_TYPE_RELOC);

			String filename = ConstantHelper.RETURN_FILE_TYPE_RELOC + String.format("%09d", savedFile.getId());

			Path tempPath = Paths.get(company.getInterfaceOutput() + "\\" + filename + ConstantHelper.TMP_FILE_EXT);
			Path textPath = Paths.get(company.getInterfaceOutput() + "\\" + filename + ConstantHelper.TEXT_FILE_EXT);
			BufferedWriter writer = Files.newBufferedWriter(tempPath);
			writer.write(ConstantHelper.BAAN_FILE_START_FILE_RECORD);
			writer.write("\n");
			int lineCnt = 0;
			writer.write(relocHeader.getReceiptHeader().getCustomerReference() + "|" + relocHeader.getLineNo() + "|"
					+ relocHeader.getQuantity().intValue() + "|" + relocHeader.getFromLocation() + "|" + "lost|0");

			writer.write("\n");
			writer.write(ConstantHelper.BAAN_FILE_END_FILE_RECORD + " - " + lineCnt + " record written");
			// Copy data from temp to txt file and delete temp file
			writer.close();
			Files.copy(tempPath, textPath, StandardCopyOption.REPLACE_EXISTING);
			Files.delete(tempPath);

			savedFile.setFilename(filename + ConstantHelper.TEXT_FILE_EXT);
			uploadFileRepository.save(savedFile);
			relocHeader.setProcessed(true);
			relocHeader.setLastUpdated(new Date());
			relocHeader.setLastUpdatedBy(userId);
			relocHeaderRepo.save(relocHeader);			
			
			return true;
		}

		return false;
	}




	public String getLineNo(int line, int maxvalue) {
		String lineNumber = "";
		int numberOfDigits = 0;
		numberOfDigits = String.valueOf(line).length();
		for (int i = 0; i < maxvalue - numberOfDigits; i++) {
			lineNumber += "0";
		}
		lineNumber = lineNumber + String.valueOf(line);
		return lineNumber;
	}

	private static String getDecimalFormat(Double qty) {
		String pattern = ".00000";
		DecimalFormat decimalFormat = new DecimalFormat(pattern);
		String number = decimalFormat.format(qty );
		return number;
	}
	

	public static void main(String args[]) {
//		Path tempPath = Paths.get("C:\\kukfiles\\output\\" + "test100" + ".tmp");
//		Path textPath = Paths.get("C:\\kukfiles\\output\\" + "test100" + ".txt");
//		try {
//			BufferedWriter writer = Files.newBufferedWriter(tempPath);
//			writer.write("test the file");
//			writer.write("\r");
//			writer.write("test the file");
//			writer.close();
//			Files.copy(tempPath, textPath, StandardCopyOption.REPLACE_EXISTING);
//			Files.delete(tempPath);
			System.out.println(getDecimalFormat(new Double(00000012.00000)));
			

//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

	}

}
