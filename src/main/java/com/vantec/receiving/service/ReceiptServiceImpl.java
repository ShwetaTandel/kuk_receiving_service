package com.vantec.receiving.service;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.receiving.business.RDTInfo;
import com.vantec.receiving.business.ReceiptInfo;
import com.vantec.receiving.entity.DownloadFile;
import com.vantec.receiving.entity.ReceiptHeader;
import com.vantec.receiving.entity.RtsBody;
import com.vantec.receiving.entity.RtsHeader;
import com.vantec.receiving.exception.VantecReceivingException;
import com.vantec.receiving.model.ChangePnoDTO;
import com.vantec.receiving.model.ReceiptHeaderDTO;
import com.vantec.receiving.model.RelocTaskDTO;
import com.vantec.receiving.model.RtsHeaderDTO;
import com.vantec.receiving.repository.CompanyRepository;
import com.vantec.receiving.repository.DocumentStatusRepository;
import com.vantec.receiving.repository.DownloadFileRepository;
import com.vantec.receiving.repository.InventoryMasterRepository;
import com.vantec.receiving.repository.PartRepository;
import com.vantec.receiving.repository.PickBodyRepository;
import com.vantec.receiving.repository.ReceiptBodyRepository;
import com.vantec.receiving.repository.ReceiptDetailRepository;
import com.vantec.receiving.repository.ReceiptHeaderRepository;
import com.vantec.receiving.repository.RtsHeaderRepository;
import com.vantec.receiving.repository.VendorRepository;
import com.vantec.receiving.util.ConstantHelper;
import com.vantec.receiving.util.ReceiptFileHandlerHelper;

@Service("orderService")
public class ReceiptServiceImpl implements ReceiptService {
	private static Logger logger = LoggerFactory.getLogger(ReceiptServiceImpl.class);

	@Autowired
	DocumentStatusRepository documentStatusRepository;

	@Autowired
	PartRepository partRepository;

	@Autowired
	CompanyRepository companyRepository;

	@Autowired
	ReceivingServiceImpl receivingServiceImpl;

	@Autowired
	ReceiptHeaderRepository receiptHeaderRepository;

	@Autowired
	ReceiptBodyRepository receiptBodyRepository;

	@Autowired
	ReceiptDetailRepository receiptDetailRepository;

	@Autowired
	InventoryMasterRepository inventoryMasterRepository;

	@Autowired
	RtsHeaderRepository rtsHeaderRepository;

	@Autowired
	ReceiptInfo receiptInfo;

	@Autowired
	DownloadFileRepository downloadFileRepo;
	
	@Autowired
	PickBodyRepository pickBodyRepository;
	
	@Autowired
	VendorRepository vendorRepo;
	
	@Autowired
	RDTInfo rdtInfo;

	
	@Override
	/****
	 * Read the GRN file and create corresponding receipts
	 * 
	 * 
	 */
	public void readReceipt(File file) throws IOException, ParseException {

		DownloadFile recordFile = recordFileRead(file.getName(), ConstantHelper.GRN_FILE_TYPE, ConstantHelper.SYSUSER);
		
		List<ReceiptHeaderDTO> receiptHeaderDTOs = ReceiptFileHandlerHelper.readGRNFile(file);
		
		for (ReceiptHeaderDTO receiptHeaderDTO : receiptHeaderDTOs) {
			//if(acceptedVendors.contains(receiptHeaderDTO.getVendors().get(0))){
				ReceiptHeader receiptHeader = receiptHeaderRepository
						.findByCustomerReference(receiptHeaderDTO.getGrnNumber());
				if (receiptHeader == null) {
					logger.info("Adding Receipt for"+receiptHeaderDTO.getGrnNumber());
					receivingServiceImpl.createReceipt(receiptHeaderDTO);
					recordFile.setProcessed(true);
					downloadFileRepo.save(recordFile);

				}else{
					logger.info("Existing Receipt found for"+receiptHeaderDTO.getGrnNumber());
					recordFile.setProcessed(false);
					downloadFileRepo.save(recordFile);
					
				}
			/*}else{
				if(receiptHeaderDTO.getVendors().size() > 1){
					logger.info("Ignoring -> its Multi vendor receipt->"+receiptHeaderDTO.getGrnNumber());
				}else {
					logger.info("Ignoring -> Vendor not configured ->"+receiptHeaderDTO.getGrnNumber() + "  "+ receiptHeaderDTO.getVendors().get(0));
				}
				recordFile.setProcessed(false);
				downloadFileRepo.save(recordFile);
			}*/

		}
	}

	@Override
	/****
	 * Read the RTS file and create corresponding RTS receipts
	 * 
	 * 
	 */
	@Transactional
	public void returnToStore(File file) throws IOException, ParseException {

		DownloadFile recordFile = recordFileRead(file.getName(), ConstantHelper.RTS_FILE_TYPE, ConstantHelper.SYSUSER);
		List<RtsHeaderDTO> rtsHeaderDTOs = ReceiptFileHandlerHelper.readRTSFile(file);
		boolean setQtyZero = false;
		for (RtsHeaderDTO rtsHeaderDTO : rtsHeaderDTOs) {
			Integer cnt = partRepository.checkIfPartsExist(rtsHeaderDTO.getParts());
			//if (cnt == rtsHeaderDTO.getParts().size()) {
				RtsHeader rtsHeader = rtsHeaderRepository.findByOrderReference(rtsHeaderDTO.getOrderReference());
				if (rtsHeader == null) {
					rtsHeader = receivingServiceImpl.createRtsTask(rtsHeaderDTO);
					recordFile.setProcessed(true);
					downloadFileRepo.save(recordFile);
				} else {
					logger.info("File already processed");
				}
				if (rtsHeader.getOrderType().equalsIgnoreCase(ConstantHelper.FAB_RTS)) {
					// Auto process and send message

					for (RtsBody rtsbody : rtsHeader.getRtsBodies()) {
						receiptInfo.createInventoryAndTransactionHistoryForAutoRTSTasks(rtsHeader,
								ConstantHelper.FAB_RTS_DEFAULT_LOCATION);
						receiptInfo.processRTSBodiesAndDetails(rtsHeader, ConstantHelper.SYSUSER,
								rtsbody.getPartNumber(), rtsHeader.getOrderType(), true, setQtyZero);
					}

				}
//			} else {
//				logger.info("Skipping file. Parts not configured " + rtsHeaderDTO.getOrderReference());
//				recordFile.setProcessed(false);
//				downloadFileRepo.save(recordFile);
//			}
		}
	}

	
	@Override
	/***
	 * Mark the status deleted for all Headers/body/details related with GRN
	 * from RVS file
	 * 
	 * 
	 */
	public void deleteReceipt(File file) throws IOException, ParseException {

		recordFileRead(file.getName(), ConstantHelper.RVS_FILE_JOB_USER, ConstantHelper.SYSUSER);
		List<String> grnsToDelete = ReceiptFileHandlerHelper.readRVSFile(file);
		for (String grn : grnsToDelete) {

			receivingServiceImpl.deleteReceipt(grn);

		}

	}
	
	
	
	@Override
	/****
	 * Read the RELOC file and create corresponding receipts
	 * 
	 * 
	 */
	public void readReloc(File file) throws IOException, ParseException {

		DownloadFile recordFile = recordFileRead(file.getName(), ConstantHelper.RELOC_FILE_TYPE, ConstantHelper.SYSUSER);
		try {
			List<RelocTaskDTO> relocTasksDTO = ReceiptFileHandlerHelper.readReloc(file);

			for (RelocTaskDTO relocTaskDTO : relocTasksDTO) {
				List<String> parts = receivingServiceImpl.createRelocTask(relocTaskDTO);
				recordFile.setProcessed(true);
				downloadFileRepo.save(recordFile);
				if(parts!= null && parts.size() > 0){
					logger.info("Calling allocation for parts");
					receiptInfo.callAllocateForNewStock(parts);
				
				}
			}
		} catch (VantecReceivingException e) {
			recordFile.setProcessed(false);
			downloadFileRepo.save(recordFile);
		}
	}
	
	
	@Override
	/****
	 * Read the Change PNO  file and create corresponding receipts
	 * 
	 * 
	 */
	public void readChgPno(File file) throws IOException, ParseException {

		DownloadFile recordFile = recordFileRead(file.getName(), ConstantHelper.CHGPNO_FILE_TYPE,
				ConstantHelper.SYSUSER);

		List<ChangePnoDTO> changePnos = ReceiptFileHandlerHelper.readChgPno(file);

		for (ChangePnoDTO changePnoDTO : changePnos) {
			boolean response = receivingServiceImpl.processChangePartNo(changePnoDTO);
			recordFile.setProcessed(response);
			downloadFileRepo.save(recordFile);
		}

	}
	
	/****
	 * @param filename
	 * 
	 * Update the file name processed in DB
	 */
	private DownloadFile recordFileRead(String fileName, String type, String user){
		DownloadFile file = new DownloadFile();
		file.setCreatedBy(user);
		file.setDateCreated(new Date());
		file.setFilename(fileName);
		file.setType(type);
		file.setProcessed(false);
		
		file = downloadFileRepo.save(file);		
		return file;
		
	}

}
