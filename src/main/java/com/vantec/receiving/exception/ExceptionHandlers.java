package com.vantec.receiving.exception;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.vantec.receiving.controller.RDTController;

@ControllerAdvice
public class ExceptionHandlers extends BaseExceptionHandler {
	private static Logger LOGGER = LoggerFactory.getLogger(RDTController.class);


	@ExceptionHandler(EntityNotFoundException.class)
	@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Not able to find entity")
	public void handleEntityNotFoundException(EntityNotFoundException ex) {
		LOGGER.error("Entity not found "+ex.getMessage());
	}
	
	

}
