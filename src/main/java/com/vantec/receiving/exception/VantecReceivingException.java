package com.vantec.receiving.exception;

public class VantecReceivingException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public VantecReceivingException(String message,Throwable cause) {
        super(message,cause);
    }

//	private static final long serialVersionUID = 1L;
//	private ErrorDetails errorResponse;
//	
//	public VantecReceivingException(ErrorDetails errorResponse) {
//		super();
//		this.errorResponse = errorResponse;
//	}
//
//
//
//	public ErrorDetails getErrorResponse() {
//		return errorResponse;
//	}
	
	

}
