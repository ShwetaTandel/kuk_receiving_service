package com.vantec.receiving.exception;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
// @RestController
@Order(Ordered.LOWEST_PRECEDENCE)
public class BaseExceptionHandler extends ResponseEntityExceptionHandler {

	/*@ExceptionHandler(Exception.class)
	public void handleError(Exception e, HttpServletResponse response) throws IOException { // //ErrorDetails
		//errorDetails = new ErrorDetails(HttpStatus.BAD_REQUEST.value(), "Nested is " + e.getMessage());
		response.sendError(HttpStatus.BAD_REQUEST.value(), e.getMessage());

	}*/
	//for all runtime exceptions
	/*@ExceptionHandler(Exception.class)
	public final ResponseEntity<ErrorDetails> handleAllExceptions(Exception ex, WebRequest request) {
		
		ErrorDetails errorDetails = new ErrorDetails(new Date(),request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}*/
	
}
