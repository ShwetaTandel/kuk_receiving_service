package com.vantec.receiving.dao;

import java.util.List;

import com.vantec.receiving.entity.Company;
import com.vantec.receiving.entity.DefaultSetting;
import com.vantec.receiving.entity.DocumentStatus;
import com.vantec.receiving.entity.InventoryMaster;
import com.vantec.receiving.entity.Location;
import com.vantec.receiving.entity.Pallet;
import com.vantec.receiving.entity.Part;
import com.vantec.receiving.entity.ReceiptHeader;
import com.vantec.receiving.entity.TransactionType;
import com.vantec.receiving.exception.VantecReceivingException;

public interface RDTDAO {
	
	public DocumentStatus findStatusByID(int id);
	
	public List<ReceiptHeader> findReceiptWithTrailerAndStatus(String trailerRef,DocumentStatus status) throws VantecReceivingException;

	public Location findLocationByHashCode(String hashCode);
	
	public TransactionType findTranscationByCode(String transactionTypeCode);
	
	public Part findPartByPartNumber(String partNumber);
	
	//public Integer findMaxLengthOfSerial(String serialNumber);

	public Boolean validateFromLocationByTransaction(Location location, TransactionType transaction);
	
	public Boolean validateToLocationByTransaction(Location location, TransactionType transaction);
	
   // public Prefix validatePrefix(String fieldName,String prefix,TransactionType transactionType);
    
    public InventoryMaster findInvFromPartSerial(Part part,String serialReference);
	
    public Company getCompany(String company);
	
	public List<DefaultSetting> getDefaultSettings();

	public String createPlt(String palletCode);

	Boolean updateRecieptStatus(ReceiptHeader receiptHeaders, DocumentStatus status);

	String createVirtualPlt(String palletTypeName);

}
