package com.vantec.receiving.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.receiving.command.CustomValueGeneratorCommand;
import com.vantec.receiving.command.PltNumberGenerator;
import com.vantec.receiving.entity.Company;
import com.vantec.receiving.entity.DefaultSetting;
import com.vantec.receiving.entity.DocumentStatus;
import com.vantec.receiving.entity.InventoryMaster;
import com.vantec.receiving.entity.Location;
import com.vantec.receiving.entity.Pallet;
import com.vantec.receiving.entity.Part;
import com.vantec.receiving.entity.ReceiptHeader;
import com.vantec.receiving.entity.TransactionType;
import com.vantec.receiving.exception.VantecReceivingException;

@Transactional
@Repository
public class RDTDAOImpl implements RDTDAO {

	private static Logger LOGGER = LoggerFactory.getLogger(RDTDAOImpl.class);

	@PersistenceContext
	private EntityManager entityManager;
	

	@SuppressWarnings("unchecked")
	@Override
	public List<ReceiptHeader> findReceiptWithTrailerAndStatus(String trailerRef, DocumentStatus status)
			throws VantecReceivingException {
		List<ReceiptHeader> receipt = null;
		receipt = entityManager.createNamedQuery("findReceiptWithTrailerAndStatus")
				 .setParameter("trailerRef", trailerRef)
				 .setParameter("status", status)
				 .getResultList();
		if (null == receipt || receipt.size() == 0) {
			LOGGER.error("Not able to find information");
		}

		return receipt;
		
	}
	
	public DocumentStatus findStatusByID(int id){
		DocumentStatus status = entityManager.find(DocumentStatus.class, id);
		return status;
	}

	@Override
	public Location findLocationByHashCode(String hashCode) {
		 
		Location location = (Location) entityManager.createNamedQuery("findLocationByHashCode").setParameter("hashCode", hashCode).getSingleResult();
		return location;
	}
	
	@Override
	public Boolean validateFromLocationByTransaction(Location location,TransactionType transaction) {
		Boolean isValid;
		List result = entityManager.createNamedQuery("validateFromLocationByTransaction")
				        .setParameter("location", location)
				        .setParameter("transaction", transaction)
				        .getResultList();
		if(result != null){
			isValid = true;
		}else{
			isValid = false;
		}
		return isValid;
	}
	
	@Override
	public Boolean validateToLocationByTransaction(Location location,TransactionType transaction) {
		Boolean isValid;
		List result = entityManager.createNamedQuery("validateToLocationByTransaction")
				        .setParameter("location", location)
				        .setParameter("transaction", transaction)
				        .getResultList();
		if(result != null){
			isValid = true;
		}else{
			isValid = false;
		}
		return isValid;
	}
	
	
	
//	@Override
//	public Prefix validatePrefix(String fieldName, String prefixName, TransactionType transactionType) {
//		Boolean isValid;
//		Prefix result = (Prefix)entityManager.createNamedQuery ("validatePrefix")
//				        .setParameter("fieldName", fieldName)
//				        .setParameter("prefixName", prefixName)
//				        .setParameter("transactionId", transactionType)
//				        .getSingleResult();
//		return result;
//	}

	@Override
	public TransactionType findTranscationByCode(String transactionTypeCode) {
		
		TransactionType transactionType = (TransactionType) entityManager.createNamedQuery("findTranscationByCode").setParameter("transactionTypeCode", transactionTypeCode).getSingleResult();
		
		return transactionType;
	}



	@Override
	public String createVirtualPlt(String palletTypeName) {
		Pallet plt = new Pallet();
		entityManager.persist(plt);
		Long pltId = plt.getId();
		CustomValueGeneratorCommand<String> customValueGeneratorCommand = new PltNumberGenerator();
		String pltNumber = customValueGeneratorCommand.generate(pltId,palletTypeName);
		plt.setPltNumber(pltNumber);
		plt.setDateCreated(new Date());
		entityManager.persist(plt);

		return pltNumber;
	}
	
	@Override
	public String createPlt(String palletCode) {
		Pallet plt = new Pallet();
		plt.setPltNumber(palletCode);
		plt.setDateCreated(new Date());
		entityManager.persist(plt);

		return palletCode;
	}
	
	
	@Override
	public Company getCompany(String companyName) {
		Company companySettings = (Company) entityManager.createNamedQuery("getDefaultSettingsForCompany").setParameter("companyName", companyName).getSingleResult();
		return companySettings;
	}

//	@Override
//	public List<Prefix> getPrefix() {
//		List<Prefix> prefixes = entityManager.createNamedQuery("getPrefix").getResultList();
//		return prefixes;
//	}
 
	@Override
	public Part findPartByPartNumber(String partNumber) {
		Part part = (Part) entityManager.createNamedQuery("findPartByPartNumber").setParameter("partNumber", partNumber).getSingleResult();
		return part;
	}
	
	@Override
	public InventoryMaster findInvFromPartSerial(Part part, String serialReference) {
		InventoryMaster inv = (InventoryMaster) entityManager.createNamedQuery("findInvFromPartSerial")
				.setParameter("part", part)
				.setParameter("serialReference", serialReference)
				.getSingleResult();
		return inv;
	}

	@Override
	public List<DefaultSetting> getDefaultSettings() {
		List<DefaultSetting> defaultSetting = entityManager.createNamedQuery("getDefaultSetting").getResultList();
		return defaultSetting;
	}

	@Override
	public Boolean updateRecieptStatus(ReceiptHeader receiptHeader,DocumentStatus status) {
			receiptHeader.setDocumentStatus(status);
			entityManager.merge(receiptHeader);
		return true;
	}
	
	

	

	
	
	

}
