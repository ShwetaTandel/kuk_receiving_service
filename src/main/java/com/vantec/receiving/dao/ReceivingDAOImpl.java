package com.vantec.receiving.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.receiving.command.CustomValueGeneratorCommand;
import com.vantec.receiving.command.DocumentReferenceGenerator;
import com.vantec.receiving.entity.DocumentStatus;
import com.vantec.receiving.entity.ProductType;
import com.vantec.receiving.entity.ReceiptBody;
import com.vantec.receiving.entity.ReceiptDetail;
import com.vantec.receiving.entity.ReceiptHeader;
import com.vantec.receiving.entity.Vendor;
import com.vantec.receiving.model.ReceiptHeaderDTO;
import com.vantec.receiving.model.TransactionDetailRequest;
import com.vantec.receiving.repository.PartRepository;
import com.vantec.receiving.repository.VendorRepository;
import com.vantec.receiving.util.ConstantHelper;

@Transactional
@Repository
public class ReceivingDAOImpl implements ReceivingDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	PartRepository partRepository;
	
	@Autowired
	VendorRepository vendorRepository;

	public boolean createRequest(ReceiptHeader receiptHeader) {
		
		
		entityManager.persist(receiptHeader);

		CustomValueGeneratorCommand<String> customValueGeneratorCommand = new DocumentReferenceGenerator();

		String documentReference = customValueGeneratorCommand.generate(receiptHeader.getId(),ConstantHelper.RECEIPT_REFERENCE_PREFIX);
		receiptHeader.setDocumentReference(documentReference);
		entityManager.persist(receiptHeader);

		List<ReceiptBody> receiptBodies = receiptHeader.getReceiptBodies();

		for (ReceiptBody receiptBody : receiptBodies) {
			receiptBody.setReceiptHeader(receiptHeader);
			receiptBody.setDocumentReference(documentReference);
			
			
			try{
				entityManager.persist(receiptBody);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			List<ReceiptDetail> receiptDetails = receiptBody.getReceiptDetails();

			for (ReceiptDetail receiptDetail : receiptDetails) {
				receiptDetail.setReceiptBody(receiptBody);
				receiptDetail.setDocumentReference(documentReference);
				entityManager.persist(receiptDetail);
			}
		}
		return true;
	}

	public boolean updateRequest(ReceiptHeader receiptHeader) {

		entityManager.persist(receiptHeader);


		ReceiptHeader receiptHeader2 = entityManager.find(ReceiptHeader.class, receiptHeader.getDocumentReference());
		receiptHeader2.setDateUpdated(new Date());
		receiptHeader2.setExpectedDeliveryDate(receiptHeader.getExpectedDeliveryDate());
		receiptHeader2.setHaulierReference(receiptHeader.getHaulierReference());
		receiptHeader2.setProductType(receiptHeader.getProductType());
		List<ReceiptBody> receiptBodies = receiptHeader2.getReceiptBodies();
		for(ReceiptBody receiptBody: receiptBodies)
		{
			ReceiptBody receiptBody2 = new ReceiptBody();
		}
		receiptHeader2.setReceiptBodies(receiptHeader.getReceiptBodies());
		receiptHeader2.setDocumentStatus(receiptHeader.getDocumentStatus());
		receiptHeader2.setUpdatedBy(receiptHeader.getUpdatedBy());
		receiptHeader2.setVendor(receiptHeader.getVendor());




		entityManager.merge(receiptHeader);
		return true;
	}

	public List<ReceiptHeader> getAllReceiptDocs(int pageNumber, int numberOfResults) {
		if (pageNumber == 0) {
			throw new IllegalStateException();
		}
		Query query = entityManager.createQuery("From ReceiptHeader");
		query.setFirstResult((pageNumber - 1) * numberOfResults);
		query.setMaxResults(numberOfResults);
		List<ReceiptHeader> receipts = query.getResultList();
		return receipts;
	}
	
	public boolean updateDocHeader(ReceiptHeader receiptHeader){
		
		ReceiptHeader receiptHeader2 = entityManager.find(ReceiptHeader.class, receiptHeader.getDocumentReference());
		return true;
	}
	
	public boolean createReceiptHeader(){
		
		
		
		return false;
	}
	
	public List<ReceiptHeader> getAllChildHeaders(String haulierReference){
		
		
		@SuppressWarnings("unchecked")
		List<ReceiptHeader> receiptHeaders = entityManager.createNamedQuery("getAllChildHeaders")
												 .setParameter("haulierReference", haulierReference)
												 .getResultList();
		
		return receiptHeaders;
	}
	
	public List<ReceiptHeader> getAllActiveChildHeaders(String haulierReference, DocumentStatus status){
		
		
		@SuppressWarnings("unchecked")
		List<ReceiptHeader> receiptHeaders = entityManager.createNamedQuery("getAllActiveChildHeaders")
												 .setParameter("haulierReference", haulierReference)
												 .setParameter("status", status)
												 .getResultList();
		
		return receiptHeaders;
	}
	
	
//	public ReceiptDetail updateReceiptDetail(TransactionDetailRequest txn){
//		ReceiptDetail receiptDetail = entityManager.find(ReceiptDetail.class, txn.getHeader());
//		entityManager.persist(receiptDetail);
//		return receiptDetail;
//	}
	
	public Boolean deleteReceiptDetail(String documentReference,String serialReference){
		
		Boolean isDeleted = false;
		int value = entityManager.createNamedQuery("deleteReceiptDetail")
				                         .setParameter("documentReference", documentReference)
				                         .setParameter("serialReference", serialReference)
				                         .executeUpdate();
		if(value == 1){
			isDeleted = true;
		}
		return isDeleted;
	}
	
	public Boolean deleteReceiptBody(String documentReference){
		
		Boolean isDeleted = false;
		int value = entityManager.createNamedQuery("deleteReceiptBody")
				                         .setParameter("documentReference", documentReference)
				                         .executeUpdate();
		if(value == 1){
			isDeleted = true;
		}
		return isDeleted;
	}
	
	
	public Boolean deleteReceiptHeader(String documentReference) {
		
		Boolean isDeleted = false;
		int value = entityManager.createNamedQuery("deleteReceiptHeader")
				                         .setParameter("documentReference", documentReference)
				                         .executeUpdate();
		if(value == 1){
			isDeleted = true;
		}
		return isDeleted;
	}
	
//	public void deleteInventory(String partNumber,String serialReference){
//		int value = entityManager.createNamedQuery("deleteInventory")
//				                         .setParameter("partNumber", partNumber)
//				                         .setParameter("serialReference", serialReference)
//				                         .executeUpdate();
//	}
	
	public void editReceiptHeader(ReceiptHeaderDTO receiptHeaderDTO,ReceiptHeader receiptHeader) throws ParseException{
		
//		receiptHeader.setDateUpdated(new Date());
//		receiptHeader.setUpdatedBy(receiptHeaderDTO.getUpdatedBy());
//		String dateStr = receiptHeaderDTO.getExpectedDeliveryDate();
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		Date expectDeliveryDate = sdf.parse(dateStr);
//		receiptHeader.setExpectedDeliveryDate(expectDeliveryDate);
//		receiptHeader.setHaulierReference(receiptHeaderDTO.getHaulierReference());
//		receiptHeader.setVendor(vendorRepository.findByVendorReferenceCode(receiptHeaderDTO.getVendorCode()));
//		receiptHeader.setJisInd(receiptHeaderDTO.getJisInd()?"D":"");
//		entityManager.persist(receiptHeader);
	}

	@Override
	public ReceiptHeader saveUpdateReceiptHeader(TransactionDetailRequest txn,ReceiptHeader header) {
		ReceiptHeader receiptHeader = null;
		if(header != null){
			receiptHeader = header;
		}else{
			receiptHeader = new ReceiptHeader();
			entityManager.persist(receiptHeader);
			CustomValueGeneratorCommand<String> customValueGeneratorCommand = new DocumentReferenceGenerator();
			String documentReference = customValueGeneratorCommand.generate(receiptHeader.getId(),ConstantHelper.RECEIPT_REFERENCE_PREFIX);
			receiptHeader.setDocumentReference(documentReference);
			receiptHeader.setDateCreated(new Date());
			receiptHeader.setCreatedBy(txn.getCurrentUser());
		}
		receiptHeader.setDateUpdated(new Date());
		receiptHeader.setUpdatedBy(txn.getCurrentUser());
		entityManager.persist(receiptHeader);
		return receiptHeader;
	}

	@SuppressWarnings("unused")
	@Override
	public ReceiptBody saveUpdateReceiptBody(ReceiptHeader header,ReceiptBody body,TransactionDetailRequest txn) {
		
		ReceiptBody receiptBody = null;
		if(body != null){
			receiptBody = body;
			receiptBody.setReceivedQty(receiptBody.getReceivedQty() + txn.getQuantity());
		}else{
			receiptBody = new ReceiptBody();
			receiptBody.setReceiptHeader(header);
			receiptBody.setDateCreated(new Date());
			receiptBody.setCreatedBy(txn.getCurrentUser());
			receiptBody.setPartNumber(txn.getPartNumber());
			receiptBody.setPart(txn.getPart());
			receiptBody.setAdvisedQty(0.00);
			receiptBody.setDocumentReference(header.getDocumentReference());
			receiptBody.setProductType(header.getProductType());
			receiptBody.setReceivedQty(txn.getQuantity());
			
		}
		receiptBody.setDateUpdated(new Date());
		receiptBody.setUpdatedBy(txn.getCurrentUser());
		
		receiptBody.setDifference(receiptBody.getReceivedQty() - receiptBody.getAdvisedQty());

		
		entityManager.persist(receiptBody);
		return receiptBody;
	} 

	@Override
	public ReceiptDetail saveUpdateReceiptDetail(ReceiptHeader header,ReceiptBody body,ReceiptDetail detail,TransactionDetailRequest txn) {
		ReceiptDetail receiptDetail = null;
		if(detail != null){
			receiptDetail = detail;
			receiptDetail.setReceivedQty(receiptDetail.getReceivedQty() + txn.getQuantity());
		}else{
			receiptDetail = new ReceiptDetail();
		    receiptDetail.setReceiptBody(body);
			receiptDetail.setDateCreated(new Date());
			receiptDetail.setCreatedBy(txn.getCurrentUser());
			receiptDetail.setSerialReference(txn.getSerialReference());
			receiptDetail.setPartNumber(txn.getPartNumber());
			receiptDetail.setAdvisedQty(0.00);
			receiptDetail.setDocumentReference(header.getDocumentReference());
			receiptDetail.setProductType(header.getProductType());
			receiptDetail.setRanOrder(txn.getRanOrder());
			receiptDetail.setPart(txn.getPart());
			receiptDetail.setReceivedQty(txn.getQuantity());
		}
		
		receiptDetail.setDateUpdated(new Date());
		receiptDetail.setUpdatedBy(txn.getCurrentUser());
		receiptDetail.setDifference(receiptDetail.getReceivedQty() - receiptDetail.getAdvisedQty());
		
		
		entityManager.persist(receiptDetail);
		return receiptDetail;
	}

	@Override
	public ReceiptBody findBodyFromHeaderAndPart(ReceiptHeader receiptHeader, String partNumber) {
		ReceiptBody receiptBody;
		try{
		receiptBody = (ReceiptBody) entityManager.createNamedQuery("getBodyFromHeaderAndPart")
												 .setParameter("receiptHeader", receiptHeader)
												 .setParameter("partNumber", partNumber)
												 .getSingleResult();
		}catch(NoResultException noResultException){
		 //			No result was found ..Log it and return null
			return null;
		}
		
		return receiptBody;
	}

	@Override
	public ReceiptDetail findDetailFromBodyAndSerial(ReceiptBody receiptBody, String serialReference) {
		ReceiptDetail receiptDetail;
		try{
		 receiptDetail = (ReceiptDetail) entityManager.createNamedQuery("getDetailFromBodyAndSerial")
					 .setParameter("receiptBody", receiptBody)
					 .setParameter("serialReference", serialReference)
					 .getSingleResult();
			
		}catch(NoResultException noResultException){
			 //			No result was found ..Log it and return null
				return null;
		}
		
		
		return receiptDetail;
	}

	@Override
	public ReceiptHeader findHeaderFromCustomerReference(String customerReference) {
		ReceiptHeader receiptHeader;
		try{
			receiptHeader = (ReceiptHeader) entityManager.createNamedQuery("getHeaderFromCustomerReference")
												 .setParameter("customerReference", customerReference)
												 .getSingleResult();
		}catch(NoResultException noResultException){
		 //			No result was found ..Log it and return null
			return null;
		}
		
		return receiptHeader;
	}
	
	@Override
	public ProductType findProductType(String productName) {
		ProductType productType;
		try{
			productType = (ProductType) entityManager.createNamedQuery("getProductType")
												 .setParameter("productName", productName)
												 .getSingleResult();
		}catch(NoResultException noResultException){
		 //			No result was found ..Log it and return null
			return null;
		}
		
		return productType;
	}
	
	@Override
	public DocumentStatus findDocumentStatus(String status) {
		DocumentStatus documentStatus;
		try{
			documentStatus = (DocumentStatus) entityManager.createNamedQuery("getDocumentStatus")
												 .setParameter("status", status)
												 .getSingleResult();
		}catch(NoResultException noResultException){
		 //			No result was found ..Log it and return null
			return null;
		}
		
		return documentStatus;
	}

	@Override
	public Vendor findVendor(String chargeType) {
		Vendor vendor;
		try{
			vendor = (Vendor) entityManager.createNamedQuery("getVendor")
												 .setParameter("chargeType", chargeType)
												 .getSingleResult();
		}catch(NoResultException noResultException){
		 //			No result was found ..Log it and return null
			return null;
		}
		return null;
	}

	@Override
	public boolean addVendor(Vendor vendor) {
		entityManager.persist(vendor);
		return true;
	}

//	@Override
//	public boolean createCharge(ChargeDTO chargeDTO) {
//		entityManager.persist(vendor);
//		return true;
//	}
//
//	@Override
//	public boolean editCharge(ChargeDTO chargeDTO) {
//		entityManager.persist(vendor);
//		return true;
//	}

	
	
	
	
	
	
	

}
