package com.vantec.receiving.dao;

import java.text.ParseException;
import java.util.List;

import com.vantec.receiving.entity.DocumentStatus;
import com.vantec.receiving.entity.ProductType;
import com.vantec.receiving.entity.ReceiptBody;
import com.vantec.receiving.entity.ReceiptDetail;
import com.vantec.receiving.entity.ReceiptHeader;
import com.vantec.receiving.entity.Vendor;
import com.vantec.receiving.model.ReceiptHeaderDTO;
import com.vantec.receiving.model.TransactionDetailRequest;


public interface ReceivingDAO {	

	boolean createRequest(ReceiptHeader receiptHeader);
	
	List<ReceiptHeader> getAllReceiptDocs(int startRow,int numberOfResults);
	
	List<ReceiptHeader> getAllChildHeaders(String haulierReference);

	boolean createReceiptHeader();
	
    public ReceiptHeader saveUpdateReceiptHeader(TransactionDetailRequest txn , ReceiptHeader header);
	
	public ReceiptBody saveUpdateReceiptBody(ReceiptHeader header,ReceiptBody body,TransactionDetailRequest txn);
	
	public ReceiptDetail saveUpdateReceiptDetail(ReceiptHeader header,ReceiptBody body,ReceiptDetail detail,TransactionDetailRequest txn);
	
	ReceiptHeader findHeaderFromCustomerReference(String customerReference);

	ReceiptBody findBodyFromHeaderAndPart(ReceiptHeader receiptHeader, String partNumber);
	
	ReceiptDetail findDetailFromBodyAndSerial(ReceiptBody receiptBody, String serialNumber);

	ProductType findProductType(String productName);

	DocumentStatus findDocumentStatus(String status);
	
	Vendor findVendor(String chargeType);
	
	public Boolean deleteReceiptDetail(String documentreference,String serialReference);

//	/void deleteInventory(String partNumber, String serialReference);

	Boolean deleteReceiptBody(String documentReference);

	Boolean deleteReceiptHeader(String documentReference);
	
	void editReceiptHeader(ReceiptHeaderDTO receiptHeaderDTO,ReceiptHeader receiptHeader) throws ParseException;

	boolean addVendor(Vendor vendor);

	List<ReceiptHeader> getAllActiveChildHeaders(String haulierReference, DocumentStatus closeStatus);

//	boolean createCharge(ChargeDTO chargeDTO);
//
//	boolean editCharge(ChargeDTO chargeDTO);
	

}
