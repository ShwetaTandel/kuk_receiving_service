package com.vantec.receiving.util;

public interface ConstantHelper {

	/**Document status table constants**/
	public static final String DOCUMENT_STATUS_COMPLETED = "COMPLETE";
	public static final String DOCUMENT_STATUS_TRANSMITTED = "TRANSMITTED";
	public static final String DOCUMENT_STATUS_QUARANTINE = "QUARANTINE";
	public static final String DOCUMENT_STATUS_OPEN = "OPEN";
	public static final String DOCUMENT_STATUS_DELETED = "DELETED"; 
	public static final String DOCUMENT_STATUS_CLOSED =  "CLOSED";
	
	public static final String CUSTOMER_lOCATION_AREA_QA =  "quarantine";
	public static final String CUSTOMER_lOCATION_AREA_STOCK =  "stock";
	public static final String CUSTOMER_lOCATION_AREA_INSP =  "inspection";
	public static final String CUSTOMER_lOCATION_AREA_RETURN =  "return";
	public static final String LOCATION_TYPE_LOCATION_AREA_STORAGE =  "storage";
	
	
	
	public static final String INVENTORY_STATUS_QA = "QTINE"; 
	public static final String INVENTORY_STATUS_OVERBOOKING =  "OVERBOOKING";
	
	
	public static final String RECEIPT_REFERENCE_PREFIX =  "RCP";
	public static final String RELOC_REFERENCE_PREFIX =  "RELOC";

	/**Inventory status table constants**/
	public static final String INVENTORY_STATUS_OK = "OK";
	
	/**KUK  constants**/
	public static final String KUK_VENDOR_CODE = "KUK";
	
	public static final String NOT_EXIST_PART_NUMBER = "notexist";
	public static final String LOCATION_NEW = "NEW";
	
	/**RTS  constants**/
	
	public static final String REWORK_RTS = "REWORK-RTS";
	public static final String REWORK_EMERG_RTS = "REWORK-EMERG-RTS";
	public static final String EXPEDITE_RTS = "EXPEDITE-RTS";
	public static final String EXPEDITE_EMERG_RTS = "EXPEDITE-EMERG-RTS";
	public static final String UNPLANNED_RTS = "UNPLANNED-RTS";
	public static final String FAB_RTS = "FAB-RTS";
	
	public static final String RTS_DEFAULT_LOCATION= "JDR001";
	public static final String FAB_RTS_DEFAULT_LOCATION= "REVO01";
	public static final String UNPLANNED_RTS_FILE_RETURN_TYPE_GENERAL= "UNRTS";
	public static final String UNPLANNED_RTS_FILE_RETURN_TYPE_FOR_REASON_CODE_18= "RTSCC";
	public static final String UNPLANNED_REASON_CODE_RTS_PROJ= "RTS_PROJ";
	
	public static final String UNPLANNED_RTS_SERIAL_PREFIX= "URTS";
	
	public static final String UNPLANNED_RTS_FILE_RETURN_ORIGIN = "WA";
	
	
	/**Transaction codes**/
	public static final String UNPLANNED_RTS_TRANSACTION_SHORT_CODE = "URTS";
	public static final String FAB_RTS_TRANSACTION_SHORT_CODE = "FRTS";
	public static final String RTS_TRANSACTION_SHORT_CODE = "FSRTS";
	public static final String UNPLANNED_RTS_TRANSACTION_REFERNCE = "UNPLANNED RTS";
	public static final String FAB_RTS_TRANSACTION_REFERENCE = "FAB RTS";
	public static final String SA_OUT_SHORT_CODE = "SA-OUT";
	public static final String RECEIPT_OUT_SHORT_CODE = "RCPOUT";
	public static final String RECEIPT_TRANSACTION = "RCVS";

	
	/**File  constants**/
	public static final String FAB_RTS_INDICATOR = "M";
	public static final String REWORK_RTS_INDICATOR = "CR";
	public static final String FAB_RTS_POSITION_INDICATOR = "0";
	public static final String GRN_TYPE_CONTAINER = "CONTAINER";
	public static final String GRN_TYPE_LOCAL = "LOCAL";
	public static final String RECEIPT_FILE_HEADER_INDICATOR = "H";
	public static final String RECEIPT_FILE_DETAIL_INDICATOR = "D";
	public static final String BAAN_FILE_START_FILE_RECORD = "Start of file";
	public static final String BAAN_FILE_END_FILE_RECORD = "End of file";
	public static final String BAAN_FILE_SIMPLE_DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
	public static final String BAAN_FILE_SIMPLE_DATE_FORMAT = "dd/MM/yyyy";
	public static final String RTS_FILE_MODE = "A";
	
	public static final String RETURN_FILE_TYPE_OFFLOD = "OFFLOD";
	public static final String RETURN_FILE_TYPE_UNPLANNED_RTS = "STOCKA";
	public static final String RETURN_FILE_TYPE_PLANNED_RTS = "PICKED";
	public static final String RETURN_FILE_TYPE_STKMV = "CHGSTK";
	public static final String RETURN_FILE_TYPE_RELOC = "RELOC"; 
	
	/**General  constants**/
	public static final String SYSUSER = "SYS";
	public static final String RVS_FILE_JOB_USER = "RVS File Job";
	public static final String TMP_FILE_EXT = ".tmp";
	public static final String TEXT_FILE_EXT = ".txt";
	public static final String GRN_FILE_TYPE = "GRN File Job";
	public static final String RTS_FILE_TYPE = "RTS File Job";
	public static final String RELOC_FILE_TYPE = "RELOC File Job";
	public static final String CHGPNO_FILE_TYPE = "CHGPNO File Job";
	
	
	
	public static final String RECEIPT_DETAIL__SERIAL_PREFIX= "RCPLBL";
	public static final String OFFSITE_STORAGE_SERIAL_PREFIX= "STKTNFR";
	

	public static final String VENDOR_KUK_REF_CODE = "KUK";
	

}
