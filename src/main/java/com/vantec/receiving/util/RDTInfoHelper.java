package com.vantec.receiving.util;

import java.util.ArrayList;
import java.util.List;

import com.vantec.receiving.entity.Company;
import com.vantec.receiving.entity.DefaultSetting;
import com.vantec.receiving.entity.Part;
import com.vantec.receiving.model.CompanyDTO;
import com.vantec.receiving.model.DefaultSettingDTO;
import com.vantec.receiving.model.PartDTO;

public final class RDTInfoHelper {
	
	public static CompanyDTO convertCompanyToModel(Company company) {
		CompanyDTO companyDTO = new CompanyDTO();
		companyDTO.setAddress(company.getAddress());
		companyDTO.setCreatedBy(company.getCreatedBy());
		companyDTO.setDateCreated(company.getDateCreated());
		companyDTO.setDateUpdated(company.getDateUpdated());
		companyDTO.setInterfaceActive(company.getInterfaceActive());
		companyDTO.setInterfaceInput(company.getInterfaceInput());
		companyDTO.setInterfaceOutPut(company.getInterfaceOutput());
		companyDTO.setIpoStorage(company.getMixedPartStorage());
		companyDTO.setMaxBoxQty(company.getMaxBoxPieceQty());
		companyDTO.setMaxPartNumberLength(company.getMaxPartNumberLength());
		companyDTO.setMaxRanNumberLength(company.getMaxRanNumberLength());
		companyDTO.setMaxSerialNumberLength(company.getMaxSerialNumberLength());
		companyDTO.setMinBoxQty(company.getMinBoxQty());
		companyDTO.setMinPartNumberLength(company.getMinPartNumberLength());
		companyDTO.setMinRanNumberLength(company.getMinRanNumberLength());
		companyDTO.setMinSerialNumberLength(company.getMinSerialNumberLength());
		companyDTO.setName(company.getCompanyName());
		companyDTO.setQtyDecimalPlace(company.getQtyDecimalPlace());
		companyDTO.setShortCode(company.getCompanyReferenceCode());
		companyDTO.setToDecant(company.getToDecant());
		companyDTO.setToQA(company.getToQA());
		companyDTO.setUpdatedBy(company.getUpdatedBy());
		companyDTO.setVirtualPalletLbl(company.getVirtualPalletLbl());
		return companyDTO;
	}
	
	public static List<DefaultSettingDTO> convertDefaultSettingModel(List<DefaultSetting> defaultSettings) {
		List<DefaultSettingDTO> settings = new ArrayList<DefaultSettingDTO>();
		for(DefaultSetting defaultSetting:defaultSettings){
			DefaultSettingDTO setting = new DefaultSettingDTO();
			setting.setFieldName(defaultSetting.getFieldName());
			setting.setMax(defaultSetting.getMax());
			setting.setMin(defaultSetting.getMax());
			setting.setPrefixName(defaultSetting.getPrefixName());
			setting.setSystemGenerated(defaultSetting.getSystemGenerated());
			settings.add(setting);
			
		}
		return settings;
	}
	
	public static PartDTO convertPartModel(Part part) {
		PartDTO partDTO = new PartDTO();
		//partDTO.setCountOnRCpt(part.isCountOnRCpt());
		//partDTO.setDecant(part.isDecant());
		//partDTO.setFixedLocationCode(part.getFixedLocationTypeId());
	    //  partDTO.setInspect(part.isInspect());
		
		//partDTO.setLoosePcsEqualTo1(part.getLoosePcsEqualTo1());
		
		//partDTO.setMiniStock(part.getMiniStock());
		
		partDTO.setPartDescription(part.getPartDescription());
		
		//partDTO.setPartFamily(part.getPartFamily());
		
		
		partDTO.setPartNumber(part.getPartNumber());
		//partDTO.setShelfLife(part.getShelfLife());
		
	    //	partDTO.setVandorPartNumber(part.getVandorPartNumber());
		
		partDTO.setWiCode(part.getWiCode());
		
		return partDTO;
	}
	

}
