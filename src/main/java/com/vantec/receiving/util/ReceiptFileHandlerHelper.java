package com.vantec.receiving.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.vantec.receiving.model.ChangePnoDTO;
import com.vantec.receiving.model.ReceiptBodyDTO;
import com.vantec.receiving.model.ReceiptHeaderDTO;
import com.vantec.receiving.model.RelocTaskDTO;
import com.vantec.receiving.model.RtsBodyDTO;
import com.vantec.receiving.model.RtsDetailDTO;
import com.vantec.receiving.model.RtsHeaderDTO;

public final class ReceiptFileHandlerHelper {

	private static String[] splitLine(String line) {
		// Split Line at |
		line = line.replaceAll("\"", "");
		line = line.concat("|End");
		String[] values = line.split("\\|");
		return values;
	}

	public static List<ReceiptHeaderDTO> readGRNFile(File file) throws IOException {

		// Sample Lines
		// "H|111665|TCKU6256984"
		// Header|GRN Number|Container ID
		// "D|111665|1|KUK20170|2073203971|1||TCKU6256984|24|PRD000261|||1|0|N|Y|"
		// Detail||Seq Number|Advice Note|Part Number|Qty Advised|Ship Ref Case
		// No|Ship Ref Case No|SuppCode |||Qty Accepted|Qty To
		// Quarantine|Inspect Flag|Req Cnt Flag|new Items|
		FileReader fileReader = new FileReader(file);
		BufferedReader lines = new BufferedReader(fileReader);
		String line = "";
		boolean isContainer = false;

		List<ReceiptHeaderDTO> receiptHeaderDTOs = new ArrayList<ReceiptHeaderDTO>();

		ReceiptHeaderDTO receiptHeaderDTO = null;
		List<ReceiptBodyDTO> receiptBodyDTOs = null;
		ReceiptBodyDTO receiptBodyDTO = null;
		
		List<String> parts = new ArrayList<>();
		while ((line = lines.readLine()) != null) {

			String[] values = splitLine(line);
			
			if (values[0].equalsIgnoreCase(ConstantHelper.RECEIPT_FILE_HEADER_INDICATOR)) {
				if (receiptHeaderDTO != null) {
					receiptHeaderDTO.setReceiptBodyDTO(receiptBodyDTOs);
					receiptHeaderDTOs.add(receiptHeaderDTO);
				}
				receiptHeaderDTO = new ReceiptHeaderDTO();
				receiptBodyDTOs = new ArrayList<ReceiptBodyDTO>();
				receiptHeaderDTO.setGrnNumber(values[1]);
				receiptHeaderDTO.setContainerId(values[2]);
			}
			if (values[0].equalsIgnoreCase(ConstantHelper.RECEIPT_FILE_DETAIL_INDICATOR)) {
				receiptBodyDTO = new ReceiptBodyDTO();

				receiptBodyDTO.setSequenceNumber(Integer.parseInt(values[2]));
				receiptHeaderDTO.setAdviceNote(values[3]);
				receiptBodyDTO.setPartNumber(values[4]);
				receiptBodyDTO.setQuantityAdvised(values[5]);
				receiptBodyDTO.setShippingReferenceCaseReference(values[7] + values[8]);
				if(receiptBodyDTO.getShippingReferenceCaseReference()!= null && !receiptBodyDTO.getShippingReferenceCaseReference().isEmpty()){
					isContainer = true;
				}
				receiptHeaderDTO.setGrnType(isContainer == true ? ConstantHelper.GRN_TYPE_CONTAINER : ConstantHelper.GRN_TYPE_LOCAL);
				receiptBodyDTO.setSupplierCode(values[9]);
				if(!receiptHeaderDTO.getVendors().contains(values[9])){
					receiptHeaderDTO.getVendors().add(values[9]);
				}
				receiptBodyDTO.setQtyAccepted(values[12]);
				receiptBodyDTO.setQtyToQuarantine(values[13]);
				receiptBodyDTO.setInspectFlag(values[14]);
				receiptBodyDTO.setRequiredCountFlag(values[15]);
				receiptBodyDTO.setNewItems(values[16]);
				
				if (parts.contains(values[4] + values[7] + values[8])) {
					updatePartQty(receiptBodyDTOs, receiptBodyDTO);
				} else {
					receiptBodyDTOs.add(receiptBodyDTO);
					parts.add(values[4] + values[7] + values[8]);

				}
			}
			if (values[0].contains(ConstantHelper.BAAN_FILE_END_FILE_RECORD)) {
				
				receiptHeaderDTO.setReceiptBodyDTO(receiptBodyDTOs);
				receiptHeaderDTOs.add(receiptHeaderDTO);
			}

		}

		fileReader.close();
		lines.close();

		return receiptHeaderDTOs;

	}
	
	/**
	 * 
	 * @param file
	 * @return The GRN number to delete
	 * @throws IOException
	 */
	public static List<String> readRVSFile(File file) throws IOException {
		
		//"Start of file"
		//"114499"
		//"End of file - 1 records written"
		FileReader fileReader = new FileReader(file);
		BufferedReader lines = new BufferedReader(fileReader);
		String line = "";
		List<String> grnNos = new ArrayList<>();
		while ((line = lines.readLine()) != null) {

			String[] values = splitLine(line);
			if(values[0].contains(ConstantHelper.BAAN_FILE_END_FILE_RECORD) || values[0].contains(ConstantHelper.BAAN_FILE_START_FILE_RECORD)){
				//	skip the line
			}else{
				grnNos.add(values[0]);
			}
		}
		fileReader.close();
		lines.close();
		return grnNos;
		
	}
	
	/**
	 * readRTSFile
	 * @param file
	 * @return
	 * @throws IOException
	 * @throws ParseException 
	 */
	public static List<RtsHeaderDTO> readRTSFile(File file) throws IOException, ParseException {

		//A	P	4	MR0037674	0	2084621503	1	6325	2084621503	2084621503		
		//A	P	4	AF0002472	8913	20Y8101260	1	EC	K75144	PC490LC	22/01/2021 11:34	PC490LC-11E0    
		//Mode	Type	Origin	External Order,	Position,	Part,	Qty	,Location,	"BaaN Serialor Part",	"Machine Model or Part",	"MachineDate/ Time",	Machine Model Variant
		FileReader fileReader = new FileReader(file);
		BufferedReader lines = new BufferedReader(fileReader);
		String line = "";

		List<RtsHeaderDTO> rtsHeaderDTOs = new ArrayList<RtsHeaderDTO>();

		RtsHeaderDTO rtsHeaderDTO = null;
		RtsBodyDTO rtsBodyDTO = null;
		List<String> orders = new ArrayList<>();
		
		
		while ((line = lines.readLine()) != null) {

			String[] values = splitLine(line);
			//Check Mode A - Add
			if (values[0].equalsIgnoreCase(ConstantHelper.RTS_FILE_MODE)) {
				if(orders.contains(values[3])){
					rtsBodyDTO = new RtsBodyDTO();
				}else{
					rtsHeaderDTO = new RtsHeaderDTO();
					rtsBodyDTO = new RtsBodyDTO();
					rtsHeaderDTO.setOrigin(values[2]);
					rtsHeaderDTO.setOrderReference(values[3]);
					
					rtsHeaderDTO.setPosition(values[4]);
					rtsHeaderDTO.setMachineModel(values[9]);
					if(!values[10].isEmpty() && values[10].length() > 11){
						rtsHeaderDTO.setDateTime(new SimpleDateFormat(ConstantHelper.BAAN_FILE_SIMPLE_DATE_TIME_FORMAT).parse(values[10]));
					}else if(!values[10].isEmpty()){
						rtsHeaderDTO.setDateTime(new SimpleDateFormat(ConstantHelper.BAAN_FILE_SIMPLE_DATE_FORMAT).parse(values[10]));
					}
					rtsHeaderDTO.setModelVariant(values[11]);
					orders.add(values[3]);
				}
				rtsBodyDTO.setPartNumber(values[5]);
				rtsBodyDTO.setQtyReceived(Double.valueOf(values[6]));
				rtsBodyDTO.setBaanLocation(values[7]);
				rtsBodyDTO.setBaanSerial(values[8]);
				// populate details
				RtsDetailDTO rtsDetail = new RtsDetailDTO();
				rtsDetail.setDocumentReference(values[3]);
				rtsDetail.setPartNumber(values[5]);
				if(!rtsHeaderDTO.getParts().contains(values[5])){
					rtsHeaderDTO.getParts().add(values[5]);
				}
				rtsDetail.setReceivedQty(Double.valueOf(values[6]));
				rtsDetail.setTransactedQty(Double.valueOf(0));
				rtsDetail.setSerialReference(values[3]);
				rtsBodyDTO.getRtsDetails().add(rtsDetail);
				
				//update flags if Fab RTS
				if(rtsHeaderDTO.getOrderReference().startsWith(ConstantHelper.FAB_RTS_INDICATOR) && rtsHeaderDTO.getPosition().equals("0")){
					rtsDetail.setLocationCode(ConstantHelper.FAB_RTS_DEFAULT_LOCATION);
					rtsHeaderDTO.setFABRTS(true);
				}
				rtsHeaderDTO.getRtsBodies().add(rtsBodyDTO);
				rtsHeaderDTOs.add(rtsHeaderDTO);

			}
		}
		fileReader.close();
		lines.close();
		return rtsHeaderDTOs;

	}
	
	
	/**
	 * readReloc
	 * @param file
	 * @return
	 * @throws IOException
	 * @throws ParseException 
	 */
	public static List<RelocTaskDTO> readReloc(File file) throws IOException, ParseException {

		//119330|11|1|quarantine|stock|
		//119376|6|1|quarantine|inspection|PP0224805
		//GRN,	Line,	Qty,	Fromlocation,	ToLocation,	Order number
		FileReader fileReader = new FileReader(file);
		BufferedReader lines = new BufferedReader(fileReader);
		String line = "";

		List<RelocTaskDTO> relocTasks = new ArrayList<RelocTaskDTO>();
		while ((line = lines.readLine()) != null) {

			String[] values = splitLine(line);
			//Check Mode A - Add
			if(values[0].contains(ConstantHelper.BAAN_FILE_END_FILE_RECORD) || values[0].contains(ConstantHelper.BAAN_FILE_START_FILE_RECORD)){
				//	skip the line
			}else{
				RelocTaskDTO relocTask = new RelocTaskDTO();
				
				relocTask.setGrnReference(values[0]);
				relocTask.setLineNo(Integer.valueOf( values[1]));
				relocTask.setQuantity(Double.valueOf(values[2]));
				relocTask.setFromLocation(values[3]);
				relocTask.setToLocation(values[4]);
				relocTask.setOrderReference(values[5]);
				relocTasks.add(relocTask);

			}
		}
		fileReader.close();
		lines.close();
		return relocTasks;

	}
	
	
	/**
	 * readChgPno
	 * @param file
	 * @return
	 * @throws IOException
	 * @throws ParseException 
	 */
	public static List<ChangePnoDTO> readChgPno(File file) throws IOException, ParseException {

//		121082|1|20770KA731|20770KA731NK|4  
//
//		File breakdown:
//		121082|1|20770KA731|20770KA731NK|4  
//		GRN - 121082
//		GRN Line No  - 1
//		Original Part Number - 20770KA731
//		New Part Number – 20770KA731NK
//		New GRN Line No - 4
		
		FileReader fileReader = new FileReader(file);
		BufferedReader lines = new BufferedReader(fileReader);
		String line = "";

		List<ChangePnoDTO> chagePrtNoTasks = new ArrayList<ChangePnoDTO>();
		while ((line = lines.readLine()) != null) {

			String[] values = splitLine(line);
			//Check Mode A - Add
			if(values[0].contains(ConstantHelper.BAAN_FILE_END_FILE_RECORD) || values[0].contains(ConstantHelper.BAAN_FILE_START_FILE_RECORD)){
				//	skip the line
			}else{
				ChangePnoDTO pnoTask = new ChangePnoDTO();
				
				pnoTask.setGrnReference(values[0]);
				pnoTask.setLineNo(Integer.valueOf( values[1]));
				pnoTask.setPartNumber(values[2]);
				pnoTask.setNewPartNumber(values[3]);
				pnoTask.setNewLineNo(Integer.valueOf( values[4]));
				chagePrtNoTasks.add(pnoTask);

			}
		}
		fileReader.close();
		lines.close();
		return chagePrtNoTasks;

	}

	private static void updatePartQty(List<ReceiptBodyDTO> bodyList, ReceiptBodyDTO tmpBody) {

		for (ReceiptBodyDTO body : bodyList) {
			if (body.getPartNumber().equals(tmpBody.getPartNumber())
					&& body.getShippingReferenceCaseReference().equals(tmpBody.getShippingReferenceCaseReference())) {
				body.setQuantityAdvised(Double.parseDouble(body.getQuantityAdvised())
						+ Double.parseDouble(tmpBody.getQuantityAdvised()) + "");
				body.setQtyAccepted(
						Double.parseDouble(body.getQtyAccepted()) + Double.parseDouble(tmpBody.getQtyAccepted()) + "");
				body.setQtyToQuarantine(Double.parseDouble(body.getQtyToQuarantine())
						+ Double.parseDouble(tmpBody.getQtyToQuarantine()) + "");
			}

		}

	}

}
