package com.vantec.receiving.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.vantec.receiving.entity.ReceiptHeader;
import com.vantec.receiving.entity.Vendor;
import com.vantec.receiving.model.ReceiptHeaderDTO;
import com.vantec.receiving.model.RtsBodyDTO;
import com.vantec.receiving.model.RtsDetailDTO;
import com.vantec.receiving.model.RtsHeaderDTO;
import com.vantec.receiving.model.UnplannedRTSDTO;
import com.vantec.receiving.model.VendorDTO;

public final class RecievingServiceHelper {
	
	public static Vendor convertVendorViewToEntity(VendorDTO vendorDTO,Vendor vendor) throws ParseException {
		
		if(vendor == null){
			vendor = new Vendor();
			vendor.setDateCreated(new Date());
			vendor.setCreatedBy(vendorDTO.getCreatedBy());
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		vendor.setAutoPick(vendorDTO.getAutoPick());
		vendor.setAutoReceive(vendorDTO.getAutoReceive());
		vendor.setContactEmail(vendorDTO.getContactEmail());
		vendor.setContactName(vendorDTO.getContactName());
		vendor.setContactTel(vendorDTO.getContactTel());
		//vendor.setCountry(vendorDTO.getCountry());
		vendor.setDateUpdated(new Date());
		vendor.setDecant(vendorDTO.getDecant());
		vendor.setDecantChargeId(vendorDTO.getDecantCharge());
		vendor.setDespatchChargeId(vendorDTO.getDespatchCharge());
		Date effectiveFromDate = sdf.parse(vendorDTO.getEffectiveFrom());
		vendor.setEffectiveFrom(effectiveFromDate);
		Date effectiveToDate = sdf.parse(vendorDTO.getEffectiveTo());
		vendor.setEffectiveTo(effectiveToDate);
		vendor.setInspect(vendorDTO.getInspect());
		vendor.setInspectChargeId(vendorDTO.getInspectCharge());
		vendor.setPostCode(vendorDTO.getPostCode());
		vendor.setReceiptChargeId(vendorDTO.getReceiptCharge());
		vendor.setReportChargeIds(vendorDTO.getReportChargeIds());
		vendor.setStorageChargeDay(vendorDTO.getStorageChargeDay());
		vendor.setStorageChargeId(vendorDTO.getStorageCharge());
		vendor.setStreet(vendorDTO.getStreet());
		//vendor.setTown(vendorDTO.getTown());
		vendor.setTransportChargeId(vendorDTO.getTransportCharge());
		vendor.setUpdatedBy(vendorDTO.getUpdatedBy());
		vendor.setVatNumber(vendorDTO.getVatNumber());
		vendor.setVatPercentage(vendorDTO.getVatPercentage());
		vendor.setVendorReferenceCode(vendorDTO.getVendorCode());
		vendor.setVendorName(vendorDTO.getVendorName());
		
		return vendor;
	
    }
	/**
	 * 
	 * 
	 * @param dto
	 * @return
	 */
	public static RtsHeaderDTO convertUnplannedRTSToRTSTask(UnplannedRTSDTO dto){
		RtsHeaderDTO rtsHeader = new RtsHeaderDTO();
		

		rtsHeader.setCreatedAt(new Date());
		rtsHeader.setCreatedBy(dto.getUser());
		rtsHeader.setLastUpdated(new Date());
		rtsHeader.setLastUpdatedBy(dto.getUser());
		rtsHeader.setOrderType(ConstantHelper.UNPLANNED_RTS);
		rtsHeader.setReasonCode(dto.getReasonCode());
		rtsHeader.setIsProcessed(false);
		
		RtsBodyDTO body = new RtsBodyDTO();
		body.setPartNumber(dto.getPartNumber());
		body.setCreatedAt(new Date());
		body.setCreatedBy(dto.getUser());
		body.setLastUpdated(new Date());
		body.setLastUpdatedBy(dto.getUser());
		body.setQtyReceived(dto.getPieceQty()* dto.getNoOfBoxes());
		
		for(int i= 0 ; i< dto.getNoOfBoxes();i++){
			String formattedSerial = ConstantHelper.UNPLANNED_RTS_SERIAL_PREFIX;
//			if(serialCnt < 9999){
//				formattedSerial += String.format("%04d", serialCnt +1 );
//			}else{
//				formattedSerial += (serialCnt +1) ;
//			}
//			serialCnt++;
			RtsDetailDTO detail = new RtsDetailDTO();
			detail.setLocationCode(ConstantHelper.RTS_DEFAULT_LOCATION);
			detail.setPartNumber(dto.getPartNumber());
			detail.setCreatedBy(dto.getUser());
			detail.setReceivedQty(dto.getPieceQty());
			detail.setSerialReference(formattedSerial);
			body.getRtsDetails().add(detail);
		}
		rtsHeader.getRtsBodies().add(body);
		
		return rtsHeader;
	}
	
	
	
	public static ReceiptHeader convertViewToEntity(ReceiptHeaderDTO receivingRequests) {
//		ReceiptHeader receiptHeader = new ReceiptHeader();
//		ReceiptBody receiptPart = null;
//		ReceiptDetail receiptSerial = null;
//		
//		List<ReceiptBody> receiptBodies = new ArrayList<ReceiptBody>();
//		List<ReceiptBodyDTO> receiptBodiesRequest = receivingRequests.getRecieptBody();
//		List<ReceiptDetail> receiptDetails = new ArrayList<ReceiptDetail>();
//		
//		//Header
//		receiptHeader.setCreatedBy(receivingRequests.getCreatedBy());
//		receiptHeader.setDateCreated(new Date());
//		receiptHeader.setUpdatedBy(receivingRequests.getCreatedBy());
//		receiptHeader.setDateUpdated(new Date());
//		receiptHeader.setCustomerReference(receivingRequests.getCustomerReference());
//		receiptHeader.setExpectedDeliveryDate(receivingRequests.getEta() != null ? receivingRequests.getEta() : new Date());
//		receiptHeader.setHaulierReference(receivingRequests.getHaulierReference());
//		receiptHeader.setProductType(receivingRequests.getProductType());
//		receiptHeader.setDocumentStatus(receivingRequests.getDocumentStatus());
//		receiptHeader.setVendor(receivingRequests.getVendor());
//		receiptHeader.setInspect(receivingRequests.getRequireInspection());
//		
//		receiptHeader.setJisInd(receivingRequests.getJisInd()?"D":"");
//		
//		//Body
//		if(receiptBodiesRequest != null){
//			for(ReceiptBodyDTO receiptBodyRequest : receiptBodiesRequest) {
//				receiptPart = new ReceiptBody();
//		    	receiptPart.setAdvisedQty(setConversionFactor(receiptBodyRequest.getAdvisedQty(),receiptBodyRequest.getPart().getConversionFactor()));
//		    	if(receiptBodyRequest.getReceivedQty() == null){
//		    		receiptBodyRequest.setReceivedQty((double) 0);
//		    	}
//		    	receiptPart.setReceivedQty(setConversionFactor(receiptBodyRequest.getReceivedQty(),receiptBodyRequest.getPart().getConversionFactor()));
//		    	receiptPart.setDifference(receiptPart.getReceivedQty() - receiptPart.getAdvisedQty());
//		    	receiptPart.setPartNumber(receiptBodyRequest.getPartNumber());
//		        receiptPart.setPart(receiptBodyRequest.getPart());
//		    	receiptPart.setProductType(receivingRequests.getProductType());
//		    	receiptPart.setDateCreated(new Date());
//		    	receiptPart.setCreatedBy("sys");
//		    	receiptPart.setDateUpdated(new Date());
//		    	receiptPart.setUpdatedBy("sys");
//		    	receiptPart.setDocumentReference(receivingRequests.getDocumentReference());
//		    	receiptPart.setReceiptHeader(receiptHeader);
//		    	
//		    	
//		    	
//		    	//Detail
//		    	
//			    	List<ReceiptDetailDTO> receiptDetailsRequest = receiptBodyRequest.getReceiptDetails();
//			    	if(receiptDetailsRequest != null){
//			    	for(ReceiptDetailDTO receiptDetailRequest : receiptDetailsRequest){
//			    		receiptSerial = new ReceiptDetail();
//			    		receiptSerial.setAdvisedQty(setConversionFactor(receiptDetailRequest.getAdvisedQty(),receiptBodyRequest.getPart().getConversionFactor()));
//			    		if(receiptDetailRequest.getReceivedQty() == null){
//			    			receiptDetailRequest.setReceivedQty((double) 0);
//				    	}
//			    		receiptSerial.setReceivedQty(setConversionFactor(receiptDetailRequest.getReceivedQty(),receiptBodyRequest.getPart().getConversionFactor()));
//                 		receiptSerial.setDifference(receiptPart.getReceivedQty() - receiptPart.getAdvisedQty());
//			    		receiptSerial.setCreatedBy(receiptDetailRequest.getCreatedBy());
//			    		receiptSerial.setDateCreated(new Date());
//			    		receiptSerial.setDocumentReference(receivingRequests.getDocumentReference());
//			    		receiptSerial.setPartNumber(receiptBodyRequest.getPartNumber());
//			    		receiptSerial.setRanOrder(receiptDetailRequest.getRanOrder());
//			    		receiptSerial.setSerialReference(receiptDetailRequest.getSerialReference());
//			    		receiptSerial.setPart(receiptBodyRequest.getPart());
//			    		receiptSerial.setProductType(receivingRequests.getProductType());
//			    		receiptSerial.setReceiptBody(receiptPart);
//			    		
//			    		receiptDetails.add(receiptSerial);
//			    	}
//			    	receiptPart.setReceiptDetails(receiptDetails);
//		    	}
//		    	receiptBodies.add(receiptPart);
//		    	
//		    }
//		    receiptHeader.setReceiptBodies(receiptBodies);
//		}
	    
		
		
		return null;
		}
	
	public static Double setConversionFactor(Double qty,Integer conversionFactor) {
		return qty*conversionFactor;
	}

}
