package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "inventoryStatus")
public class InventoryStatus implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	//statusCode
	@Column(name = "inventoryStatusCode")
	private String inventoryStatusCode;

	//statusDescription
	@Column(name = "inventoryStatusDescription")
	private String inventoryStatusDescription;

	//pickable
	@Column(name = "nonPickable")
	private Boolean nonPickable;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdated")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public InventoryStatus() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getInventoryStatusCode() {
		return inventoryStatusCode;
	}

	public void setInventoryStatusCode(String inventoryStatusCode) {
		this.inventoryStatusCode = inventoryStatusCode;
	}

	public String getInventoryStatusDescription() {
		return inventoryStatusDescription;
	}

	public void setInventoryStatusDescription(String inventoryStatusDescription) {
		this.inventoryStatusDescription = inventoryStatusDescription;
	}

	public Boolean getNonPickable() {
		return nonPickable;
	}

	public void setNonPickable(Boolean nonPickable) {
		this.nonPickable = nonPickable;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
