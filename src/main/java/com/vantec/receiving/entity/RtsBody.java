package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "rtsBody")
@JsonIgnoreProperties(ignoreUnknown=true)
public class RtsBody implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rtsHeaderId", referencedColumnName = "id")
	private RtsHeader rtsHeader;


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partId", referencedColumnName = "id")
	private Part part;
	
	@Column(name = "partNumber")
	private String partNumber;
	
	
	@Column(name = "baanSerial")
	private String baanSerial;
	
	@Column(name = "baanLocation")
	private String baanLocation;
	
	@Column(name = "qtyReceived")
	private Double qtyReceived;
	
	@Column(name = "qtyTransacted")
	private Double qtyTransacted;

	
	@ManyToOne//(fetch = FetchType.LAZY)
	@JoinColumn(name = "DocumentStatusId", referencedColumnName = "id")
	private DocumentStatus documentStatus;

	@OneToMany(mappedBy = "rtsBody")
	private List<RtsDetail> rtsDetails = new ArrayList<RtsDetail>();

	@Column(name = "dateCreated")
	private Date createdAt;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdated")
	private Date lastUpdated;

	@Column(name = "lastUpdatedBy")
	private String lastUpdatedBy;

	public RtsBody() {
	}

	public Double getQtyTransacted() {
		return qtyTransacted;
	}

	public void setQtyTransacted(Double qtyTransacted) {
		this.qtyTransacted = qtyTransacted;
	}

	public List<RtsDetail> getRtsDetails() {
		return rtsDetails;
	}

	public void setRtsDetails(List<RtsDetail> rtsDetails) {
		this.rtsDetails = rtsDetails;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public RtsHeader getRtsHeader() {
		return rtsHeader;
	}

	public void setRtsHeader(RtsHeader rtsHeader) {
		this.rtsHeader = rtsHeader;
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getBaanSerial() {
		return baanSerial;
	}

	public void setBaanSerial(String baanSerial) {
		this.baanSerial = baanSerial;
	}

	public String getBaanLocation() {
		return baanLocation;
	}

	public void setBaanLocation(String baanLocation) {
		this.baanLocation = baanLocation;
	}

	public Double getQtyReceived() {
		return qtyReceived;
	}

	public void setQtyReceived(Double qtyReceived) {
		this.qtyReceived = qtyReceived;
	}

	public DocumentStatus getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus;
	}



	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}


    
}
