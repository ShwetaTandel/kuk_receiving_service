package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "relocHeader")
@JsonIgnoreProperties(ignoreUnknown=true)
public class RelocHeader implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "documentReference")
	private String documentReference;
	

	@Column(name = "lineNo")
	private Integer lineNo;
	
	@Column(name = "fromLocation")
	private String fromLocation;
	
	
	@Column(name = "toLocation")
	private String toLocation;
	
	@Column(name = "processed")
	private Boolean processed;
	
	@Column(name = "quantity")
	private Double quantity;
	
	
	@ManyToOne
	@JoinColumn(name = "receiptHeaderId", referencedColumnName = "id")
	private ReceiptHeader receiptHeader;
	
	@OneToMany(mappedBy = "relocHeader")
	private List<RelocBody> relocBodies = new ArrayList<RelocBody>();

	@Column(name = "orderReference")
	private String orderReference;

	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date lastUpdated;

	@Column(name = "lastUpdatedBy")
	private String lastUpdatedBy;

	public RelocHeader() {
	}

	public List<RelocBody> getRelocBodies() {
		return relocBodies;
	}

	public void setRelocBodies(List<RelocBody> relocBodies) {
		this.relocBodies = relocBodies;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentReference() {
		return documentReference;
	}

	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}


	public Integer getLineNo() {
		return lineNo;
	}

	public void setLineNo(Integer lineNo) {
		this.lineNo = lineNo;
	}

	public String getFromLocation() {
		return fromLocation;
	}

	public void setFromLocation(String fromLocation) {
		this.fromLocation = fromLocation;
	}

	public String getToLocation() {
		return toLocation;
	}

	public void setToLocation(String toLocation) {
		this.toLocation = toLocation;
	}

	public Boolean getProcessed() {
		return processed;
	}

	public void setProcessed(Boolean processed) {
		this.processed = processed;
	}

	

	public ReceiptHeader getReceiptHeader() {
		return receiptHeader;
	}

	public void setReceiptHeader(ReceiptHeader receiptHeader) {
		this.receiptHeader = receiptHeader;
	}

	

	public String getOrderReference() {
		return orderReference;
	}

	public void setOrderReference(String orderReference) {
		this.orderReference = orderReference;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

}
