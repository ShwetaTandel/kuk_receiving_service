package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "active")
	private Boolean active;

	@Column(name = "userId")
	private String userId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "email")
	private String email;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "companyId", referencedColumnName = "id")
	private Company company;
	
	@Column(name = "departmentId")
	private Long departmentId;
	
	@Column(name = "jobId")
	private Long jobId;
	
	@Column(name = "mainMenuId")
	private Long mainMenuId;
	
	@Column(name = "allowedMenuCodes")
	private String allowedMenuCodes;
	
	@Column(name = "canAdd")
	private Boolean canAdd;
	
	@Column(name = "canChange")
	private Boolean canChange;
	
	@Column(name = "canDelete")
	private Boolean canDelete;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "passwordResetCode")
	private String passwordResetCode;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;




	public User() {
	}




	public Long getId() {
		return id;
	}




	public void setId(Long id) {
		this.id = id;
	}




	public Boolean getActive() {
		return active;
	}




	public void setActive(Boolean active) {
		this.active = active;
	}




	public String getUserId() {
		return userId;
	}




	public void setUserId(String userId) {
		this.userId = userId;
	}




	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public String getEmail() {
		return email;
	}




	public void setEmail(String email) {
		this.email = email;
	}




	public Company getCompany() {
		return company;
	}




	public void setCompany(Company company) {
		this.company = company;
	}




	public Long getDepartmentId() {
		return departmentId;
	}




	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}




	public Long getJobId() {
		return jobId;
	}




	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}




	public Long getMainMenuId() {
		return mainMenuId;
	}




	public void setMainMenuId(Long mainMenuId) {
		this.mainMenuId = mainMenuId;
	}




	public String getAllowedMenuCodes() {
		return allowedMenuCodes;
	}




	public void setAllowedMenuCodes(String allowedMenuCodes) {
		this.allowedMenuCodes = allowedMenuCodes;
	}




	public Boolean getCanAdd() {
		return canAdd;
	}




	public void setCanAdd(Boolean canAdd) {
		this.canAdd = canAdd;
	}




	public Boolean getCanChange() {
		return canChange;
	}




	public void setCanChange(Boolean canChange) {
		this.canChange = canChange;
	}




	public Boolean getCanDelete() {
		return canDelete;
	}




	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}




	public String getPasswordResetCode() {
		return passwordResetCode;
	}




	public void setPasswordResetCode(String passwordResetCode) {
		this.passwordResetCode = passwordResetCode;
	}




	public Date getDateCreated() {
		return dateCreated;
	}




	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}




	public String getCreatedBy() {
		return createdBy;
	}




	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}




	public Date getDateUpdated() {
		return dateUpdated;
	}




	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}




	public String getUpdatedBy() {
		return updatedBy;
	}




	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


    
	

}
