package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "LocationFillingStatus",
       uniqueConstraints = {@UniqueConstraint(columnNames={"fillingCode"})} )
public class LocationFillingStatus implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "fillingCode")
	private String fillingCode;

	@Column(name = "fillingDescription")
	private String fillingDescription;
	
	@Column(name = "putaway")
	private Boolean putaway;
	
	@Column(name = "pick")
	private Boolean pick;

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public LocationFillingStatus() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFillingCode() {
		return fillingCode;
	}

	public void setFillingCode(String fillingCode) {
		this.fillingCode = fillingCode;
	}

	public String getFillingDescription() {
		return fillingDescription;
	}

	public void setFillingDescription(String fillingDescription) {
		this.fillingDescription = fillingDescription;
	}

	public Boolean getPutaway() {
		return putaway;
	}

	public void setPutaway(Boolean putaway) {
		this.putaway = putaway;
	}

	public Boolean getPick() {
		return pick;
	}

	public void setPick(Boolean pick) {
		this.pick = pick;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
