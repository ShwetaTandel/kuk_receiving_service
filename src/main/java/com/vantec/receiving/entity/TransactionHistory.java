package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="transactionHistory")
public class TransactionHistory implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	
	//ranOrder
	@Column(name = "ranOrOrder")
	private String ranOrOrder;
	
	@Column(name = "partNumber")
	private String partNumber;
	
	@Column(name = "serialReference")
	private String serialReference;
	
	@Column(name = "shortCode")
	private String shortCode;
	
	@Column(name = "transactionReferenceCode")
	private String transactionReferenceCode;
	
	//scannedQty
	@Column(name = "txnQty")
	private Double txnQty;
	
	@Column(name = "countQty")
	private Double countQty;
	
	@Column(name = "fromLocationCode")
	private String fromLocationCode;
	
	
	@Column(name = "fromPlt")
	private String fromPlt;
	
	@Column(name = "toLocationCode")
	private String toLocationCode;
	
	@Column(name = "toPlt")
	private String toPlt;
	
	@Column(name = "associatedDocumentReference")
	private String associatedDocumentReference;
	
	@Column(name = "customerReference")
	private String customerReference;
	
	@Column(name = "reasonCode")
	private String reasonCode;
	
	@Column(name = "requiresDecant")
	private Boolean requiresDecant;
	
	@Column(name = "requiresInspection")
	private Boolean requiresInspection;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdated")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getSerialReference() {
		return serialReference;
	}

	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public Double getCountQty() {
		return countQty;
	}

	public void setCountQty(Double countQty) {
		this.countQty = countQty;
	}
	


	public String getFromPlt() {
		return fromPlt;
	}

	public void setFromPlt(String fromPlt) {
		this.fromPlt = fromPlt;
	}
	


	public String getToPlt() {
		return toPlt;
	}

	public void setToPlt(String toPlt) {
		this.toPlt = toPlt;
	}



	public String getCustomerReference() {
		return customerReference;
	}

	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}



	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getFromLocationCode() {
		return fromLocationCode;
	}

	public void setFromLocationCode(String fromLocationCode) {
		this.fromLocationCode = fromLocationCode;
	}

	public String getToLocationCode() {
		return toLocationCode;
	}

	public void setToLocationCode(String toLocationCode) {
		this.toLocationCode = toLocationCode;
	}

	public String getRanOrOrder() {
		return ranOrOrder;
	}

	public void setRanOrOrder(String ranOrOrder) {
		this.ranOrOrder = ranOrOrder;
	}

	public Double getTxnQty() {
		return txnQty;
	}

	public void setTxnQty(Double txnQty) {
		this.txnQty = txnQty;
	}

	public String getAssociatedDocumentReference() {
		return associatedDocumentReference;
	}

	public void setAssociatedDocumentReference(String associatedDocumentReference) {
		this.associatedDocumentReference = associatedDocumentReference;
	}

	public Boolean getRequiresDecant() {
		return requiresDecant;
	}

	public void setRequiresDecant(Boolean requiresDecant) {
		this.requiresDecant = requiresDecant;
	}

	public Boolean getRequiresInspection() {
		return requiresInspection;
	}

	public void setRequiresInspection(Boolean requiresInspection) {
		this.requiresInspection = requiresInspection;
	}

	public String getTransactionReferenceCode() {
		return transactionReferenceCode;
	}

	public void setTransactionReferenceCode(String transactionReferenceCode) {
		this.transactionReferenceCode = transactionReferenceCode;
	}
	

    
	
	
	
	
	

}
