package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "locationType",
       uniqueConstraints = {@UniqueConstraint(columnNames={"locationTypeCode"})} )
public class LocationType implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "version")
	private Long version;

	@Column(name = "locationTypeCode")
	private String locationTypeCode;

	@Column(name = "locationTypeDescription")
	private String locationTypeDescription;
	
	@Column(name = "locationArea")
	private String locationArea;
	
	@Column(name = "defaultInventoryStatusId")
	private Long defaultInventoryStatusId;
	
	@Column(name = "active")
	private Boolean active;
	
	
	@Column(name = "looseParts")
	private Boolean looseParts;
	
	@Column(name = "singleVendorId")
	private Long singleVendorId;
	

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdated")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public LocationType() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLocationTypeCode() {
		return locationTypeCode;
	}

	public void setLocationTypeCode(String locationTypeCode) {
		this.locationTypeCode = locationTypeCode;
	}

	public String getLocationTypeDescription() {
		return locationTypeDescription;
	}

	public void setLocationTypeDescription(String locationTypeDescription) {
		this.locationTypeDescription = locationTypeDescription;
	}

	public String getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(String locationArea) {
		this.locationArea = locationArea;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getDefaultInventoryStatusId() {
		return defaultInventoryStatusId;
	}

	public void setDefaultInventoryStatusId(Long defaultInventoryStatusId) {
		this.defaultInventoryStatusId = defaultInventoryStatusId;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getLooseParts() {
		return looseParts;
	}

	public void setLooseParts(Boolean looseParts) {
		this.looseParts = looseParts;
	}

	public Long getSingleVendorId() {
		return singleVendorId;
	}

	public void setSingleVendorId(Long singleVendorId) {
		this.singleVendorId = singleVendorId;
	}
	
    
	

}
