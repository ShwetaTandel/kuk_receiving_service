package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "orderBody")
public class OrderBody implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orderHeaderId", referencedColumnName = "id")
	private OrderHeader orderHeader;
	
	@Column(name = "partNumber")
	private String partNumber;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partId", referencedColumnName = "id")
	private Part part;
	
	@Column(name = "qtyExpected")
	private Double qtyExpected;
	
	@Column(name = "qtyTransacted")
	private Double qtyTransacted;
	
	@Column(name = "difference")
	private Double difference;
	
	@Column(name = "lineNo")
	private Integer lineNo;
	
	@Column(name = "ranOrder")
	private String ranOrder;
	
	@Column(name = "documentReference")
	private String documentReference;
	
	@Column(name = "blocked")
	private Boolean blocked;
	
	@Column(name = "customerReference")
	private String customerReference;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "productTypeId", referencedColumnName = "id")
	private ProductType productType;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DocumentStatusId", referencedColumnName = "id")
	private DocumentStatus documentStatus;
	
	@Column(name = "zoneDestination")
	private String zoneDestination;
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	public Boolean getBlocked() {
		return blocked;
	}

	public void setBlocked(Boolean blocked) {
		this.blocked = blocked;
	}

	public DocumentStatus getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OrderHeader getOrderHeader() {
		return orderHeader;
	}

	public void setOrderHeader(OrderHeader orderHeader) {
		this.orderHeader = orderHeader;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	public Double getQtyExpected() {
		return qtyExpected;
	}

	public void setQtyExpected(Double qtyExpected) {
		this.qtyExpected = qtyExpected;
	}

	public Double getQtyTransacted() {
		return qtyTransacted;
	}

	public void setQtyTransacted(Double qtyTransacted) {
		this.qtyTransacted = qtyTransacted;
	}

	public Integer getLineNo() {
		return lineNo;
	}

	public void setLineNo(Integer lineNo) {
		this.lineNo = lineNo;
	}

	public String getRanOrder() {
		return ranOrder;
	}

	public void setRanOrder(String ranOrder) {
		this.ranOrder = ranOrder;
	}

	public Double getDifference() {
		return difference;
	}

	public void setDifference(Double difference) {
		this.difference = difference;
	}

	public String getDocumentReference() {
		return documentReference;
	}

	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public String getZoneDestination() {
		return zoneDestination;
	}

	public void setZoneDestination(String zoneDestination) {
		this.zoneDestination = zoneDestination;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getCustomerReference() {
		return customerReference;
	}

	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}
	

	
	
	
	

}
