package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "inventoryMaster")
public class InventoryMaster implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "version")
	private Long version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partId", referencedColumnName = "id")
	private Part part;

	@Column(name = "partNumber")
	private String partNumber;

	//ranOrder
	@Column(name = "ranOrOrder")
	private String ranOrOrder;

	@Column(name = "serialReference")
	private String serialReference;

	//pltNumber
	@Column(name = "tagReference")
	private String tagReference;

	@Column(name = "inventoryQty")
	private Double inventoryQty;

	@Column(name = "allocatedQty")
	private Double allocatedQty;
	
	@Column(name = "availableQty")
	private Double availableQty;

	@Column(name = "holdQty")
	private Double holdQty;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currentLocationId", referencedColumnName = "id")
	private Location currentLocation;

	@Column(name = "locationCode")
	private String locationCode;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "productTypeId", referencedColumnName = "id")
	private ProductType productType;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventoryStatusId", referencedColumnName = "id")
	private InventoryStatus inventoryStatus;
	
	@Column(name = "conversionFactor")
	private Integer conversionFactor;
	
	@Column(name = "recordedMissing")
	private Boolean recordedMissing = false;

	//decant
	@Column(name = "requiresDecant")
	private Boolean requiresDecant = false;

	//inspect
	@Column(name = "requiresInspection")
	private Boolean requiresInspection = false;

	//lastStockCheckDate
	@Column(name = "stockDate")
	private Date stockDate;

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdated")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public InventoryMaster() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getHoldQty() {
		return holdQty;
	}

	public void setHoldQty(Double holdQty) {
		this.holdQty = holdQty;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getSerialReference() {
		return serialReference;
	}

	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}

	public Double getInventoryQty() {
		return inventoryQty;
	}

	public void setInventoryQty(Double inventoryQty) {
		this.inventoryQty = inventoryQty;
	}

	public Double getAllocatedQty() {
		return allocatedQty;
	}

	public void setAllocatedQty(Double allocatedQty) {
		this.allocatedQty = allocatedQty;
	}

	public Double getAvailableQty() {
		return availableQty;
	}

	public void setAvailableQty(Double availableQty) {
		this.availableQty = availableQty;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getRanOrOrder() {
		return ranOrOrder;
	}

	public void setRanOrOrder(String ranOrOrder) {
		this.ranOrOrder = ranOrOrder;
	}

	public String getTagReference() {
		return tagReference;
	}

	public void setTagReference(String tagReference) {
		this.tagReference = tagReference;
	}

	public Boolean getRequiresDecant() {
		return requiresDecant;
	}

	public void setRequiresDecant(Boolean requiresDecant) {
		this.requiresDecant = requiresDecant;
	}

	public Boolean getRequiresInspection() {
		return requiresInspection;
	}

	public void setRequiresInspection(Boolean requiresInspection) {
		this.requiresInspection = requiresInspection;
	}

	public Date getStockDate() {
		return stockDate;
	}

	public void setStockDate(Date stockDate) {
		this.stockDate = stockDate;
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	public Location getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(Location currentLocation) {
		this.currentLocation = currentLocation;
	}

	public InventoryStatus getInventoryStatus() {
		return inventoryStatus;
	}

	public void setInventoryStatus(InventoryStatus inventoryStatus) {
		this.inventoryStatus = inventoryStatus;
	}

	public Boolean getRecordedMissing() {
		return recordedMissing;
	}

	public void setRecordedMissing(Boolean recordedMissing) {
		this.recordedMissing = recordedMissing;
	}

	public Integer getConversionFactor() {
		return conversionFactor;
	}

	public void setConversionFactor(Integer conversionFactor) {
		this.conversionFactor = conversionFactor;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
	
    
}
