package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@NamedQueries(  
	    {  
	@NamedQuery(
		    name="getDetailFromBodyAndSerial",
		    query="SELECT d FROM ReceiptDetail d WHERE d.receiptBody = :receiptBody and d.serialReference = :serialReference"
		),
	@NamedQuery(
		    name="deleteReceiptDetail",
		    query="delete from ReceiptDetail where documentReference = :documentReference and serialReference = :serialReference"
		)
	  }
)

@Entity
@Table(name = "receiptDetail")
@JsonIgnoreProperties(ignoreUnknown=true)
public class ReceiptDetail implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "documentReference")
	private String documentReference;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "receiptBodyId", referencedColumnName = "id")
	private ReceiptBody receiptBody;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "productTypeId", referencedColumnName = "id")
	private ProductType productType;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partId", referencedColumnName = "id")
	private Part part;
	
	@ManyToOne//(fetch = FetchType.LAZY)
	@JoinColumn(name = "DocumentStatusId", referencedColumnName = "id")
	private DocumentStatus documentStatus;

	public DocumentStatus getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus;
	}
	
	@Column(name = "partNumber")
	private String partNumber;

	@Column(name = "serialReference")
	private String serialReference;

	@Column(name = "ranOrder")
	private String ranOrder;

	@Column(name = "advisedQty")
	private Double advisedQty;

	@Column(name = "receivedQty")
	private Double receivedQty;
	
	@Column(name = "quarantinedQty")
	private Double quarantinedQty;

	@Column(name = "difference")
	private Double difference;

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date lastUpdatedDate;

	@Column(name = "lastUpdatedBy")
	private String lastUpdatedBy;

	public ReceiptDetail() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentReference() {
		return documentReference;
	}

	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}

	public ReceiptBody getReceiptBody() {
		return receiptBody;
	}

	public void setReceiptBody(ReceiptBody receiptBody) {
		this.receiptBody = receiptBody;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getSerialReference() {
		return serialReference;
	}

	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}

	public String getRanOrder() {
		return ranOrder;
	}

	public void setRanOrder(String ranOrder) {
		this.ranOrder = ranOrder;
	}

	public Double getAdvisedQty() {
		return advisedQty;
	}

	public void setAdvisedQty(Double advisedQty) {
		this.advisedQty = advisedQty;
	}

	public Double getReceivedQty() {
		return receivedQty;
	}

	public void setReceivedQty(Double receivedQty) {
		this.receivedQty = receivedQty;
	}

	public Double getDifference() {
		return difference;
	}

	public void setDifference(Double difference) {
		this.difference = difference;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return lastUpdatedDate;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.lastUpdatedDate = dateUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.lastUpdatedBy = updatedBy;
	}

	public Double getQuarantinedQty() {
		return quarantinedQty;
	}

	public void setQuarantinedQty(Double ctsQty) {
		this.quarantinedQty = ctsQty;
	}

}
