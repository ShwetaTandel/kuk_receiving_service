package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "orderHeader")
public class OrderHeader implements Serializable {

	
	private static final long serialVersionUID = 1L;

	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "documentReference")
	private String documentReference;
	
	@Column(name = "customerReference")
	private String customerReference;
	
	
	@Column(name = "orderType")
	private String orderType;
	
	@Column(name = "consignee")
	private String consignee;
	
	@Column(name = "shipTo")
	private String shipTo;
	
	@Column(name = "dockDestination")
	private String dockDestination;
	
	@Column(name = "autoCreated")
	private Boolean autoCreated;
	
	@Column(name = "autoConfirmed")
	private Boolean autoConfirmed;
	
	@Column(name = "expectedDeliveryTime")
	private Date expectedDeliveryTime;
	
	@Column(name = "timeSlot")
	private Date timeSlot;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DocumentStatusId", referencedColumnName = "id")
	private DocumentStatus documentStatus;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PickHeaderId", referencedColumnName = "id")
	private PickHeader pickHeader;
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;
	
	@Column(name = "baanSerial")
	private String baanSerial;

	@Column(name = "machineModel")
	private String machineModel;

	
	@OneToMany(mappedBy = "orderHeader")
	private List<OrderBody> orderBodies = new ArrayList<OrderBody>();

	public PickHeader getPickHeader() {
		return pickHeader;
	}

	public void setPickHeader(PickHeader pickHeader) {
		this.pickHeader = pickHeader;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentReference() {
		return documentReference;
	}

	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}

	public String getCustomerReference() {
		return customerReference;
	}

	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}

	public String getConsignee() {
		return consignee;
	}

	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}

	public String getShipTo() {
		return shipTo;
	}

	public void setShipTo(String shipTo) {
		this.shipTo = shipTo;
	}

	public String getDockDestination() {
		return dockDestination;
	}

	public void setDockDestination(String dockDestination) {
		this.dockDestination = dockDestination;
	}

	public Boolean getAutoCreated() {
		return autoCreated;
	}

	public void setAutoCreated(Boolean autoCreated) {
		this.autoCreated = autoCreated;
	}

	public Boolean getAutoConfirmed() {
		return autoConfirmed;
	}

	public void setAutoConfirmed(Boolean autoConfirmed) {
		this.autoConfirmed = autoConfirmed;
	}

	public DocumentStatus getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus;
	}

	public Date getExpectedDeliveryTime() {
		return expectedDeliveryTime;
	}

	public void setExpectedDeliveryTime(Date expectedDeliveryTime) {
		this.expectedDeliveryTime = expectedDeliveryTime;
	}

	public Date getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(Date timeSlot) {
		this.timeSlot = timeSlot;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getBaanSerial() {
		return baanSerial;
	}

	public void setBaanSerial(String baanSerial) {
		this.baanSerial = baanSerial;
	}

	public String getMachineModel() {
		return machineModel;
	}

	public void setMachineModel(String machineModel) {
		this.machineModel = machineModel;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public List<OrderBody> getOrderBodies() {
		return orderBodies;
	}

	public void setOrderBodies(List<OrderBody> orderBodies) {
		this.orderBodies = orderBodies;
	}

}

