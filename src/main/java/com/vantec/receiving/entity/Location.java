package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;



@Entity
@Table(name = "location",
       uniqueConstraints = {@UniqueConstraint(columnNames={"locationCode"})} )
public class Location implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "version")
	private Long version;
	
	@Column(name = "inventoryDispositionId")
	private Long inventoryDispositionId;
	
	@Column(name = "locationClosed")
	private Boolean locationClosed;
	
	@Column(name = "multiVendorLocation")
	private Boolean multiVendorLocation;

	@Column(name = "locationCode")
	private String locationCode;
	
	@Column(name = "locationCodeDescription")
	private String locationCodeDescription;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "aisleCode", referencedColumnName = "aisleCode")
	private LocationAisle locationAisleCode;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rackCode", referencedColumnName = "rackCode")
	private LocationRack locationRackCode;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rowCode", referencedColumnName = "rowCode")
	private LocationRow locationRowCode;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "columnCode", referencedColumnName = "columnCode")
	private LocationColumn locationColumnCode;

	//hashCode
	@Column(name = "locationHash")
	private String locationHash;
	
	@Column(name = "maxWeight")
	private Double maxWeight;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "locationTypeId", referencedColumnName = "id")
	private LocationType locationType;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "locationSubTypeId", referencedColumnName = "id")
	private LocationSubType locationSubType;

	//multiPart
	@Column(name = "multiPartLocation")
	private Boolean multiPartLocation = false;

	@Column(name = "replenWhenEmpty")
	private Boolean replenWhenEmpty = false;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fillingStatusId", referencedColumnName = "id")
	private LocationFillingStatus locationFillingStatus;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventoryStatusId", referencedColumnName = "id")
	private InventoryStatus inventoryStatus;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "productTypeId", referencedColumnName = "id")
	private ProductType productType;

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdated")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public Location() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public LocationAisle getLocationAisleCode() {
		return locationAisleCode;
	}

	public void setLocationAisleCode(LocationAisle locationAisleCode) {
		this.locationAisleCode = locationAisleCode;
	}

	public LocationRack getLocationRackCode() {
		return locationRackCode;
	}

	public void setLocationRackCode(LocationRack locationRackCode) {
		this.locationRackCode = locationRackCode;
	}

	public LocationRow getLocationRowCode() {
		return locationRowCode;
	}

	public void setLocationRowCode(LocationRow locationRowCode) {
		this.locationRowCode = locationRowCode;
	}

	public LocationColumn getLocationColumnCode() {
		return locationColumnCode;
	}

	public void setLocationColumnCode(LocationColumn locationColumnCode) {
		this.locationColumnCode = locationColumnCode;
	}
	
	public LocationType getLocationType() {
		return locationType;
	}

	public void setLocationType(LocationType locationType) {
		this.locationType = locationType;
	}
	public LocationSubType getLocationSubType() {
		return locationSubType;
	}

	public void setLocationSubType(LocationSubType locationSubType) {
		this.locationSubType = locationSubType;
	}

	public Boolean getReplenWhenEmpty() {
		return replenWhenEmpty;
	}

	public void setReplenWhenEmpty(Boolean replenWhenEmpty) {
		this.replenWhenEmpty = replenWhenEmpty;
	}

	public LocationFillingStatus getLocationFillingStatus() {
		return locationFillingStatus;
	}

	public void setLocationFillingStatus(LocationFillingStatus locationFillingStatus) {
		this.locationFillingStatus = locationFillingStatus;
	}

	public InventoryStatus getInventoryStatus() {
		return inventoryStatus;
	}

	public void setInventoryStatus(InventoryStatus inventoryStatus) {
		this.inventoryStatus = inventoryStatus;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public String getLocationHash() {
		return locationHash;
	}

	public void setLocationHash(String locationHash) {
		this.locationHash = locationHash;
	}

	public Boolean getMultiPartLocation() {
		return multiPartLocation;
	}

	public void setMultiPartLocation(Boolean multiPartLocation) {
		this.multiPartLocation = multiPartLocation;
	}
	public Double getMaxWeight() {
		return maxWeight;
	}
	public void setMaxWeight(Double maxWeight) {
		this.maxWeight = maxWeight;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getInventoryDispositionId() {
		return inventoryDispositionId;
	}

	public void setInventoryDispositionId(Long inventoryDispositionId) {
		this.inventoryDispositionId = inventoryDispositionId;
	}

	public Boolean getLocationClosed() {
		return locationClosed;
	}

	public void setLocationClosed(Boolean locationClosed) {
		this.locationClosed = locationClosed;
	}

	public String getLocationCodeDescription() {
		return locationCodeDescription;
	}

	public void setLocationCodeDescription(String locationCodeDescription) {
		this.locationCodeDescription = locationCodeDescription;
	}

	public Boolean getMultiVendorLocation() {
		return multiVendorLocation;
	}

	public void setMultiVendorLocation(Boolean multiVendorLocation) {
		this.multiVendorLocation = multiVendorLocation;
	}
	
	
	

	

}
