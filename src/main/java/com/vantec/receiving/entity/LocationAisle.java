package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "locationAisle",
       uniqueConstraints = {@UniqueConstraint(columnNames={"aisleCode"})} )
public class LocationAisle  implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "aisleCode")
	private String aisleCode;

	@Column(name = "aisleDescription")
	private String aisleDescription;
	
	@Column(name = "fillingPriority")
	private Long fillingPriority;

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public LocationAisle() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAisleCode() {
		return aisleCode;
	}

	public void setAisleCode(String aisleCode) {
		this.aisleCode = aisleCode;
	}

	public String getAisleDescription() {
		return aisleDescription;
	}

	public void setAisleDescription(String aisleDescription) {
		this.aisleDescription = aisleDescription;
	}

	public Long getFillingPriority() {
		return fillingPriority;
	}

	public void setFillingPriority(Long fillingPriority) {
		this.fillingPriority = fillingPriority;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
