package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "orderDetail")
public class OrderDetail implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventoryStatusId", referencedColumnName = "id")
	private InventoryStatus inventoryStatus;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partId", referencedColumnName = "id")
	private Part part;
	
	@Column(name = "orderHeaderId")
	private Long orderHeaderId;
	
	@Column(name = "pickHeaderId")
	private Long pickHeaderId;
	
	@Column(name = "allocatedQty")
	private Long allocatedQty;
	
	@Column(name = "lineNo")
	private Long lineNo;
	
	@Column(name = "pickPriority")
	private Long pickPriority;
	
	@Column(name = "processed")
	private Long processed;
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public InventoryStatus getInventoryStatus() {
		return inventoryStatus;
	}

	public void setInventoryStatus(InventoryStatus inventoryStatus) {
		this.inventoryStatus = inventoryStatus;
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	public Long getOrderHeaderId() {
		return orderHeaderId;
	}

	public void setOrderHeaderId(Long orderHeaderId) {
		this.orderHeaderId = orderHeaderId;
	}

	public Long getPickHeaderId() {
		return pickHeaderId;
	}

	public void setPickHeaderId(Long pickHeaderId) {
		this.pickHeaderId = pickHeaderId;
	}

	public Long getAllocatedQty() {
		return allocatedQty;
	}

	public void setAllocatedQty(Long allocatedQty) {
		this.allocatedQty = allocatedQty;
	}

	public Long getLineNo() {
		return lineNo;
	}

	public void setLineNo(Long lineNo) {
		this.lineNo = lineNo;
	}

	public Long getPickPriority() {
		return pickPriority;
	}

	public void setPickPriority(Long pickPriority) {
		this.pickPriority = pickPriority;
	}

	public Long getProcessed() {
		return processed;
	}

	public void setProcessed(Long processed) {
		this.processed = processed;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	
	
	

	

}
