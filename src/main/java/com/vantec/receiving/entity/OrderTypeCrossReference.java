package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//OrderType
@Entity
@Table(name = "orderTypeCrossReference")
public class OrderTypeCrossReference implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	//orderTypeCode
	@Column(name = "rrOrderType")
	private String rrOrderType;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "fullBoxPick")
	private Boolean fullBoxPick;
	
	@Column(name = "pickByLine")
	private Boolean pickByLine;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "productTypeId", referencedColumnName = "id")
	private ProductType productType;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getFullBoxPick() {
		return fullBoxPick;
	}

	public void setFullBoxPick(Boolean fullBoxPick) {
		this.fullBoxPick = fullBoxPick;
	}

	public Boolean getPickByLine() {
		return pickByLine;
	}

	public void setPickByLine(Boolean pickByLine) {
		this.pickByLine = pickByLine;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public String getRrOrderType() {
		return rrOrderType;
	}

	public void setRrOrderType(String rrOrderType) {
		this.rrOrderType = rrOrderType;
	}
	
	

}
