package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@NamedQueries(  
	    {  
	@NamedQuery(
		    name="findReceiptWithTrailerAndStatus",
		    query="SELECT h FROM ReceiptHeader h WHERE h.haulierReference = :trailerRef and h.documentStatus = :status"
		),
	@NamedQuery(
		    name="getAllChildHeaders",
		    query="SELECT h FROM ReceiptHeader h WHERE h.haulierReference = :haulierReference"
		),
	@NamedQuery(
		    name="getAllActiveChildHeaders",
		    query="SELECT h FROM ReceiptHeader h WHERE h.haulierReference = :haulierReference and h.documentStatus != :status"
		),
	@NamedQuery(
		    name="deleteReceiptHeader",
		    query="delete from ReceiptHeader where documentReference = :documentReference"
		)
  }  
)
@Entity
@Table(name = "receiptHeader")
@JsonIgnoreProperties(ignoreUnknown=true)
public class ReceiptHeader implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "document_reference")
	private String documentReference;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "productTypeId", referencedColumnName = "id")
	private ProductType productType;
	
	@Column(name = "customerReference")
	private String customerReference;

	@Column(name = "haulierReference")
	private String haulierReference;
	
	@Column(name = "jisInd")
	private String jisInd;
	
	@Column(name = "grnType")
	private String grnType;
	
	
	@Column(name = "deliveryNote")
	private String deliveryNote;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vendorId", referencedColumnName = "id")
	private Vendor vendor;

	@Column(name = "expectedDeliveryDate")
	private Date expectedDeliveryDate;

	@ManyToOne//(fetch = FetchType.LAZY)
	@JoinColumn(name = "DocumentStatusId", referencedColumnName = "id")
	private DocumentStatus documentStatus;
	
	@Column(name = "inspect")
	private Boolean inspect;

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@OneToMany(mappedBy = "receiptHeader")
	private List<ReceiptBody> receiptBodies = new ArrayList<ReceiptBody>();

	public ReceiptHeader() {
	}

	public List<ReceiptBody> getReceiptBodies() {
		return receiptBodies;
	}

	public void setReceiptBodies(List<ReceiptBody> receiptBodies) {
		this.receiptBodies = receiptBodies;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentReference() {
		return documentReference;
	}

	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

//	public Vendor getVendor() {
//		return vendor;
//	}
//
//	public void setVendor(Vendor vendor) {
//		this.vendor = vendor;
//	}

	public String getCustomerReference() {
		return customerReference;
	}

	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}

	public String getHaulierReference() {
		return haulierReference;
	}

	public void setHaulierReference(String haulierReference) {
		this.haulierReference = haulierReference;
	}

	public Date getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}

	public DocumentStatus getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Boolean getInspect() {
		return inspect;
	}

	public void setInspect(Boolean inspect) {
		this.inspect = inspect;
	}

	public String getJisInd() {
		return jisInd;
	}

	public void setJisInd(String jisInd) {
		this.jisInd = jisInd;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public String getDeliveryNote() {
		return deliveryNote;
	}

	public void setDeliveryNote(String deliveryNote) {
		this.deliveryNote = deliveryNote;
	}

	public String getGrnType() {
		return grnType;
	}

	public void setGrnType(String grnType) {
		this.grnType = grnType;
	}
	

}
