package com.vantec.receiving.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "relocBody")
@JsonIgnoreProperties(ignoreUnknown=true)
public class RelocBody implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	
	@Column(name = "serialReference")
	private String serialReference;

	
	@Column(name = "processed")
	private Boolean processed;
	
	@Column(name = "quantity")
	private Double quantity;
	
	@ManyToOne
	@JoinColumn(name = "inventoryMasterId", referencedColumnName = "id")
	private InventoryMaster inventoryMaster;
	
	@ManyToOne
	@JoinColumn(name = "relocHeaderId", referencedColumnName = "id")
	private RelocHeader relocHeader;
	

	public RelocBody() {
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSerialReference() {
		return serialReference;
	}

	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}

	public Boolean getProcessed() {
		return processed;
	}

	public void setProcessed(Boolean processed) {
		this.processed = processed;
	}

	public InventoryMaster getInventoryMaster() {
		return inventoryMaster;
	}

	public void setInventoryMaster(InventoryMaster inventoryMaster) {
		this.inventoryMaster = inventoryMaster;
	}

	public RelocHeader getRelocHeader() {
		return relocHeader;
	}

	public void setRelocHeader(RelocHeader relocHeader) {
		this.relocHeader = relocHeader;
	}



}
