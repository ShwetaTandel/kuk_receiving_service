package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "vendor")
public class Vendor implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "version")
	private Long version;
	
	@Column(name = "autoasnrequired")
	private Boolean autoasnrequired = false;
	
	@Column(name = "controlVendor")
	private Boolean controlVendor = false;
	
	@Column(name = "locality")
	private String locality;
	
	@Column(name = "recipient")
	private String recipient;
	
	@Column(name = "vendorName")
	private String vendorName;
	
	@Column(name = "active")
	private Boolean active = false;
	
	//vendorCode
	@Column(name = "vendorReferenceCode")
	private String vendorReferenceCode;
	
	@Column(name = "effectiveFrom")
	private Date effectiveFrom;
	
	@Column(name = "effectiveTo")
	private Date effectiveTo;
	
	@Column(name = "contactName")
	private String contactName;
	
	@Column(name = "contactTel")
	private String contactTel;
	
	@Column(name = "contactEmail")
	private String contactEmail;
	
	@Column(name = "street")
	private String street;
	
	
	//town
	@Column(name = "postalTown")
	private String postalTown;
	
	
	//country
	@Column(name = "countryName")
	private String countryName;
	
	@Column(name = "postCode")
	private String postCode;
	
	@Column(name = "autoReceive")
	private Boolean autoReceive = false;
	
	@Column(name = "autoPick")
	private Boolean autoPick = false;
	
	@Column(name = "decant")
	private Boolean decant = false;
	
	@Column(name = "inspect")
	private Boolean inspect = false;
	
	@Column(name = "convertToLt")
	private Boolean convertToLt = false;
	
	@Column(name = "vatPercentage")
	private Integer vatPercentage;
	
	@Column(name = "vatNumber")
	private Integer vatNumber;
	
	@Column(name = "storageChargeDay")
	private String storageChargeDay;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "storageChargeId", referencedColumnName = "id")
	private ChargeMaster storageChargeId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "receiptChargeId", referencedColumnName = "id")
	private ChargeMaster receiptChargeId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "despatchChargeId", referencedColumnName = "id")
	private ChargeMaster despatchChargeId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "decantChargeId", referencedColumnName = "id")
	private ChargeMaster decantChargeId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inspectChargeId", referencedColumnName = "id")
	private ChargeMaster inspectChargeId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "transportChargeId", referencedColumnName = "id")
	private ChargeMaster transportChargeId;
	
	@Column(name = "reportChargeIds")
	private String reportChargeIds;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdated")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;
	
	@Column(name = "offsiteStorage")
	private Boolean offsiteStorage = false;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "offsiteStorageLocationId", referencedColumnName = "id")
	private Location offsiteStorageLocation;
	

	public Location getOffsiteStorageLocation() {
		return offsiteStorageLocation;
	}

	public void setOffsiteStorageLocation(Location offsiteStorageLocation) {
		this.offsiteStorageLocation = offsiteStorageLocation;
	}

	public Boolean getOffsiteStorage() {
		return offsiteStorage;
	}

	public void setOffsiteStorage(Boolean offsiteStorage) {
		this.offsiteStorage = offsiteStorage;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public Date getEffectiveTo() {
		return effectiveTo;
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactTel() {
		return contactTel;
	}

	public void setContactTel(String contactTel) {
		this.contactTel = contactTel;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public Boolean getAutoReceive() {
		return autoReceive;
	}

	public void setAutoReceive(Boolean autoReceive) {
		this.autoReceive = autoReceive;
	}

	public Boolean getAutoPick() {
		return autoPick;
	}

	public void setAutoPick(Boolean autoPick) {
		this.autoPick = autoPick;
	}

	public Boolean getDecant() {
		return decant;
	}

	public void setDecant(Boolean decant) {
		this.decant = decant;
	}

	public Boolean getInspect() {
		return inspect;
	}

	public void setInspect(Boolean inspect) {
		this.inspect = inspect;
	}

	public Boolean getConvertToLt() {
		return convertToLt;
	}

	public void setConvertToLt(Boolean convertToLt) {
		this.convertToLt = convertToLt;
	}

	public ChargeMaster getStorageChargeId() {
		return storageChargeId;
	}

	public void setStorageChargeId(ChargeMaster storageChargeId) {
		this.storageChargeId = storageChargeId;
	}

	public String getStorageChargeDay() {
		return storageChargeDay;
	}

	public void setStorageChargeDay(String storageChargeDay) {
		this.storageChargeDay = storageChargeDay;
	}

	public ChargeMaster getReceiptChargeId() {
		return receiptChargeId;
	}

	public void setReceiptChargeId(ChargeMaster receiptChargeId) {
		this.receiptChargeId = receiptChargeId;
	}

	public ChargeMaster getDespatchChargeId() {
		return despatchChargeId;
	}

	public void setDespatchChargeId(ChargeMaster despatchChargeId) {
		this.despatchChargeId = despatchChargeId;
	}

	public ChargeMaster getDecantChargeId() {
		return decantChargeId;
	}

	public void setDecantChargeId(ChargeMaster decantChargeId) {
		this.decantChargeId = decantChargeId;
	}

	public ChargeMaster getInspectChargeId() {
		return inspectChargeId;
	}

	public void setInspectChargeId(ChargeMaster inspectChargeId) {
		this.inspectChargeId = inspectChargeId;
	}

	public ChargeMaster getTransportChargeId() {
		return transportChargeId;
	}

	public void setTransportChargeId(ChargeMaster transportChargeId) {
		this.transportChargeId = transportChargeId;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getReportChargeIds() {
		return reportChargeIds;
	}

	public void setReportChargeIds(String reportChargeIds) {
		this.reportChargeIds = reportChargeIds;
	}

	public Integer getVatPercentage() {
		return vatPercentage;
	}

	public void setVatPercentage(Integer vatPercentage) {
		this.vatPercentage = vatPercentage;
	}

	public Integer getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(Integer vatNumber) {
		this.vatNumber = vatNumber;
	}

	public String getVendorReferenceCode() {
		return vendorReferenceCode;
	}

	public void setVendorReferenceCode(String vendorReferenceCode) {
		this.vendorReferenceCode = vendorReferenceCode;
	}

	public String getPostalTown() {
		return postalTown;
	}

	public void setPostalTown(String postalTown) {
		this.postalTown = postalTown;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Boolean getAutoasnrequired() {
		return autoasnrequired;
	}

	public void setAutoasnrequired(Boolean autoasnrequired) {
		this.autoasnrequired = autoasnrequired;
	}

	public Boolean getControlVendor() {
		return controlVendor;
	}

	public void setControlVendor(Boolean controlVendor) {
		this.controlVendor = controlVendor;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	
}
