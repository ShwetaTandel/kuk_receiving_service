package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ttRanOrder")
public class TTRanOrder implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "ranOrder")
	private String ranOrder;
	
	@Column(name = "vendor")
	private String vendor;
	
	@Column(name = "partNumber")
	private String partNumber;
	
	@Column(name = "inventoryKey")
	private String inventoryKey;
	
	@Column(name = "packType")
	private String packType;
	
	@Column(name = "dock")
	private String dock;
	
	@Column(name = "zone")
	private String zone;
	
	@Column(name = "expectedQty")
	private Float expectedQty;
	
	@Column(name = "scannedQty")
	private Float scannedQty;
	
	@Column(name = "requiredDate")
	private Date requiredDate;
	
	@Column(name = "customerReferenceCode")
	private String customerReferenceCode;
	
	@Column(name = "completed")
	private int completed;
	
	@Column(name = "uploadedCount")
	private int uploadedCount;
	
	@Column(name = "dateCreated")
	private Date dateCreated;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRanOrder() {
		return ranOrder;
	}

	public void setRanOrder(String ranOrder) {
		this.ranOrder = ranOrder;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getInventoryKey() {
		return inventoryKey;
	}

	public void setInventoryKey(String inventoryKey) {
		this.inventoryKey = inventoryKey;
	}

	public String getPackType() {
		return packType;
	}

	public void setPackType(String packType) {
		this.packType = packType;
	}

	public String getDock() {
		return dock;
	}

	public void setDock(String dock) {
		this.dock = dock;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public Float getExpectedQty() {
		return expectedQty;
	}

	public void setExpectedQty(Float expectedQty) {
		this.expectedQty = expectedQty;
	}

	public Float getScannedQty() {
		return scannedQty;
	}

	public void setScannedQty(Float scannedQty) {
		this.scannedQty = scannedQty;
	}

	public Date getRequiredDate() {
		return requiredDate;
	}

	public void setRequiredDate(Date requiredDate) {
		this.requiredDate = requiredDate;
	}

	public String getCustomerReferenceCode() {
		return customerReferenceCode;
	}

	public void setCustomerReferenceCode(String customerReferenceCode) {
		this.customerReferenceCode = customerReferenceCode;
	}

	public int getCompleted() {
		return completed;
	}

	public void setCompleted(int completed) {
		this.completed = completed;
	}

	public int getUploadedCount() {
		return uploadedCount;
	}

	public void setUploadedCount(int uploadedCount) {
		this.uploadedCount = uploadedCount;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	

	
	

}
