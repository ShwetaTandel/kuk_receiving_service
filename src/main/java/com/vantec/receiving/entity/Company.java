package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "company")
public class Company implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "companyName")
	private String companyName;

	@Column(name = "companyReferenceCode")
	private String companyReferenceCode;

	@Column(name = "address")
	private String address;
	
	@Column(name = "interfaceInput")
	private String interfaceInput;

	@Column(name = "interfaceOutput")
	private String interfaceOutput;
	
	@Column(name = "interfaceTemp")
	private String interfaceTemp;

	
	@Column(name = "interfaceActive")
	private Boolean interfaceActive;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "productTypeId", referencedColumnName = "id")
	private ProductType productType;
	
	@Column(name = "mixedPartStorage")
	private Boolean mixedPartStorage;	

	@Column(name = "qtyDecimalPlace")
	private Integer qtyDecimalPlace;

	@Column(name = "virtualPalletLbl")
	private Boolean virtualPalletLbl;

	@Column(name = "minBoxQty")
	private Integer minBoxQty;

	@Column(name = "maxBoxPieceQty")
	private Integer maxBoxPieceQty;

	@Column(name = "minPartNumberLength")
	private Integer minPartNumberLength;

	@Column(name = "maxPartNumberLength")
	private Integer maxPartNumberLength;

	@Column(name = "minRanNumberLength")
	private Integer minRanNumberLength;

	@Column(name = "maxRanNumberLength")
	private Integer maxRanNumberLength;

	@Column(name = "minSerialNumberLength")
	private Integer minSerialNumberLength;

	@Column(name = "maxSerialNumberLength")
	private Integer maxSerialNumberLength;
	
	@Column(name = "interfaceSplitConfMsg")
	private String interfaceSplitConfMsg;
	
	@Column(name = "toQa")
	private Boolean toQA;

	@Column(name = "toDecant")
	private Boolean toDecant;
	
	@Column(name = "flagOverbookings")
	private Boolean flagOverbookings;
	
	@Column(name = "forcedLocate")
	private Boolean forcedLocate;

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdated")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public Company() {
	}

	public String getInterfaceTemp() {
		return interfaceTemp;
	}

	public void setInterfaceTemp(String interfaceTemp) {
		this.interfaceTemp = interfaceTemp;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getInterfaceInput() {
		return interfaceInput;
	}

	public void setInterfaceInput(String interfaceInput) {
		this.interfaceInput = interfaceInput;
	}

	public String getInterfaceOutput() {
		return interfaceOutput;
	}

	public void setInterfaceOutput(String interfaceOutput) {
		this.interfaceOutput = interfaceOutput;
	}

	public Boolean getInterfaceActive() {
		return interfaceActive;
	}

	public void setInterfaceActive(Boolean interfaceActive) {
		this.interfaceActive = interfaceActive;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public Boolean getMixedPartStorage() {
		return mixedPartStorage;
	}

	public void setMixedPartStorage(Boolean mixedPartStorage) {
		this.mixedPartStorage = mixedPartStorage;
	}

	public Integer getQtyDecimalPlace() {
		return qtyDecimalPlace;
	}

	public void setQtyDecimalPlace(Integer qtyDecimalPlace) {
		this.qtyDecimalPlace = qtyDecimalPlace;
	}

	public Boolean getVirtualPalletLbl() {
		return virtualPalletLbl;
	}

	public void setVirtualPalletLbl(Boolean virtualPalletLbl) {
		this.virtualPalletLbl = virtualPalletLbl;
	}

	public Integer getMinBoxQty() {
		return minBoxQty;
	}

	public void setMinBoxQty(Integer minBoxQty) {
		this.minBoxQty = minBoxQty;
	}

	public Integer getMinPartNumberLength() {
		return minPartNumberLength;
	}

	public void setMinPartNumberLength(Integer minPartNumberLength) {
		this.minPartNumberLength = minPartNumberLength;
	}

	public Integer getMaxPartNumberLength() {
		return maxPartNumberLength;
	}

	public void setMaxPartNumberLength(Integer maxPartNumberLength) {
		this.maxPartNumberLength = maxPartNumberLength;
	}

	public Integer getMinRanNumberLength() {
		return minRanNumberLength;
	}

	public void setMinRanNumberLength(Integer minRanNumberLength) {
		this.minRanNumberLength = minRanNumberLength;
	}

	public Integer getMaxRanNumberLength() {
		return maxRanNumberLength;
	}

	public void setMaxRanNumberLength(Integer maxRanNumberLength) {
		this.maxRanNumberLength = maxRanNumberLength;
	}

	public Integer getMinSerialNumberLength() {
		return minSerialNumberLength;
	}

	public void setMinSerialNumberLength(Integer minSerialNumberLength) {
		this.minSerialNumberLength = minSerialNumberLength;
	}

	public Integer getMaxSerialNumberLength() {
		return maxSerialNumberLength;
	}

	public void setMaxSerialNumberLength(Integer maxSerialNumberLength) {
		this.maxSerialNumberLength = maxSerialNumberLength;
	}

	public String getInterfaceSplitConfMsg() {
		return interfaceSplitConfMsg;
	}

	public void setInterfaceSplitConfMsg(String interfaceSplitConfMsg) {
		this.interfaceSplitConfMsg = interfaceSplitConfMsg;
	}

	public Boolean getToQA() {
		return toQA;
	}

	public void setToQA(Boolean toQA) {
		this.toQA = toQA;
	}

	public Boolean getToDecant() {
		return toDecant;
	}

	public void setToDecant(Boolean toDecant) {
		this.toDecant = toDecant;
	}

	public Boolean getFlagOverbookings() {
		return flagOverbookings;
	}

	public void setFlagOverbookings(Boolean flagOverbookings) {
		this.flagOverbookings = flagOverbookings;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Boolean getForcedLocate() {
		return forcedLocate;
	}

	public void setForcedLocate(Boolean forcedLocate) {
		this.forcedLocate = forcedLocate;
	}

	public String getCompanyReferenceCode() {
		return companyReferenceCode;
	}

	public void setCompanyReferenceCode(String companyReferenceCode) {
		this.companyReferenceCode = companyReferenceCode;
	}

	public Integer getMaxBoxPieceQty() {
		return maxBoxPieceQty;
	}

	public void setMaxBoxPieceQty(Integer maxBoxPieceQty) {
		this.maxBoxPieceQty = maxBoxPieceQty;
	}
	
	

	
	

}
