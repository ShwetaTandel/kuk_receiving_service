package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@NamedQueries(  
	    {  
	@NamedQuery(
		    name="getBodyFromHeaderAndPart",
		    query="SELECT b FROM ReceiptBody b WHERE b.receiptHeader = :receiptHeader and b.partNumber = :partNumber"
		),
	@NamedQuery(
		    name="deleteReceiptBody",
		    query="delete from ReceiptBody where documentReference = :documentReference"
		)
	  }
)

@Entity
@Table(name = "receiptBody")
@JsonIgnoreProperties(ignoreUnknown=true)
public class ReceiptBody implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "documentReference")
	private String documentReference;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "receiptHeaderId", referencedColumnName = "id")
	private ReceiptHeader receiptHeader;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "productTypeId", referencedColumnName = "id")
	private ProductType productType;

	@ManyToOne//(fetch = FetchType.LAZY)
	@JoinColumn(name = "partId", referencedColumnName = "id")
	private Part part;
	
	@Column(name = "partNumber")
	private String partNumber;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DocumentStatusId", referencedColumnName = "id")
	private DocumentStatus documentStatus;

	public DocumentStatus getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus;
	}

	@Column(name = "advisedQty")
	private Double advisedQty;
	
	@Column(name = "qtyAccepted")
	private Double qtyAccepted;

	@Column(name = "receivedQty")
	private Double receivedQty;

	@Column(name = "difference")
	private Double difference;
	
	@Column(name = "lineNo")
	private Integer lineNo;
	
	@Column(name = "vendorCode")
	private String vendorCode;
	
	@Column(name = "caseReference")
	private String caseReference;
	
	@Column(name = "newItems")
	private String newItems;
	
	@Column(name = "inspect")
	private Boolean inspect;
	
	@Column(name = "requiredCountFlag")
	private Boolean countRequiredflag;
	

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date lastUpdatedDate;

	@Column(name = "lastUpdatedBy")
	private String lastUpdatedBy;

	@Column(name = "quarantinedQty")
	private Double quarantinedQty;
	
	@Column(name = "shortage")
	private Boolean shortage;
	
	@OneToMany(mappedBy = "receiptBody")
	private List<ReceiptDetail> receiptDetails = new ArrayList<ReceiptDetail>();

	public ReceiptBody() {
	}

	public Double getQuarantinedQty() {
		return quarantinedQty;
	}

	public void setQuarantinedQty(Double quarantinedQty) {
		this.quarantinedQty = quarantinedQty;
	}

	public List<ReceiptDetail> getReceiptDetails() {
		return receiptDetails;
	}

	public void setReceiptDetails(List<ReceiptDetail> receiptDetails) {
		this.receiptDetails = receiptDetails;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentReference() {
		return documentReference;
	}

	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}

	public ReceiptHeader getReceiptHeader() {
		return receiptHeader;
	}

	public void setReceiptHeader(ReceiptHeader receiptHeader) {
		this.receiptHeader = receiptHeader;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	
	public Double getAdvisedQty() {
		return advisedQty;
	}

	public void setAdvisedQty(Double advisedQty) {
		this.advisedQty = advisedQty;
	}

	public Double getReceivedQty() {
		return receivedQty;
	}

	public void setReceivedQty(Double receivedQty) {
		this.receivedQty = receivedQty;
	}

	public Double getDifference() {
		return difference;
	}

	public void setDifference(Double difference) {
		this.difference = difference;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return lastUpdatedDate;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.lastUpdatedDate = dateUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.lastUpdatedBy = updatedBy;
	}

	public Integer getLineNo() {
		return lineNo;
	}

	public void setLineNo(Integer lineNo) {
		this.lineNo = lineNo;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getCaseReference() {
		return caseReference;
	}

	public void setCaseReference(String caseReference) {
		this.caseReference = caseReference;
	}

	public Double getQtyAccepted() {
		return qtyAccepted;
	}

	public void setQtyAccepted(Double qtyAccepted) {
		this.qtyAccepted = qtyAccepted;
	}

	public Boolean getInspect() {
		return inspect;
	}

	public void setInspect(Boolean inspect) {
		this.inspect = inspect;
	}

	public Boolean getCountRequiredflag() {
		return countRequiredflag;
	}

	public void setCountRequiredflag(Boolean countRequiredflag) {
		this.countRequiredflag = countRequiredflag;
	}

	public String getNewItems() {
		return newItems;
	}

	public void setNewItems(String newItems) {
		this.newItems = newItems;
	}

	public Boolean getShortage() {
		return shortage;
	}

	public void setShortage(Boolean shortage) {
		this.shortage = shortage;
	}
	
	
    
}
