package com.vantec.receiving.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "menuDetail")
public class MenuDetail implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "active")
	private Boolean active;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "menuBodyId", referencedColumnName = "id")
	private MenuBody menuBodyId;

	@Column(name = "description")
	private String description;
	
	@Column(name = "mainSequence")
	private Integer mainSequence;
	
	@Column(name = "subSequence")
	private Integer subSequence;
	
	@Column(name = "menuCode")
	private Integer menuCode;
	
	@Column(name = "action")
	private String action;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public MenuDetail() {
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public MenuBody getMenuBodyId() {
		return menuBodyId;
	}

	public void setMenuBodyId(MenuBody menuBodyId) {
		this.menuBodyId = menuBodyId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMainSequence() {
		return mainSequence;
	}

	public void setMainSequence(Integer mainSequence) {
		this.mainSequence = mainSequence;
	}

	public Integer getSubSequence() {
		return subSequence;
	}

	public void setSubSequence(Integer subSequence) {
		this.subSequence = subSequence;
	}

	public Integer getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(Integer menuCode) {
		this.menuCode = menuCode;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

    
	
	

}
