package com.vantec.receiving.command;

public interface CustomValueGeneratorCommand<T> {

	public T generate(Long id,String name);
}
