package com.vantec.receiving.command;

public class PltNumberGenerator implements CustomValueGeneratorCommand<String>{
	
	

	public PltNumberGenerator() {
	}

	public String generate(Long id,String palletTypeName) {

		String prefix = palletTypeName;

		int maxvalue = 3;
		String documentReference = "";
		int numberOfDigits = 0;
		numberOfDigits = String.valueOf(id).length();
		for (int i = 0; i < maxvalue - numberOfDigits; i++) {
			documentReference += "0";
		}
		documentReference = prefix + documentReference + String.valueOf(id);
		return documentReference;

	}

}
