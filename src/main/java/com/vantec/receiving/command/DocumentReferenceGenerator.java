package com.vantec.receiving.command;

public class DocumentReferenceGenerator implements CustomValueGeneratorCommand<String> {

	public DocumentReferenceGenerator() {
	}

	public String generate(Long id,String prefix) {


		int maxvalue = 6;
		String documentReference = "";
		int numberOfDigits = 0;
		numberOfDigits = String.valueOf(id).length();
		for (int i = 0; i < maxvalue - numberOfDigits; i++) {
			documentReference += "0";
		}
		documentReference = prefix + documentReference + String.valueOf(id);
		return documentReference;

	}

}
