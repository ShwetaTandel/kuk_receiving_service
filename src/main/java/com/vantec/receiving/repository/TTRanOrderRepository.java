package com.vantec.receiving.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.TTRanOrder;

public interface TTRanOrderRepository extends JpaRepository<TTRanOrder, Serializable>{
	
	List<TTRanOrder> findByPartNumberAndCompletedOrderByDateCreated(String partNumber,int completed);
}

