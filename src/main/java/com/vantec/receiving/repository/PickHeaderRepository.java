package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.PickHeader;

public interface PickHeaderRepository  extends JpaRepository<PickHeader, Serializable>{
	
	
	PickHeader findById(Long id);

}
