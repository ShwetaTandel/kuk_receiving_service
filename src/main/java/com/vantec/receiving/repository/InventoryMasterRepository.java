package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.InventoryMaster;

public interface InventoryMasterRepository extends JpaRepository<InventoryMaster, Serializable>{
	
	InventoryMaster findBySerialReferenceAndPartNumber(String serialNumber,String partNumber);

}
