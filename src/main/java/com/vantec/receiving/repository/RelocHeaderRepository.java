package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.RelocHeader;

public interface RelocHeaderRepository extends JpaRepository<RelocHeader, Serializable>{
	
	RelocHeader findByDocumentReference(String documentReference);

}

