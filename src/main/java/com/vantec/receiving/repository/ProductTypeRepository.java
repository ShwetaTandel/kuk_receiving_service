package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.ProductType;

public interface ProductTypeRepository extends JpaRepository<ProductType, Serializable>{

	ProductType findById(Long id);
	
	ProductType findByProductTypeCode(String name);

}
