package com.vantec.receiving.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.DocumentStatus;
import com.vantec.receiving.entity.ReceiptHeader;

public interface ReceiptHeaderRepository extends JpaRepository<ReceiptHeader, Serializable>{

	ReceiptHeader findByDocumentReference(String documentReference);
	ReceiptHeader findByDocumentReferenceAndDocumentStatus(String documentReference,DocumentStatus status);
	
	ReceiptHeader findByCustomerReference(String grnReference);
	
	List<ReceiptHeader> findByHaulierReference(String haulierReference);
	
	List<ReceiptHeader> findByHaulierReferenceAndDocumentStatus(String haulierReference,DocumentStatus status);
	
	ReceiptHeader findByDeliveryNoteAndHaulierReference(String deliveryNote,String haulierReference);


}
