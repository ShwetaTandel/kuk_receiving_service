package com.vantec.receiving.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.receiving.entity.Part;

public interface PartRepository extends JpaRepository<Part, Serializable>{

	Part findByPartNumber(String partNumber);
	
	@Query("Select count(p) from Part p where p.partNumber in (:parts) and p.isAS400 = false")
    Integer checkIfPartsExist(@Param("parts") List<String> parts);


}
