package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.Company;

public interface CompanyRepository extends JpaRepository<Company, Serializable>{

	Company findByCompanyName(String companyName);
}

