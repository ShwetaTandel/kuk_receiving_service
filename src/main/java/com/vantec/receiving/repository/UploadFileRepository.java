package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vantec.receiving.entity.UploadFile;
import com.vantec.receiving.util.ConstantHelper;

public interface UploadFileRepository extends JpaRepository<UploadFile, Serializable>{
	
	@Query(value="SELECT * FROM upload_file r WHERE r.type in ('"+ConstantHelper.RETURN_FILE_TYPE_OFFLOD +"','"+ConstantHelper.RETURN_FILE_TYPE_PLANNED_RTS +"','"+ConstantHelper.RETURN_FILE_TYPE_UNPLANNED_RTS + "') order by id desc limit 1",nativeQuery = true)
	UploadFile findLatestReturnFile();



}
