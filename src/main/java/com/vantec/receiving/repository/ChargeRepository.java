package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.ChargeMaster;

public interface ChargeRepository extends JpaRepository<ChargeMaster, Serializable>{

	ChargeMaster findById(Long id);
	
	ChargeMaster findByChargeType(String chargeType);
}

