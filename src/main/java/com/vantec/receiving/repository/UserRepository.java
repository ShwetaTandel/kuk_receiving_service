package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.User;

public interface UserRepository extends JpaRepository<User, Serializable>{

	User findByName(String name);


}
