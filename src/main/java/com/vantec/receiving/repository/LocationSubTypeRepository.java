package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.LocationSubType;



public interface LocationSubTypeRepository extends JpaRepository<LocationSubType, Serializable>{
	
	LocationSubType findById(Long id);
	
	LocationSubType findByLocationSubTypeCode(String locationSubTypeCode);
}
