package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vantec.receiving.entity.RtsHeader;
import com.vantec.receiving.util.ConstantHelper;

public interface RtsHeaderRepository extends JpaRepository<RtsHeader, Serializable>{

	RtsHeader findByOrderReference(String orderReference);
	
	@Query("select count(t) from RtsHeader t where t.orderType = '"+ConstantHelper.UNPLANNED_RTS+"'")
	Integer findMaxIDForUnplannedRTS();


}
