package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.DownloadFile;

public interface DownloadFileRepository extends JpaRepository<DownloadFile, Serializable>{
}
