package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.receiving.entity.DocumentStatus;
import com.vantec.receiving.entity.RtsBody;
import com.vantec.receiving.entity.RtsDetail;

public interface RtsDetailRepository extends JpaRepository<RtsDetail, Serializable>{
	
//	@Query("select count(t) from RtsDetail t where t.serialReference like 'URTS%'")
//	Integer findMaxIDForUnplannedRTSSerial();
	
	@Modifying
    @Query("UPDATE RtsDetail c SET c.documentStatus = :documentStatus , c.lastUpdatedBy =:lastUpdatedBy, c.lastUpdated = now() WHERE c.rtsBody = :rtsBody")
    int updateStatusByBody(@Param("rtsBody") RtsBody rtsBody, @Param("lastUpdatedBy") String updatedBy,@Param("documentStatus") DocumentStatus documentStatus);

	@Modifying
    @Query("UPDATE RtsDetail c SET c.documentStatus = :documentStatus , c.lastUpdatedBy =:lastUpdatedBy, c.lastUpdated = now(), c.qtyTransacted =:qtyTransacted WHERE c.rtsBody = :rtsBody")
    int updateStatusAndTransactedQtyByBody(@Param("rtsBody") RtsBody rtsBody, @Param("lastUpdatedBy") String updatedBy,@Param("documentStatus") DocumentStatus documentStatus, @Param("qtyTransacted") Double qtyTransacted);




}
