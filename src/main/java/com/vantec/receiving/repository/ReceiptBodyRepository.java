package com.vantec.receiving.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.receiving.entity.DocumentStatus;
import com.vantec.receiving.entity.ReceiptBody;
import com.vantec.receiving.entity.ReceiptHeader;

public interface ReceiptBodyRepository extends JpaRepository<ReceiptBody, Serializable>{

	ReceiptBody findByDocumentReferenceAndPartNumber(String documentReference,String partNumber);

	ReceiptBody findByPartNumber(String partNumber);
	
	ReceiptBody findByReceiptHeaderAndLineNo(ReceiptHeader receiptHeader,Integer lineNo);
	
	List<ReceiptBody> findByDocumentReference(String documentReference);
	
	List<ReceiptBody> findByDocumentReferenceAndDocumentStatus(String documentReference, DocumentStatus status);
	
	List<ReceiptBody> findByPartNumberAndReceiptHeader(String partNumber,ReceiptHeader receiptHeader);
	
	List<ReceiptBody> findByCaseReferenceAndDocumentReferenceAndDocumentStatus(String caseReference, String documentReference, DocumentStatus status);
	
	ReceiptBody findById(Long id);
	List<ReceiptBody> findByReceiptHeader(ReceiptHeader receiptHeader);
	
	@Query("select t from ReceiptBody t where t.documentReference = :documentReference and t.documentStatus != :documentStatus")
	List<ReceiptBody>  findByDocumentReferenceAndStatusNotTransmitted(@Param("documentReference") String documentReference, @Param("documentStatus") DocumentStatus documentStatus);

	
	@Query("select count(t) from ReceiptBody t where t.documentReference = :documentReference and t.quarantinedQty > 0")
    Integer findCountOfQuarantinedBodies(@Param("documentReference") String documentReference);
	
	@Query("select count(t) from ReceiptBody t where t.documentReference = :documentReference and (t.advisedQty != t.receivedQty or t.advisedQty=0)")
    Integer findBodiesWithDiscrepancies(@Param("documentReference") String documentReference);
	
	@Query("select count(t) from ReceiptBody t where t.documentReference = :documentReference")
    Integer findCountOfAllBodies(@Param("documentReference") String documentReference);
	
	@Query("select count(t) from ReceiptBody t where t.documentReference = :documentReference and t.documentStatus = :documentStatus")
    Integer findCountOfAllBodiesWithStatus(@Param("documentReference") String documentReference, @Param("documentStatus") DocumentStatus documentStatus);
	
	@Modifying
    @Query("UPDATE ReceiptBody c SET c.documentStatus = :documentStatus , c.lastUpdatedBy =:lastUpdatedBy, c.lastUpdatedDate = now() WHERE c.documentReference = :documentReference and c.caseReference = :caseReference")
    int updateStatusByCase(@Param("documentReference") String documentReference,@Param("caseReference") String caseReference, @Param("lastUpdatedBy") String updatedBy,@Param("documentStatus") DocumentStatus documentStatus);
	
	@Modifying
    @Query("UPDATE ReceiptBody c SET c.documentStatus = :documentStatus , c.lastUpdatedBy =:lastUpdatedBy, c.lastUpdatedDate = now() WHERE c.documentReference = :documentReference")
    int updateStatusByDoc(@Param("documentReference") String documentReference, @Param("lastUpdatedBy") String updatedBy,@Param("documentStatus") DocumentStatus documentStatus);


}
