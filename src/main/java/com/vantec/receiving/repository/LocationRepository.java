package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.Location;

public interface LocationRepository extends JpaRepository<Location, Serializable>{

	Location findByLocationCode(String locationCode);
	
	Location findByLocationHash(String hashCode);


}
