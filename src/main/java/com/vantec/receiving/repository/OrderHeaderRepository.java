package com.vantec.receiving.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.OrderHeader;
import com.vantec.receiving.entity.PickHeader;

public interface OrderHeaderRepository extends JpaRepository<OrderHeader, Serializable>{
	
	OrderHeader findByPickHeader(PickHeader pickHeader);
	
	List<OrderHeader> findByCustomerReferenceAndPickHeaderNotNull(String custReference);
	
	List<OrderHeader> findByCustomerReference(String custReference);

}
