package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.DocumentStatus;

public interface DocumentStatusRepository extends JpaRepository<DocumentStatus, Serializable>{

	DocumentStatus findByDocumentStatusCode(String documentStatusCode);
}
