package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.InventoryStatus;

public interface InventoryStatusRepository extends JpaRepository<InventoryStatus, Serializable>{

	InventoryStatus findByInventoryStatusCode(String status);

}
