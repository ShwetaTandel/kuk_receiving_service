package com.vantec.receiving.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.receiving.entity.DocumentStatus;
import com.vantec.receiving.entity.OrderBody;
import com.vantec.receiving.entity.OrderHeader;
import com.vantec.receiving.entity.Part;

public interface OrderBodyRepository extends JpaRepository<OrderBody, Serializable>{
	
	OrderBody findById(Long id);

	List<OrderBody> findByOrderHeader(OrderHeader orderHeader);
	
	OrderBody findByOrderHeaderAndPart(OrderHeader orderHeader,Part part);
	
	OrderBody findByPartNumberAndLineNoAndCustomerReference(String partNumber, Integer lineNo, String custRef);
	
	List<OrderBody> findByPartNumberAndCustomerReference(String partNumber, String custRef);
	
	@Query("SELECT c.partNumber, SUM(c.qtyExpected), c.part, c.zoneDestination FROM OrderBody c where c.orderHeader in(:orderHeaders) GROUP BY c.partNumber, c.zoneDestination")
	List<Object[]>  findBodiesGroupByPartAndZoneForOrderHeaders(@Param("orderHeaders")List<OrderHeader> orderHeaders);
	
	@Query("SELECT c FROM OrderBody c where c.orderHeader in(:orderHeaders) and c.qtyTransacted = c.qtyExpected and c.documentStatus =:documentStatus")
	List<OrderBody>  findAllPickedBodiesForHeaders(@Param("orderHeaders")List<OrderHeader> orderHeaders, @Param("documentStatus")DocumentStatus documentStatus);
	
	@Query("SELECT count(c) FROM OrderBody c where c.orderHeader =:orderHeader and c.documentStatus =:documentStatus")
	Integer  findCountOfOpenBodies(@Param("orderHeader")OrderHeader orderHeader, @Param("documentStatus")DocumentStatus documentStatus);
	
	
}

