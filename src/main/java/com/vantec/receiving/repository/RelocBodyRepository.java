package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.receiving.entity.RelocBody;
import com.vantec.receiving.entity.RelocHeader;

public interface RelocBodyRepository extends JpaRepository<RelocBody, Serializable>{
	
	@Query("Select count(t) from RelocBody t where t.processed = false and relocHeader=:relocHeader")
	Integer findCountOfUnprocessedBodies(@Param("relocHeader")  RelocHeader relocHeader);

}

