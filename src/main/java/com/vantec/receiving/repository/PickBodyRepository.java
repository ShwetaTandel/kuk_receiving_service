package com.vantec.receiving.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.receiving.entity.PickBody;
import com.vantec.receiving.entity.PickHeader;

public interface PickBodyRepository  extends JpaRepository<PickBody, Serializable>{
	
	@Query("SELECT count(r) FROM PickBody r WHERE r.partNumber = :partNumber and r.qtyShortage > 0")
	Integer findByPartNumberHavingShortageQty(@Param("partNumber") String partNumber);
	
	PickBody findByPickHeaderAndPartNumberAndProcessed(PickHeader pickHeader, String partNumber, Boolean processed);
	
	
	@Query("Select distinct(p.documentReference) from PickBody p join p.pickHeader h where h.pickType = 'FAB' and p.qtyShortage > 0 and p.partNumber in (:parts)")
    List<String> checkShortagesForFABOrders(@Param("parts") List<String> parts);
	

	@Query("Select distinct(p.documentReference) from PickBody p join p.pickHeader h where h.pickType = 'AAA' and p.qtyShortage > 0 and p.partNumber in (:parts)")
    List<String> checkShortagesForAAAOrders(@Param("parts") List<String> parts);
	

	//@Query("Select distinct(p.documentReference) from PickBody p join p.pickHeader h where h.pickType = 'AAA' and p.qtyShortage > 0 and p.partNumber in (:parts) UNION Select distinct(p.documentReference) from PickBody p join p.pickHeader h where h.pickType is not null and p.qtyShortage > 0 and p.partNumber in (:parts)")
    //List<String> checkShortagesForOrders(@Param("parts") List<String> parts);
	
}
