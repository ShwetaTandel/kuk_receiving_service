package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.Pallet;

public interface PalletRepository extends JpaRepository<Pallet, Serializable>{

	Pallet findByPltNumber(String pltNumber);


}
