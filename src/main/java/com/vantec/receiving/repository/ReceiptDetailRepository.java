package com.vantec.receiving.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.receiving.entity.DocumentStatus;
import com.vantec.receiving.entity.ReceiptBody;
import com.vantec.receiving.entity.ReceiptDetail;

public interface ReceiptDetailRepository extends JpaRepository<ReceiptDetail, Serializable>{

	ReceiptDetail findByDocumentReferenceAndPartNumberAndSerialReference(String documentReference,String partNumber,String serialReference);
	
	ReceiptDetail findBySerialReference(String serialReference);
	
	List<ReceiptDetail> findByDocumentReference(String documentReference);
	
	List<ReceiptDetail> findByDocumentReferenceAndPartNumber(String documentReference,String partNumber);
	
	ReceiptDetail findBySerialReferenceAndPartNumberAndReceiptBody(String serialReference,String partNumber,ReceiptBody receiptBody);
	
	
	@Query("SELECT c FROM ReceiptDetail c WHERE c.receiptBody = :receiptBody and c.partNumber = :partNumber and c.receivedQty > 0")
	List<ReceiptDetail> findByPartNumberAndReceiptBodyAndReceivedQtyGreaterThanZero(@Param("partNumber")String partNumber,@Param("receiptBody")ReceiptBody receiptBody);
	List<ReceiptDetail>  findByReceiptBody(ReceiptBody receiptBody);
	
	@Modifying
    @Query("UPDATE ReceiptDetail c SET c.documentStatus = :documentStatus , c.lastUpdatedBy =:lastUpdatedBy, c.lastUpdatedDate = now() WHERE c.receiptBody = :receiptBody")
    int updateStatusByBody(@Param("receiptBody") ReceiptBody receiptBody, @Param("lastUpdatedBy") String updatedBy,@Param("documentStatus") DocumentStatus documentStatus);

	
//	
//	@Query("select count(t) from ReceiptDetail t where t.serialReference like 'RCPLBL%'")
//	Integer findMaxIDForRCPOdetteLabelSerials();
//	
//	@Query("select count(t) from ReceiptDetail t where t.serialReference like 'STKTNFR%'")
//	Integer findMaxIDForOffsiteStorageSerials();


}
