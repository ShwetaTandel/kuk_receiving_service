package com.vantec.receiving.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.receiving.entity.TransactionType;

public interface TransactionTypeRepository extends JpaRepository<TransactionType, Serializable>{
	
	TransactionType findByTransactionTypeCode(String transactionTypeCode);

}
