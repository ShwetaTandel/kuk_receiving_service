package com.vantec.receiving.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vantec.receiving.entity.Vendor;

public interface VendorRepository extends JpaRepository<Vendor, Serializable>{
	
	Vendor findById(Long id);
	
	Vendor findByVendorReferenceCode(String vendorCode);
	
	@Query("SELECT distinct p.vendorReferenceCode FROM Vendor p")
	List<String>  findAllVendorNames();
	

}
