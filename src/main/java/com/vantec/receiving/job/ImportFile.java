package com.vantec.receiving.job;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.vantec.receiving.entity.Company;
import com.vantec.receiving.repository.CompanyRepository;
import com.vantec.receiving.service.ReceiptService;

@Component
public class ImportFile {

	private static Logger logger = LoggerFactory.getLogger(ImportFile.class);

	@Autowired
	CompanyRepository companyRepository;

	@Autowired
	ReceiptService receiptService;

	@Scheduled(fixedRate = 20000)
	public void readGRNFile() {

		try {

			String inpPath = getPath();
			String tmpPath = getTempPath();
			List<File> filesInFolder = Files.walk(Paths.get(inpPath)).filter(Files::isRegularFile).map(Path::toFile)
					.collect(Collectors.toList());
			filesInFolder = filesInFolder.stream().filter(file -> file.getName().startsWith("GRN")).filter(file -> file.getName().endsWith(".txt"))
					.collect(Collectors.toList());

			for (File file : filesInFolder) {
				String fileName = file.getName();
				logger.info("reading file " + file.getName());
				try {
					receiptService.readReceipt(file);
					deleteFile(file);
				} catch (DataIntegrityViolationException ex) {
					file.renameTo(new File(tmpPath + "/" + fileName));
					logger.error("Moving file to temp folder due to error -"+fileName + ex);
					logger.error(ex.getMessage(), ex);
					throw new EntityNotFoundException(ex.getMessage());
				} catch (ParseException ex) {
					file.renameTo(new File(tmpPath + "/" + fileName));
					logger.error("Moving file to temp folder due to error-"+fileName + ex);
					logger.error(ex.getMessage(), ex);
					throw new EntityNotFoundException(ex.getMessage());
				} catch (Exception ex) {
					file.renameTo(new File(tmpPath + "/" + fileName));
					logger.error(ex.getMessage(), ex);
					logger.error("Moving file to temp folder due to error-"+fileName + ex);
					throw new DataIntegrityViolationException(ex.getMessage());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Scheduled(fixedRate = 25000)
	public void readRVSFile() {

		try {
			String inpPath = getPath();
			String tmpPath = getTempPath();

			List<File> filesInFolder = Files.walk(Paths.get(inpPath)).filter(Files::isRegularFile).map(Path::toFile)
					.collect(Collectors.toList());
			List<File> allFilesInFolder = new ArrayList<File>();
			allFilesInFolder.addAll(filesInFolder.stream().filter(file -> file.getName().startsWith("RVS")).filter(file -> file.getName().endsWith(".txt"))
					.collect(Collectors.toList()));
			allFilesInFolder.addAll(filesInFolder.stream().filter(file -> file.getName().startsWith("REVERS")).filter(file -> file.getName().endsWith(".txt"))
					.collect(Collectors.toList()));

			for (File file : allFilesInFolder) {
				String fileName = file.getName();
				logger.info("reading file " + file.getName());
				try {
					receiptService.deleteReceipt(file);
					deleteFile(file);
				} catch (DataIntegrityViolationException ex) {
					file.renameTo(new File(tmpPath + "/" + fileName));
					logger.error("Moving file to temp folder due to error -"+fileName  + ex);
					logger.error(ex.getMessage(), ex);
					throw new EntityNotFoundException(ex.getMessage());
				} catch (ParseException ex) {
					file.renameTo(new File(tmpPath + "/" + fileName));
					logger.error("Moving file to temp folder due to error-"+fileName  + ex);
					logger.error(ex.getMessage(), ex);
					throw new EntityNotFoundException(ex.getMessage());
				} catch (Exception ex) {
					file.renameTo(new File(tmpPath + "/" + fileName));
					logger.error("Moving file to temp folder due to error-"+fileName  + ex);
					logger.error(ex.getMessage(), ex);
					throw new DataIntegrityViolationException(ex.getMessage());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Scheduled(fixedRate = 25000)
	public void readRTSFile() {

		try {
			String inpPath = getPath();
			String tmpPath = getTempPath();

			List<File> filesInFolder = Files.walk(Paths.get(inpPath)).filter(Files::isRegularFile).map(Path::toFile)
					.collect(Collectors.toList());
			List<File> allFilesInFolder = new ArrayList<File>();
			allFilesInFolder.addAll(filesInFolder.stream().filter(file -> file.getName().startsWith("RTS")).filter(file -> file.getName().endsWith(".txt"))
					.collect(Collectors.toList()));
			allFilesInFolder.addAll(filesInFolder.stream().filter(file -> file.getName().startsWith("TORTS")).filter(file -> file.getName().endsWith(".txt"))
					.collect(Collectors.toList()));

			for (File file : allFilesInFolder) {
				String fileName = file.getName();
				logger.info("reading file " + file.getName());
				try {
					receiptService.returnToStore(file);
					deleteFile(file);
				} catch (DataIntegrityViolationException ex) {
					file.renameTo(new File(tmpPath + "/" + fileName));
					logger.error("Moving file to temp folder due to error- "+fileName  + ex);
					logger.error(ex.getMessage(), ex);
					throw new EntityNotFoundException(ex.getMessage());
				} catch (ParseException ex) {
					file.renameTo(new File(tmpPath + "/" + fileName));
					logger.error("Moving file to temp folder due to error- "+fileName + ex);
					logger.error(ex.getMessage(), ex);
					throw ex;
				} catch (Exception ex) {
					file.renameTo(new File(tmpPath + "/" + fileName));
					logger.error("Moving file to temp folder due to error- "+fileName + ex);
					logger.error(ex.getMessage(), ex);
					throw ex;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Scheduled(fixedRate = 25000)
	public void readRelocFile() {

		try {
			String inpPath = getPath();
			String tmpPath = getTempPath();

			List<File> filesInFolder = Files.walk(Paths.get(inpPath)).filter(Files::isRegularFile).map(Path::toFile)
					.collect(Collectors.toList());
			filesInFolder = filesInFolder.stream().filter(file -> file.getName().startsWith("REL")).filter(file -> file.getName().endsWith(".txt"))
					.collect(Collectors.toList());

			for (File file : filesInFolder) {
				String fileName = file.getName();
				logger.info("reading file " + file.getName());
				try {
					receiptService.readReloc(file);
					deleteFile(file);
				} catch (DataIntegrityViolationException ex) {
					file.renameTo(new File(tmpPath + "/" + fileName));
					logger.error("Moving file to temp folder due to error- "+fileName);
					logger.error(ex.getMessage(), ex);
					throw new EntityNotFoundException(ex.getMessage());
				} catch (ParseException ex) {
					file.renameTo(new File(tmpPath + "/" + fileName));
					logger.error("Moving file to temp folder due to error- "+fileName);
					logger.error(ex.getMessage(), ex);
					throw new EntityNotFoundException(ex.getMessage());
				} catch (Exception ex) {
					file.renameTo(new File(tmpPath + "/" + fileName));
					logger.error("Moving file to temp folder due to error- "+fileName);
					logger.error(ex.getMessage(), ex);
					throw new DataIntegrityViolationException(ex.getMessage());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Scheduled(fixedRate = 25000)
	public void readCngPnoFile() {

		try {
			String inpPath = getPath();
			String tmpPath = getTempPath();

			List<File> filesInFolder = Files.walk(Paths.get(inpPath)).filter(Files::isRegularFile).map(Path::toFile)
					.collect(Collectors.toList());
			filesInFolder = filesInFolder.stream().filter(file -> file.getName().startsWith("CHGPNO")).filter(file -> file.getName().endsWith(".txt"))
					.collect(Collectors.toList());

			for (File file : filesInFolder) {
				String fileName = file.getName();
				logger.info("reading file " + file.getName());
				try {
					receiptService.readChgPno(file);
					deleteFile(file);
				} catch (DataIntegrityViolationException ex) {
					file.renameTo(new File(tmpPath + "/" + fileName));
					logger.error("Moving file to temp folder due to error- "+fileName);
					logger.error(ex.getMessage(), ex);
					throw new EntityNotFoundException(ex.getMessage());
				} catch (ParseException ex) {
					file.renameTo(new File(tmpPath + "/" + fileName));
					logger.error("Moving file to temp folder due to error- "+fileName);
					logger.error(ex.getMessage(), ex);
					throw new EntityNotFoundException(ex.getMessage());
				} catch (Exception ex) {
					file.renameTo(new File(tmpPath + "/" + fileName));
					logger.error("Moving file to temp folder due to error- "+fileName);
					logger.error(ex.getMessage(), ex);
					throw new DataIntegrityViolationException(ex.getMessage());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getPath() {
		List<Company> companys = companyRepository.findAll();
		return companys.get(0).getInterfaceInput();
	}

	private String getTempPath() {
		List<Company> companys = companyRepository.findAll();
		return companys.get(0).getInterfaceTemp();
	}

	private void deleteFile(File file) {

		try {
			logger.info("deleting file " + file.getName());
			file.delete();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

	}

	public static void main(String arg[]) {
		// try {
		String qty;
		try {
			qty = String.format("%.3f", Float.valueOf(39000 / 1000));
		} catch (Exception ex) {
			qty = "0.000";
			logger.error(ex.getMessage(), ex);
		}
		// List<File> filesInFolder =
		// Files.walk(Paths.get("X:\\AKM\\")).filter(Files::isRegularFile).map(Path::toFile).collect(Collectors.toList());
		// for(File file:filesInFolder){
		// System.out.println("reading file " + file.getName());;
		// }
		/*
		 * int maxId = 12365; String formattedId = "URTS"; if(maxId < 999){
		 * formattedId += String.format("%04d", maxId +1 ); }else{ formattedId
		 * += (maxId +1) ; } System.out.println(formattedId); } catch (Exception
		 * e) { // TODO Auto-generated catch block e.printStackTrace(); }
		 */

	}

}
