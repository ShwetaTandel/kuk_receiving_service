package com.vantec.receiving.controller;


import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.vantec.receiving.entity.ReceiptHeader;
import com.vantec.receiving.exception.VantecReceivingException;
import com.vantec.receiving.model.CloseDTO;
import com.vantec.receiving.model.DeleteDocumentDetailRequest;
import com.vantec.receiving.model.HaulierReferenceDTO;
import com.vantec.receiving.model.UnplannedRTSDTO;
import com.vantec.receiving.model.VendorDTO;
import com.vantec.receiving.service.ReceivingService;

@RestController
@RequestMapping("/")
public class ReceivingController {
	
	
	private static Logger logger = LoggerFactory.getLogger(ReceivingController.class);
	
	
	@Autowired
	ReceivingService receivingService;
	
	//Release receipt from quarantine
	@RequestMapping(value = "/releaseReceipt", method = RequestMethod.GET)
	public ResponseEntity<Boolean> releaseReceipt(@RequestParam("documentReference") String documentReference, @RequestParam("updatedBy") String user)
			throws VantecReceivingException {

		try{
			 logger.info("releaseReceipt called..");
			 Boolean success = receivingService.releaseReceipt(documentReference,user);
			return new ResponseEntity<Boolean>(success, HttpStatus.OK);
	    }catch(Exception ex){
	    	logger.error("Values : "   +  "documentReference : " + documentReference );
	    	logger.error(ex.getMessage(), ex);
	    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	}
	
	//Process RTS
	@RequestMapping(value = "/processRTS", method = RequestMethod.GET)
		public ResponseEntity<Boolean> processRTS(@RequestParam("headerIds") String headerIds, @RequestParam("updatedBy") String user, @RequestParam("notReturned") Boolean notReturned)
				throws VantecReceivingException {

			try{
				 logger.info("processRTS called..");
				 Boolean success = receivingService.processRTS(headerIds, user, notReturned);
				return new ResponseEntity<Boolean>(success, HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : "   +  "documentReference : " + headerIds );
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
		}
	
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public ResponseEntity<Boolean> test()
			throws VantecReceivingException {

		try{
			String uri = "http://localhost:8095/pick/test";
		    RestTemplate restTemplate = new RestTemplate();
		    //Object result = restTemplate.getForObject(uri, String.class);
		    Object request = null;
			restTemplate.postForObject(uri, request, String.class);
			 //logger.info("processRTS called.."+result);
	    }catch(Exception ex){
	    	logger.error("Values : "   +  "documentReference : " );
	    	logger.error(ex.getMessage(), ex);
	    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
	    }
		return null;
	}


	
	/** add unplanned RTS
	 * 
	 * 
	 * @param haulierReferenceDTO
	 * @return
	 * @throws VantecReceivingException
	 */
	@RequestMapping(value = "/addUnplannedRTS", method = RequestMethod.POST)
	public ResponseEntity<String> addUnplannedRTS(@RequestBody UnplannedRTSDTO unplannedRTS) throws VantecReceivingException {
		   
		   
	    	try{
				logger.info("addUnplannedRTS called..");
				Boolean success = receivingService.addUnplannedRTS(unplannedRTS);
				return new ResponseEntity<String>(success.toString(), HttpStatus.OK);
				 
					
		    }catch(Exception ex){
		    	logger.error("HaulierReferenceDTO Values : "   +  "partNumber  : " + unplannedRTS.getPartNumber() );
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	 	}
	
	/****
	 * 
	 * @param grnReference
	 * @param user
	 * @return
	 * @throws VantecReceivingException
	 */
	@RequestMapping(value = "/addDetailsForOdetteLabel", method = RequestMethod.GET)
			public ResponseEntity<String> addDetailsForOdetteLabel(@RequestParam("grnReference") String grnReference, @RequestParam("updatedBy") String user)
					throws VantecReceivingException {

				try{
					 logger.info("addDetailsForOdetteLabel called..");
					 String success = receivingService.addDetailsForOdetteLabel(grnReference, user);
					 return new ResponseEntity<String>(success, HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +  "grnref : " + grnReference );
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
			}
	
	//Get Receipt Document
	@RequestMapping(value = "/getAllReceiptDocs",params = { "page", "size" }, method = RequestMethod.GET)
	public ResponseEntity<List<ReceiptHeader>> getAllReceiptDocs(@RequestParam( "page" ) Integer page, @RequestParam( "size" ) Integer size) {
		
		try{
			logger.info("getAllReceiptDocs called..");
			List<ReceiptHeader> receipts = receivingService.getAllReceiptDocs(page,size);
			return new ResponseEntity<List<ReceiptHeader>>(receipts,HttpStatus.OK);
	    }catch(Exception ex){
	    	logger.error("ReceiptHeaderDTO Values : "   +  "page : " + page +
	    			                                      "size : " + size );
	    	logger.error(ex.getMessage(), ex);
	    	return new ResponseEntity<List<ReceiptHeader>>(HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	}
	
	
	@RequestMapping(value = "/addHaulierReferenceToMultipleReceipt", method = RequestMethod.POST)
	public ResponseEntity<String> addHaulierReferenceToMultipleReceipt(@RequestBody HaulierReferenceDTO haulierReferenceDTO) throws VantecReceivingException {
		   
		   
	    	try{
				logger.info("addHaulierReferenceToMultipleReceipt called..");
				receivingService.addHaulierReferenceToMultipleReceipt(haulierReferenceDTO);
			    return new ResponseEntity<String>("addHaulierReferenceToMultipleReceipt called", HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("HaulierReferenceDTO Values : "   +  "haulireference : " + haulierReferenceDTO.getHaulierReference() +
		    			                                      "documentreference : " + haulierReferenceDTO.getDocumentReferences() );
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	 	}
	
	@RequestMapping(value = "/deleteHaulierReferenceToMultipleReceipt", method = RequestMethod.POST)
	public ResponseEntity<String> deleteHaulierReferenceToMultipleReceipt(@RequestBody HaulierReferenceDTO haulierReferenceDTO) throws VantecReceivingException {
		   
		   
	    	try{
				logger.info("deleteHaulierReferenceToMultipleReceipt called..");
				 receivingService.deleteHaulierReferenceToMultipleReceipt(haulierReferenceDTO);
			    	return new ResponseEntity<String>("deleteHaulierReferenceToMultipleReceipt called", HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("HaulierReferenceDTO Values : "   +  "haulireference : " + haulierReferenceDTO.getHaulierReference() +
		    			                                      "documentreference : " + haulierReferenceDTO.getDocumentReferences() );
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	 	}
	
	@RequestMapping(value = "/addHaulierRefernecToSingleReceipt", method = RequestMethod.POST)
	public ResponseEntity<Boolean> addHaulierRefernecToSingleReceipt(@RequestParam("haulierReference") String haulierReference,@RequestParam("documentReference") String documentReference)
			throws VantecReceivingException {
		
		try{
			logger.info("addHaulierRefernecToSingleReceipt called..");
			Boolean isCompleted = false;
			isCompleted = receivingService.addHaulierRefernecToSingleReceipt(haulierReference,documentReference);
			return new ResponseEntity<Boolean>(isCompleted, HttpStatus.OK);
	    }catch(Exception ex){
	    	logger.error("Values : "   +  "haulireference : " + haulierReference +
	    			                                      "documentreference : " + documentReference );
	    	logger.error(ex.getMessage(), ex);
	    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	}
	
	@RequestMapping(value = "/deleteDocumentDetail", method = RequestMethod.POST)
	public ResponseEntity<Boolean> deleteDocumentDetail(@RequestBody DeleteDocumentDetailRequest delReq) throws VantecReceivingException {
		   
	    	try{
				 logger.info("deleteDocumentDetail called..");
				 Boolean isDeleted = false;
				 isDeleted = receivingService.deleteDocumentDetail(delReq);
			     return new ResponseEntity<Boolean>(isDeleted, HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("DeleteDocumentDetailRequest Values : "   +  "documentreference : " + delReq.getDocumentReference() +
		    			                                      "partNumber : " + delReq.getPartNumber() );
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	 	}
	
	@RequestMapping(value = "/deleteDocument", method = RequestMethod.POST) 
	public ResponseEntity<Boolean> deleteDocument(@RequestParam("documentReference") String documentReference){
		    
	    	try{
				 logger.info("deleteDocument called..");
				 Boolean isDeleted = false;
				 isDeleted = receivingService.deleteDocument(documentReference);
			     return new ResponseEntity<Boolean>(isDeleted, HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : "   +  "documentreference : " + documentReference );
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	    	
	 	}
	
	@RequestMapping(value = "/validateHaulierReference", method = RequestMethod.GET)
	public ResponseEntity<Boolean> validateHaulierReference(@RequestParam("haulierReference") String haulierReference)
			throws VantecReceivingException {

		try{
			 logger.info("validateHaulierReference called..");
			 Boolean isExist = receivingService.validateHaulierReference(haulierReference);
			return new ResponseEntity<Boolean>(isExist, HttpStatus.OK);
	    }catch(Exception ex){
	    	logger.error("Values : "   +  "haulierReference : " + haulierReference );
	    	logger.error(ex.getMessage(), ex);
	    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	}
	
	@RequestMapping(value = "/createVendor", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createVendor(@RequestBody VendorDTO vendorDTO) throws ParseException {
			
			try{
				 logger.info("createVendor called..");
				 return new ResponseEntity<Boolean>(receivingService.createVendor(vendorDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("VendorDTO Values : "   +  "vendon Name  : " + vendorDTO.getVendorName() );
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	@RequestMapping(value = "/editVendor", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editVendor(@RequestBody VendorDTO vendorDTO) throws ParseException {
			
			try{
				 logger.info("editVendor called..");
				 return new ResponseEntity<Boolean>(receivingService.editVendor(vendorDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("VendorDTO Values : "   +  "vendon Name  : " + vendorDTO.getVendorName() );
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	@RequestMapping(value = "/closeReceipt", method = RequestMethod.POST)
	public ResponseEntity<Boolean> closeReceipt(@RequestBody CloseDTO closeDTO)
			throws VantecReceivingException, IOException {
		
		
		try{
			logger.info("closeReceipt called.. from screen");
			Boolean isCompleted = false;
			isCompleted = receivingService.closeReceipt(closeDTO.getDocumentReference(),closeDTO.getUserId(), true);
			return new ResponseEntity<Boolean>(isCompleted, HttpStatus.OK);
	    }catch(Exception ex){
	    	logger.error("CloseDTO Values : "   +  "documentReference : " + closeDTO.getDocumentReference() +
	    			                               "serialReference" + closeDTO.getUserId());
	    	logger.error(ex.getMessage(), ex);
	    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	}
	

}
