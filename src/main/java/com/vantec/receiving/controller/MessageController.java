package com.vantec.receiving.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vantec.receiving.model.OffsiteReturnMessageDTO;
import com.vantec.receiving.model.RTSReturnMessageDTO;
import com.vantec.receiving.service.MessageService;
import com.vantec.receiving.service.ReceivingService;

@RestController
@RequestMapping("/")
public class MessageController {

	private static Logger logger = LoggerFactory.getLogger(MessageController.class);

	@Autowired
	MessageService messageService;
	
	@Autowired
	ReceivingService receivingService;

	@RequestMapping(value = "/writeCompleteReceiptMessage", method = RequestMethod.POST)
	public ResponseEntity<Boolean> writeCompleteReceiptMessage(String documentReference,String userId) throws IOException{
			
			try{
				logger.info("writeCompleteReceiptMessage called.. from RDT");
				return new ResponseEntity<Boolean>(receivingService.closeReceipt(documentReference, userId, false), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : "   +  "documentReference : " + documentReference);
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/writeCompleteCaseMessage", method = RequestMethod.POST)
	public ResponseEntity<Boolean> writeCompleteCaseMessage(String caseReference, String documentReference,String userId) throws IOException{
			
			try{
				logger.info("writeCompleteReceiptMessage called..");
				return new ResponseEntity<Boolean>(messageService.writeCompleteCaseMessage(caseReference, documentReference,userId), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : "   +  "documentReference : " + documentReference + "  caseReference : "+caseReference);
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	
	@RequestMapping(value = "/writeRTSReturnMessage", method = RequestMethod.POST)
	public ResponseEntity<Boolean> writeRTSReturnMessage(@RequestBody RTSReturnMessageDTO request) throws IOException{
			
			try{
				logger.info("writeRTSReturnMessage called..");
				logger.info("Values : "   +  "order ref : " + request.getOrderReference() + "Type is "+request.getType());
				return new ResponseEntity<Boolean>(messageService.writeRTSReturnMessage(request),HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : "   +  "order ref : " + request.getOrderReference() + "Type is "+request.getType());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}

	@RequestMapping(value = "/writeSTKMVOffsiteReturnMessage", method = RequestMethod.POST)
	public ResponseEntity<Boolean> writeSTKMVOffsiteReturnMessage(@RequestBody OffsiteReturnMessageDTO request) throws IOException{
			
			try{
				logger.info("writeSTKMVOffsiteReturnMessage called..");
				logger.info(
						"STKMV request for part NUmber  reference  ->" + request.getPartNumber() + "Qty -> " + request.getQuantity() + "   from location -> "+request.getFromLocation());
				return new ResponseEntity<Boolean>(messageService.writeSTKMVOffsiteReturnMessage(request),HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : "   +  "Part num : " + request.getPartNumber());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	

	@RequestMapping(value = "/writeRelocReturnMessage", method = RequestMethod.GET)
	public ResponseEntity<Boolean> writeRelocReturnMessage(String documentReference,String userId) throws IOException{
			
			try{
				logger.info("writeRelocReturnMessage called..");
				return new ResponseEntity<Boolean>(messageService.writeRelocReturnMessage( documentReference,userId), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : "   +  "documentReference : " + documentReference );
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	


}




















