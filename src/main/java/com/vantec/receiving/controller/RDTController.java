package com.vantec.receiving.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vantec.receiving.exception.VantecReceivingException;
import com.vantec.receiving.model.DefaultSettingDTO;
import com.vantec.receiving.model.PartDTO;
import com.vantec.receiving.model.TransactionDetailRequest;
import com.vantec.receiving.model.ValidateResponse;
import com.vantec.receiving.service.RDTService;

@RestController
@RequestMapping("/")
public class RDTController {

	private static Logger logger = LoggerFactory.getLogger(RDTController.class);

	@Autowired
	RDTService rdtService;

	@RequestMapping(value = "/validateRDTField", method = RequestMethod.GET)
	public ResponseEntity<ValidateResponse> validateRDTField(@RequestParam("fieldName") String fieldName,
			@RequestParam("fieldValue") String fieldValue, @RequestParam("transactionType") String transactionType)
			throws VantecReceivingException {

		

		ValidateResponse validateResponse = null;
		validateResponse = rdtService.validateRDTField(fieldName, fieldValue, transactionType);
	    return new ResponseEntity<ValidateResponse>(validateResponse, HttpStatus.OK);
		

	}
	
	@RequestMapping(value = "/validatePartSerial", method = RequestMethod.GET)
	public ResponseEntity<ValidateResponse> validatePartSerial(@RequestParam("partNumber") String partNumber,
															   @RequestParam("serialNumber") String serialNumber)
			throws VantecReceivingException {

		
		

		ValidateResponse validateResponse = null;
		validateResponse = rdtService.validatePartSerial(partNumber, serialNumber);
		return new ResponseEntity<>(validateResponse, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/defaultSettings", method = RequestMethod.GET)
	public ResponseEntity<List<DefaultSettingDTO>> defaultSettings(@RequestParam("company") String companyName)
			throws VantecReceivingException {
		
		List<DefaultSettingDTO> settings = null;
		settings = rdtService.getDefaultSettings(companyName);
		return new ResponseEntity<>(settings, HttpStatus.OK);
	}
	

	@RequestMapping(value = "/createPlt", method = RequestMethod.GET)
	public ResponseEntity<String> createPlt(@RequestParam("palletCode") String palletCode,@RequestParam("virtualFlag") Boolean virtualFlag,@RequestParam("transactionTypeId") String transactionTypeId) throws VantecReceivingException {
		
		
		try{
			logger.info("createPlt called..");
			String pltNumber = null;
					if(virtualFlag == true && palletCode == ""){
				
						pltNumber = rdtService.createVirtualPlt(transactionTypeId);
					}else{
				
						pltNumber = rdtService.createPlt(palletCode);
					}
					return new ResponseEntity<String>(pltNumber, HttpStatus.OK);
	    }catch(Exception ex){
	    	logger.error("Values : "   +  "palletCode : " + palletCode);
	    	logger.error(ex.getMessage(), ex);
	    	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
	    }

	}
	
	@RequestMapping(value = "/findPartDetails", method = RequestMethod.GET)
	public ResponseEntity<PartDTO> findPartDetails(@RequestParam("partNumber") String partNumber) throws VantecReceivingException {
		
		
		try{
			logger.info("findPartDetails called..");
			PartDTO partDTO = new PartDTO();
			partDTO = rdtService.findPartDetails(partNumber);
		    return new ResponseEntity<PartDTO>(partDTO, HttpStatus.OK);
	    }catch(Exception ex){
	    	logger.error("Values : "   +  "partNumber : " + partNumber);
	    	logger.error(ex.getMessage(), ex);
	    	return new ResponseEntity<PartDTO>(HttpStatus.INTERNAL_SERVER_ERROR);
	    }

	}
	
	@RequestMapping(value = "/processTransaction", method = RequestMethod.POST)
	public ResponseEntity<String> processTransaction(@RequestBody TransactionDetailRequest transactionDetailCommand) throws VantecReceivingException {
		    
		    
	    	try{
				logger.info("processTransaction called..");
				rdtService.processTransaction(transactionDetailCommand);
		    	return new ResponseEntity<String>("processTransaction called", HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("TransactionDetailRequest Values : "   +  "partNumber : " + transactionDetailCommand.getPartNumber() +
		    			                                               "serialReference" + transactionDetailCommand.getSerialReference() +
		    			                                                "hauliReference : " + transactionDetailCommand.getHaulierReference() +
		    			                                                "quantity" + transactionDetailCommand.getQuantity());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	 	}
	
	@RequestMapping(value = "/completeReceipts", method = RequestMethod.GET)
	public ResponseEntity<Boolean> completeReceipts(@RequestParam("haulierReference") String haulierReference)
			throws VantecReceivingException {
		
		try{
			logger.info("completeReceipts called..");
			Boolean isCompleted = false;
			isCompleted = rdtService.completeReceipts(haulierReference);
			return new ResponseEntity<Boolean>(isCompleted, HttpStatus.OK);
	    }catch(Exception ex){
	    	logger.error("Values : "   +  "haulierReference : " + haulierReference);
	    	logger.error(ex.getMessage(), ex);
	    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	}
	
	
	
	

}




















