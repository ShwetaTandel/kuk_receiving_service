package com.vantec.receiving.business;

import java.io.IOException;
import java.util.List;

import com.vantec.receiving.entity.InventoryMaster;
import com.vantec.receiving.entity.ReceiptBody;
import com.vantec.receiving.entity.ReceiptDetail;
import com.vantec.receiving.entity.ReceiptHeader;
import com.vantec.receiving.entity.TTRanOrder;
import com.vantec.receiving.exception.VantecReceivingException;
import com.vantec.receiving.model.DefaultSettingDTO;
import com.vantec.receiving.model.PartDTO;
import com.vantec.receiving.model.TransactionDetailRequest;
import com.vantec.receiving.model.ValidateResponse;

public interface RDTInfo {
	
	ValidateResponse validateHaulierReference(String trilerRef) throws VantecReceivingException;
	
	ValidateResponse validateSerial(String serial);
	
	ValidateResponse validatePart(String partNumber);
	
	ValidateResponse validateFromLocation(String location,String transactionType);
	
	ValidateResponse validateToLocation(String location,String transactionType);
	
	ValidateResponse validateRanOrder(String ranOrder);
	
	ValidateResponse validatePlt(String plt);
	
	Boolean updateDocument(TransactionDetailRequest txn);
	
	Boolean updateInventory(TransactionDetailRequest txn);
	
	Boolean recordTransaction(TransactionDetailRequest txn);
	
	ValidateResponse validatePartSerial(String partNumber ,String serialReference);
	
	List<DefaultSettingDTO> getDefaultSettings(String company);

	String createPlt(String palletCode);

	PartDTO findPartDetails(String partNumber);

	Boolean completeReceipts(String haulierReference);

	ValidateResponse validateVendorCode(String vendorCode);

	String createVirtualPlt(String transactionTypeCode);

	Boolean closeReceipt(String documentReference,String userId);

	List<ReceiptBody> findAllPartNumbers(String documentReference);

	TTRanOrder findOldestRanOrder(String partNumber);

	List<ReceiptDetail> findReceiptDetails(String documentReference);

	InventoryMaster findInventory(String serialReference, String partNumber);

	void updateInventoryAndDetail(InventoryMaster inventory,
			ReceiptDetail receiptDetail,String userId);

	List<ReceiptHeader> findReceiptHeaders(String haulierReference);

	Boolean sendReceiptMessage(String documentReference, String userId) throws IOException;
}
