package com.vantec.receiving.business;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.vantec.receiving.command.CustomValueGeneratorCommand;
import com.vantec.receiving.command.DocumentReferenceGenerator;
import com.vantec.receiving.entity.DocumentStatus;
import com.vantec.receiving.entity.InventoryMaster;
import com.vantec.receiving.entity.InventoryStatus;
import com.vantec.receiving.entity.Location;
import com.vantec.receiving.entity.OrderBody;
import com.vantec.receiving.entity.OrderHeader;
import com.vantec.receiving.entity.Part;
import com.vantec.receiving.entity.PickBody;
import com.vantec.receiving.entity.ReceiptBody;
import com.vantec.receiving.entity.ReceiptDetail;
import com.vantec.receiving.entity.ReceiptHeader;
import com.vantec.receiving.entity.RelocBody;
import com.vantec.receiving.entity.RelocHeader;
import com.vantec.receiving.entity.RtsBody;
import com.vantec.receiving.entity.RtsDetail;
import com.vantec.receiving.entity.RtsHeader;
import com.vantec.receiving.entity.TransactionHistory;
import com.vantec.receiving.entity.Vendor;
import com.vantec.receiving.model.AllocateByPartDTO;
import com.vantec.receiving.model.RTSReturnMessageDTO;
import com.vantec.receiving.model.ReceiptBodyDTO;
import com.vantec.receiving.model.ReceiptHeaderDTO;
import com.vantec.receiving.model.RelocTaskDTO;
import com.vantec.receiving.model.RtsBodyDTO;
import com.vantec.receiving.model.RtsDetailDTO;
import com.vantec.receiving.model.RtsHeaderDTO;
import com.vantec.receiving.repository.DocumentStatusRepository;
import com.vantec.receiving.repository.InventoryMasterRepository;
import com.vantec.receiving.repository.InventoryStatusRepository;
import com.vantec.receiving.repository.LocationRepository;
import com.vantec.receiving.repository.LocationSubTypeRepository;
import com.vantec.receiving.repository.LocationTypeRepository;
import com.vantec.receiving.repository.OrderBodyRepository;
import com.vantec.receiving.repository.OrderHeaderRepository;
import com.vantec.receiving.repository.PartRepository;
import com.vantec.receiving.repository.PickBodyRepository;
import com.vantec.receiving.repository.PickHeaderRepository;
import com.vantec.receiving.repository.ReceiptBodyRepository;
import com.vantec.receiving.repository.ReceiptDetailRepository;
import com.vantec.receiving.repository.ReceiptHeaderRepository;
import com.vantec.receiving.repository.RelocBodyRepository;
import com.vantec.receiving.repository.RelocHeaderRepository;
import com.vantec.receiving.repository.RtsBodyRepository;
import com.vantec.receiving.repository.RtsDetailRepository;
import com.vantec.receiving.repository.RtsHeaderRepository;
import com.vantec.receiving.repository.TransactionHistoryRepository;
import com.vantec.receiving.repository.VendorRepository;
import com.vantec.receiving.service.MessageService;
import com.vantec.receiving.util.ConstantHelper;

@Service("receiptInfo")
public class ReceiptInfoImpl implements ReceiptInfo {
	
	private static Logger logger = LoggerFactory.getLogger(ReceiptInfoImpl.class);

	@Autowired
	private ReceiptDetailRepository receiptDetailRepository;

	@Autowired
	private ReceiptBodyRepository receiptBodyRepository;

	@Autowired
	private ReceiptHeaderRepository receiptHeaderRepository;
	
	@Autowired
	private RelocHeaderRepository relocHeaderRepository;
	
	@Autowired
	private RelocBodyRepository relocBodyRepository;
	@Autowired
	private VendorRepository vendorRepository;

	@Autowired
	private PartRepository partRepository;

	@Autowired
	private DocumentStatusRepository documentStatusRepository;

	@Autowired
	ReceiptHeaderRepository receiptHeaderRepositiory;

	@Autowired
	InventoryMasterRepository inventoryMasterRepository;

	@Autowired
	PickBodyRepository pickBodyRepo;
	
	@Autowired
	OrderHeaderRepository orderHeaderRepository;
	
	@Autowired
	OrderBodyRepository  orderBodyRepository;
	
	@Autowired
	PickHeaderRepository pickHeaderRepository;

	@Autowired
	RtsHeaderRepository rtsHeaderRepo;

	@Autowired
	RtsBodyRepository rtsBodyRepository;

	@Autowired
	RtsDetailRepository rtsDetailRepository;

	@Autowired
	TransactionHistoryRepository transHistRepo;

	@Autowired
	LocationRepository locRepo;

	@Autowired
	InventoryStatusRepository invStatusRepo;

	@Autowired
	MessageService messageService;
	
	@Autowired
	VendorRepository vendorRepo;
	
	@Autowired
	LocationTypeRepository locTypeRepo;
	
	@Autowired
	LocationSubTypeRepository locSubTypeRepo;

	public ReceiptHeader createReceiptHeader(ReceiptHeaderDTO receiptHeaderDTO) {
		ReceiptHeader receiptHeader = new ReceiptHeader();
		DocumentStatus documentStatus = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_OPEN);

		Vendor vendor = vendorRepository.findByVendorReferenceCode(ConstantHelper.KUK_VENDOR_CODE);
		receiptHeader.setCustomerReference(receiptHeaderDTO.getGrnNumber());
		receiptHeader.setHaulierReference(receiptHeaderDTO.getContainerId());
		receiptHeader.setDocumentStatus(documentStatus);
		receiptHeader.setExpectedDeliveryDate(new Date());
		receiptHeader.setVendor(vendor);
		receiptHeader.setGrnType(receiptHeaderDTO.getGrnType());

		receiptHeader.setCreatedBy(ConstantHelper.SYSUSER);
		receiptHeader.setUpdatedBy(ConstantHelper.SYSUSER);
		receiptHeader.setDateCreated(new Date());
		receiptHeader.setDateUpdated(new Date());

		receiptHeaderRepository.save(receiptHeader);

		CustomValueGeneratorCommand<String> customValueGeneratorCommand = new DocumentReferenceGenerator();
		String documentReference = customValueGeneratorCommand.generate(receiptHeader.getId(), ConstantHelper.RECEIPT_REFERENCE_PREFIX);
		receiptHeader.setDocumentReference(documentReference);
		ReceiptHeader savedHeader = receiptHeaderRepository.save(receiptHeader);
		return savedHeader;
	}
	
	public RelocHeader createRelocHeader(RelocTaskDTO relocTaskDTO) {
		RelocHeader relocHeader = new RelocHeader();
		ReceiptHeader receiptHeader = receiptHeaderRepositiory.findByCustomerReference(relocTaskDTO.getGrnReference());
		if(receiptHeader == null){
			logger.info("GRN Not found for this RELOC tasks "+relocTaskDTO.getGrnReference());
			return null;
		}
		relocHeader.setReceiptHeader(receiptHeader);
	
		
		relocHeader.setProcessed(false);
		relocHeader.setOrderReference(relocTaskDTO.getOrderReference());
		relocHeader.setFromLocation(relocTaskDTO.getFromLocation());
		relocHeader.setToLocation(relocTaskDTO.getToLocation());
		relocHeader.setLineNo(relocTaskDTO.getLineNo());
		relocHeader.setQuantity(relocTaskDTO.getQuantity());
		

		relocHeader.setCreatedBy(ConstantHelper.SYSUSER);
		relocHeader.setLastUpdatedBy(ConstantHelper.SYSUSER);
		relocHeader.setDateCreated(new Date());
		relocHeader.setCreatedBy(ConstantHelper.SYSUSER);

		relocHeader = relocHeaderRepository.save(relocHeader);

		CustomValueGeneratorCommand<String> customValueGeneratorCommand = new DocumentReferenceGenerator();
		String documentReference = customValueGeneratorCommand.generate(relocHeader.getId(), ConstantHelper.RELOC_REFERENCE_PREFIX);
		relocHeader.setDocumentReference(documentReference);
		RelocHeader savedTask = relocHeaderRepository.save(relocHeader);
		return savedTask;
	}
	
	public RelocHeader createRelocBody(RelocHeader relocHeader){
		
		ReceiptBody receiptBody = receiptBodyRepository.findByReceiptHeaderAndLineNo(relocHeader.getReceiptHeader(), relocHeader.getLineNo());
		List<ReceiptDetail> details = receiptDetailRepository.findByReceiptBody(receiptBody);
		if (details != null) {
			RelocBody body = new RelocBody();
			double quantityRemaining = relocHeader.getQuantity();
			for(ReceiptDetail detail:details){
				InventoryMaster inv = inventoryMasterRepository.findBySerialReferenceAndPartNumber(detail.getSerialReference(), detail.getPartNumber());
				/* */
				if (inv != null && (inv.getInventoryStatus().getInventoryStatusCode()
						.equalsIgnoreCase(ConstantHelper.INVENTORY_STATUS_OVERBOOKING)
						|| inv.getInventoryStatus().getInventoryStatusCode()
								.equalsIgnoreCase(ConstantHelper.INVENTORY_STATUS_QA))) {
				
						double quantity = (quantityRemaining > detail.getReceivedQty().doubleValue())
								? detail.getReceivedQty().doubleValue() : quantityRemaining;
						body.setQuantity(quantity);
						body.setSerialReference(inv.getSerialReference());
						body.setInventoryMaster(inv);
						body.setProcessed(false);
						body.setRelocHeader(relocHeader);
						relocHeader.getRelocBodies().add(body);
						relocBodyRepository.save(body);
						quantityRemaining -= detail.getReceivedQty().doubleValue();
						if (quantityRemaining <= 0) {
							break;
						}
					
				}
				
			}
		}
		return relocHeaderRepository.save(relocHeader);
		
	}
	
	/***
	 * @param RelocHeader
	 * 
	 * 
	 */
	public List<String> processRelocTasks(RelocHeader relocHeader){
		
		List<String> parts = new ArrayList<String>();
		for (RelocBody body : relocHeader.getRelocBodies()){
			// If inv status code = OVERBOOKING & stock in storage
			boolean overBooking = body.getInventoryMaster()!=null && body.getInventoryMaster().getInventoryStatus().getInventoryStatusCode()
					.equalsIgnoreCase(ConstantHelper.INVENTORY_STATUS_OVERBOOKING) && body.getInventoryMaster().getCurrentLocation().getLocationType().getLocationArea()
							.equalsIgnoreCase(ConstantHelper.LOCATION_TYPE_LOCATION_AREA_STORAGE);
			//if stock is already in storage location and 'to location' in message = STOCK 
			boolean stockStorageCheck = relocHeader.getToLocation().equalsIgnoreCase(ConstantHelper.CUSTOMER_lOCATION_AREA_STOCK) && body.getInventoryMaster()!=null && body.getInventoryMaster().getCurrentLocation().getLocationType().getLocationArea()
					.equalsIgnoreCase(ConstantHelper.LOCATION_TYPE_LOCATION_AREA_STORAGE);
			if (overBooking || stockStorageCheck) {
				body.setProcessed(true);
				body = relocBodyRepository.save(body);
				
				InventoryMaster master = body.getInventoryMaster();
				master.setInventoryStatus(invStatusRepo.findByInventoryStatusCode(ConstantHelper.INVENTORY_STATUS_OK));
				master.setUpdatedBy(ConstantHelper.SYSUSER);
				master.setDateUpdated(new Date());
				master.setHoldQty(master.getHoldQty() - body.getQuantity());
				master.setAvailableQty(master.getAvailableQty() + body.getQuantity());
				inventoryMasterRepository.save(master);
				parts.add(master.getPartNumber());
			}
		}
		Integer cnt = relocBodyRepository.findCountOfUnprocessedBodies(relocHeader);
		if(cnt.intValue() == 0){
			relocHeader.setProcessed(true);
			relocHeader.setLastUpdated(new Date());
			relocHeader.setLastUpdatedBy(ConstantHelper.SYSUSER);
			relocHeaderRepository.save(relocHeader);
		}
		return parts;
		
	}

	/***
	 * createReceiptBody
	 * @param receiptHeader
	 * @param receiptBodyDTO
	 */
	public ReceiptBody createReceiptBody(ReceiptHeader receiptHeader, ReceiptBodyDTO receiptBodyDTO) {
		

		ReceiptBody receiptBody = new ReceiptBody();
		receiptBody.setQuarantinedQty(Double.valueOf(receiptBodyDTO.getQtyToQuarantine()));
		Part part = partRepository.findByPartNumber(receiptBodyDTO.getPartNumber());
		Vendor vendor = vendorRepository.findByVendorReferenceCode(receiptBodyDTO.getSupplierCode());
		if(vendor == null){
			logger.info("Skipping adding body for vendor "+ receiptBodyDTO.getSupplierCode());
			return null;
		}else{
			if (part == null && receiptBody.getQuarantinedQty()>0 ) {
				part = partRepository.findByPartNumber(ConstantHelper.NOT_EXIST_PART_NUMBER);
			}else if(part == null && receiptBody.getQuarantinedQty()==0){
				//Genuine missing part create one
				part = createNewPart(receiptBodyDTO.getPartNumber());
			}
		}
		if (part!=null && part.getIsAS400()) {
			logger.info("Skipping adding for part "+part.getPartNumber() + " " +part.getIsAS400());
			return null;
		
		}
		DocumentStatus documentStatus = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_OPEN);

		receiptBody.setLineNo(receiptBodyDTO.getSequenceNumber());
		receiptBody.setPartNumber(receiptBodyDTO.getPartNumber());
		receiptBody.setPart(part);
		receiptBody.setDocumentStatus(documentStatus);
		receiptBody.setAdvisedQty(Double.valueOf(receiptBodyDTO.getQuantityAdvised()));
		receiptBody.setReceivedQty(Double.valueOf(0));
		receiptBody.setDifference(receiptBody.getReceivedQty() - receiptBody.getAdvisedQty());
		
		if (receiptBody.getQuarantinedQty() > 0) {
			documentStatus = documentStatusRepository
					.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_QUARANTINE);
		}
		receiptBody.setVendorCode(receiptBodyDTO.getSupplierCode());
		
		receiptBody.setCaseReference(receiptBodyDTO.getShippingReferenceCaseReference());
		if (receiptBodyDTO.getQtyAccepted() != null && !("").equalsIgnoreCase(receiptBodyDTO.getQtyAccepted())) {
			receiptBody.setQtyAccepted(Double.valueOf(receiptBodyDTO.getQtyAccepted()));
		}
		receiptBody.setInspect(receiptBodyDTO.getInspectFlag().equalsIgnoreCase("Y") ? true : false);
		receiptBody.setCountRequiredflag(receiptBodyDTO.getRequiredCountFlag().equalsIgnoreCase("Y") ? true : false);
		receiptBody.setNewItems(receiptBodyDTO.getNewItems());

		receiptBody.setUpdatedBy(ConstantHelper.SYSUSER);
		receiptBody.setCreatedBy(ConstantHelper.SYSUSER);
		receiptBody.setDateCreated(new Date());
		receiptBody.setDateUpdated(new Date());

		receiptBody.setDocumentReference(receiptHeader.getDocumentReference());
		receiptBody.setReceiptHeader(receiptHeader);
		receiptBody.setShortage(checkAnyShortagesForPart(receiptBodyDTO.getPartNumber()));
		return receiptBodyRepository.save(receiptBody);

	}
	
	private Part createNewPart (String partNumber){
		Vendor vendor = vendorRepo.findByVendorReferenceCode(ConstantHelper.VENDOR_KUK_REF_CODE);
			
			Part part = new Part();
			part.setPartNumber(partNumber);
			part.setVendor(vendor);
			try {
				part.setEffectiveFrom(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2000-01-01 00:00:00"));
				part.setEffectiveTo(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2035-12-31 00:00:00"));
			} catch (ParseException e) {
			}
			part.setVersion(0L);
			part.setActive(true);
			part.setImaginaryPart(false);
			part.setRequiresCount(false);
			part.setRequiresDecant(false);
			part.setRequiresInspection(false);
			part.setSafetyStock(0);
			part.setFullBoxPick(false);
			part.setModular(false);
			part.setFixedLocationTypeId(locTypeRepo.findByLocationTypeCode(ConstantHelper.LOCATION_NEW).getId());
			part.setFixedSubTypeId(locSubTypeRepo.findByLocationSubTypeCode(ConstantHelper.LOCATION_NEW).getId());
			part.setIncludeInventory(0);
			part.setLoosePartQty(0);
			part.setConversionFactor(1);
			part.setPartDescription(partNumber);
			part.setCreatedBy(ConstantHelper.GRN_FILE_TYPE);
			part.setLastUpdatedBy(ConstantHelper.GRN_FILE_TYPE);
			part.setDateCreated(new Date());
			part.setLastUpdated(new Date());
			return partRepository.save(part);
	}
	
	public void createDetailandInventoryForOffsiteStorage(ReceiptBody receiptBody, Vendor vendor){

		
			//Since this is auto process received Qty = Advised Qty
			DocumentStatus completeStatus = documentStatusRepository.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_COMPLETED);
			if (receiptBody.getPart().getIsBigPart()) {
				// Create receipt detail line equal to the qty
				double avlQty = receiptBody.getAdvisedQty().doubleValue();
				double holdQty = receiptBody.getQuarantinedQty().doubleValue();
				if(receiptBody.getQuarantinedQty().doubleValue() > 0){
					avlQty = receiptBody.getAdvisedQty().doubleValue() - receiptBody.getQuarantinedQty().doubleValue();
				}
				for (int i = 0; i < (int)avlQty; i++) {
					ReceiptDetail detail = createReceiptDetail(receiptBody, ConstantHelper.GRN_FILE_TYPE,
							1.0, 1.0, 
							ConstantHelper.OFFSITE_STORAGE_SERIAL_PREFIX, completeStatus);
					
					addTransactionHistory(ConstantHelper.GRN_FILE_TYPE, receiptBody.getDocumentReference(),
						detail.getPartNumber(), 1.0, ConstantHelper.RECEIPT_TRANSACTION,
						vendor.getOffsiteStorageLocation().getLocationCode(), detail.getSerialReference());
				
					addInventoryMasterRecord(ConstantHelper.GRN_FILE_TYPE, ConstantHelper.INVENTORY_STATUS_OK, detail.getPart(),detail.getPartNumber(),
						vendor.getOffsiteStorageLocation(), detail.getSerialReference(), 1.0,
						Double.valueOf(0), Double.valueOf(0));
				}
				for (int i = 0; i < (int)holdQty; i++) {
					ReceiptDetail detail = createReceiptDetail(receiptBody, ConstantHelper.GRN_FILE_TYPE,
							1.0, 1.0, 
							ConstantHelper.OFFSITE_STORAGE_SERIAL_PREFIX, completeStatus);
					
					addTransactionHistory(ConstantHelper.GRN_FILE_TYPE, receiptBody.getDocumentReference(),
						detail.getPartNumber(), 1.0, ConstantHelper.RECEIPT_TRANSACTION,
						vendor.getOffsiteStorageLocation().getLocationCode(), detail.getSerialReference());
				
					addInventoryMasterRecord(ConstantHelper.GRN_FILE_TYPE, ConstantHelper.INVENTORY_STATUS_QA, detail.getPart(),detail.getPartNumber(),
						vendor.getOffsiteStorageLocation(), detail.getSerialReference(),Double.valueOf(0),
						Double.valueOf(0), 1.0);
				}
			} else {
				//IF we have quarantine qty and its less than advised we need to mark inv status QA and add qtine qty in hold qty
				String invStatus = ConstantHelper.INVENTORY_STATUS_OK;
				double avlQty = receiptBody.getAdvisedQty().doubleValue();
				double holdQty = receiptBody.getQuarantinedQty().doubleValue();
				if(receiptBody.getQuarantinedQty().doubleValue() > 0){
					avlQty = receiptBody.getAdvisedQty().doubleValue() - receiptBody.getQuarantinedQty().doubleValue();
					invStatus = ConstantHelper.INVENTORY_STATUS_QA;
				}
				ReceiptDetail detail = createReceiptDetail(receiptBody, ConstantHelper.GRN_FILE_TYPE,
						receiptBody.getAdvisedQty(), receiptBody.getAdvisedQty(), 
						ConstantHelper.OFFSITE_STORAGE_SERIAL_PREFIX, completeStatus);
				
				addTransactionHistory(ConstantHelper.GRN_FILE_TYPE, receiptBody.getDocumentReference(),
						detail.getPartNumber(), detail.getReceivedQty(), ConstantHelper.RECEIPT_TRANSACTION,
						vendor.getOffsiteStorageLocation().getLocationCode(), detail.getSerialReference());
				
			    addInventoryMasterRecord(ConstantHelper.GRN_FILE_TYPE, invStatus, detail.getPart(),detail.getPartNumber(),
					vendor.getOffsiteStorageLocation(), detail.getSerialReference(), avlQty,
					Double.valueOf(0), holdQty);
			    
			}
			
			receiptBody.setDocumentStatus(completeStatus);
			receiptBody.setReceivedQty(receiptBody.getAdvisedQty());
			receiptBody.setDifference(receiptBody.getReceivedQty() - receiptBody.getAdvisedQty());
			receiptBodyRepository.save(receiptBody);
			

	}
	
	
	private void addTransactionHistory(String user, String docRef, String partNum, Double txnQty, String txnRefCode, String locCode, String serialRef){
			TransactionHistory hist = new TransactionHistory();
			hist.setCreatedBy(user);
			hist.setDateCreated(new Date());
			hist.setDateUpdated(new Date());
			hist.setUpdatedBy(user);
			hist.setAssociatedDocumentReference(docRef);
			hist.setPartNumber(partNum);
			hist.setTxnQty(txnQty);
			hist.setTransactionReferenceCode(txnRefCode);
			hist.setToLocationCode(locCode);
			hist.setSerialReference(serialRef);
			hist.setShortCode(txnRefCode);
			transHistRepo.save(hist);
	}
	
	/**
	 * 
	 * Part and partnumber both are passed because it could be not exist part
	 * @param user
	 * @param invStatus
	 * @param part
	 * @param partNumber
	 * @param loc
	 * @param serialRef
	 * @param avlQty
	 * @param allocQty
	 * @param holdQty
	 */
	private void addInventoryMasterRecord(String user, String invStatus, Part part,String partNumber, Location loc, String serialRef, Double avlQty, Double allocQty, Double holdQty){

		InventoryMaster inv = new InventoryMaster();

		inv.setSerialReference(serialRef);
		inv.setLocationCode(loc.getLocationCode());
		inv.setCurrentLocation(loc);
		inv.setPart(part);
		inv.setPartNumber(partNumber);
		inv.setAvailableQty(avlQty);
		inv.setAllocatedQty(allocQty);
		inv.setHoldQty(holdQty);
		inv.setInventoryQty(avlQty + allocQty + holdQty);
		inv.setRecordedMissing(false);
		inv.setRequiresDecant(false);
		inv.setRequiresInspection(false);
		inv.setConversionFactor(1);
		inv.setVersion(0L);
		inv.setInventoryStatus(invStatusRepo.findByInventoryStatusCode(invStatus));
		inv.setCreatedBy(user);
		inv.setUpdatedBy(user);
		inv.setDateCreated(new Date());
		inv.setDateUpdated(new Date());
		inventoryMasterRepository.save(inv);
	}


	public RtsHeader createRtsHeader(RtsHeaderDTO rtsHeaderDTO) {
		RtsHeader rtsHeader = new RtsHeader();
		DocumentStatus documentStatus = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_OPEN);

		rtsHeader.setOrigin(rtsHeaderDTO.getOrigin());
		rtsHeader.setOrderReference(rtsHeaderDTO.getOrderReference());
		rtsHeader.setPosition(rtsHeaderDTO.getPosition());
		if (rtsHeaderDTO.isFABRTS()) {
			rtsHeader.setOrderType(ConstantHelper.FAB_RTS);
		} else if (rtsHeaderDTO.getOrderType() != null) {
			rtsHeader.setOrderType(rtsHeaderDTO.getOrderType());
		}  else {
			rtsHeader.setOrderType(ConstantHelper.REWORK_RTS);
		}  
		rtsHeader.setIsProcessed(false);
		rtsHeader.setMachineModel(rtsHeaderDTO.getMachineModel());
		rtsHeader.setDocumentStatus(documentStatus);
		rtsHeader.setReasonCode(rtsHeaderDTO.getReasonCode());
		rtsHeader.setDateTime(rtsHeaderDTO.getDateTime());
		rtsHeader.setCreatedBy(ConstantHelper.SYSUSER);
		rtsHeader.setLastUpdatedBy(ConstantHelper.SYSUSER);
		rtsHeader.setCreatedAt(new Date());
		rtsHeader.setLastUpdated(new Date());
		RtsHeader savedHeader = rtsHeaderRepo.save(rtsHeader);
		if(savedHeader.getOrderType().equalsIgnoreCase(ConstantHelper.UNPLANNED_RTS)){
			//Update the Order Reference
			Long maxId = savedHeader.getId();
			String formattedId = ConstantHelper.UNPLANNED_RTS_SERIAL_PREFIX; 
			if(maxId < 999){
				formattedId += String.format("%03d", maxId );
			}else{
				formattedId += (maxId) ;
			}
			savedHeader.setOrderReference(formattedId);
			savedHeader = rtsHeaderRepo.save(savedHeader);
		}
		return savedHeader;
	}

	public RtsBody createRtsBody(RtsHeader rtsHeader, RtsBodyDTO rtsBodyDTO) {

		RtsBody rtsBody = new RtsBody();
		Part part = partRepository.findByPartNumber(rtsBodyDTO.getPartNumber());
		if (part == null || (part != null && part.getIsAS400())) {
			logger.info("Skipping the RTS body as part not configuured ="+rtsBodyDTO.getPartNumber() );
			return null;
		}
		DocumentStatus documentStatus = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_OPEN);

		rtsBody.setPartNumber(rtsBodyDTO.getPartNumber());
		rtsBody.setPart(part);
		rtsBody.setDocumentStatus(documentStatus);
		rtsBody.setQtyReceived(Double.valueOf(rtsBodyDTO.getQtyReceived()));
		rtsBody.setBaanLocation(rtsBodyDTO.getBaanLocation());
		rtsBody.setBaanSerial(rtsBodyDTO.getBaanSerial());
		rtsBody.setLastUpdatedBy(ConstantHelper.SYSUSER);
		rtsBody.setCreatedBy(ConstantHelper.SYSUSER);
		rtsBody.setCreatedAt(new Date());
		rtsBody.setLastUpdated(new Date());
		rtsBody.setRtsHeader(rtsHeader);
		return rtsBodyRepository.save(rtsBody);
	}
	

	public void createInventoryAndTransactionHistoryForAutoRTSTasks(RtsHeader rtsHeader, String location) {

		Location loc = locRepo.findByLocationCode(location);
		String partNumber = rtsHeader.getRtsBodies().get(0).getPartNumber();
		Part part = partRepository.findByPartNumber(partNumber);
		// Add Inventory and trans hist
		String transactionCode = rtsHeader.getOrderType().equalsIgnoreCase(ConstantHelper.FAB_RTS) == true
				? ConstantHelper.FAB_RTS_TRANSACTION_REFERENCE
				: rtsHeader.getOrderType().equalsIgnoreCase(ConstantHelper.UNPLANNED_RTS) == true
				? ConstantHelper.UNPLANNED_RTS_TRANSACTION_REFERNCE : ConstantHelper.RTS_TRANSACTION_SHORT_CODE;

		for (RtsDetail det : rtsHeader.getRtsBodies().get(0).getRtsDetails()) {
			

			addTransactionHistory(rtsHeader.getCreatedBy(), rtsHeader.getOrderReference(), partNumber, det.getReceivedQty(),transactionCode, location, det.getSerialReference());
			
			addInventoryMasterRecord(rtsHeader.getCreatedBy(), ConstantHelper.INVENTORY_STATUS_OK, part,partNumber, loc,
					det.getSerialReference(), det.getReceivedQty(), Double.valueOf(0), Double.valueOf(0));
			
		}
	}

	/**
	 * 
	 * 
	 * Call the pick service through rest api
	 * @param pickRefs
	 */
	public void callAllocateForNewStock(List<String> parts) {
		// String uri =
		// "http://localhost:8095/pick/allocateWithTO?documentReference="+pickRef+"userId="+
		// ConstantHelper.SYSUSER;
		String uri = "http://localhost:8095/pick/allocateForParts";
		AllocateByPartDTO request = new AllocateByPartDTO();
		request.setParts(parts);
		request.setUserId(ConstantHelper.SYSUSER);
		RestTemplate restTemplate = new RestTemplate();
		// restTemplate.getForObject(uri, Boolean.class);
		// restTemplate.postForObject(uri, request, Boolean.class);
		try {
			ResponseEntity<Boolean> response = restTemplate.postForEntity(uri, request, Boolean.class);
		} catch (Exception e) {
			logger.error("Error after post call for allocation->" + e.getMessage());
		}
	}
	
	
	
	public RtsDetail createRtsDetail(RtsBody rtsBody, RtsDetailDTO rtsDetailDTO) {

		RtsDetail detail = new RtsDetail();
		detail.setPartNumber(rtsDetailDTO.getPartNumber());
		detail.setLocationCode(rtsDetailDTO.getLocationCode());
		detail.setSerialReference(rtsDetailDTO.getSerialReference());
		detail.setReceivedQty(rtsDetailDTO.getReceivedQty());
		detail.setQtyTransacted(rtsDetailDTO.getTransactedQty());
		detail.setLastUpdatedBy(ConstantHelper.SYSUSER);
		detail.setCreatedBy(ConstantHelper.SYSUSER);
		detail.setCreatedAt(new Date());
		detail.setLastUpdated(new Date());
		detail.setRtsBody(rtsBody);
		RtsDetail savedDetail = rtsDetailRepository.save(detail);
		savedDetail.setSerialReference(savedDetail.getSerialReference()+ savedDetail.getId());
		return rtsDetailRepository.save(savedDetail);

	}

	public void processRTSBodiesAndDetails(RtsHeader rtsHeader, String user, String partNumber, String rtsType,
			boolean sendMsg, boolean setQtyZero) {
		DocumentStatus completedStatus = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_COMPLETED);
		DocumentStatus transmittedStatus = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_TRANSMITTED);
		boolean msgRes = false;
		List<RtsBody> rtsBodies = rtsBodyRepository.findByRtsHeaderAndPartNumber(rtsHeader, partNumber);
		for (RtsBody body : rtsBodies) {
			body.setQtyTransacted(setQtyZero == true ? Double.valueOf(0) : body.getQtyReceived());
			body.setLastUpdated(new Date());
			body.setLastUpdatedBy(user);
			body.setDocumentStatus(completedStatus);
			rtsDetailRepository.updateStatusAndTransactedQtyByBody(body, user, completedStatus, Double.valueOf(0));
			rtsBodyRepository.save(body);
			logger.info("Marking body/detail processed " + body.getId());
		}
		if (sendMsg) {
			// Send message
			RTSReturnMessageDTO dto = new RTSReturnMessageDTO();
			dto.setOrderReference(rtsHeader.getOrderReference());
			dto.setPartNumber(partNumber);
			dto.setType(rtsType);
			dto.setUser(user);
			try {

				msgRes = messageService.writeRTSReturnMessage(dto);
				logger.info("Message written for " + rtsHeader.getOrderReference());

			} catch (IOException e) {
				logger.error("Error in writing RTS message " + e);
			}
		}

		if (msgRes) {
			int cnt = rtsBodyRepository.findCountOfUnProcessedBodies(rtsHeader, transmittedStatus);
			if (cnt == 0) {
				// Mark the Body processed
				rtsHeader.setIsProcessed(true);
				rtsHeader.setDocumentStatus(transmittedStatus);
				/// send message
				rtsHeaderRepo.save(rtsHeader);
				logger.info("Marking header processed " + rtsHeader.getOrderReference());
			}
		}

	}

	/*****
	 * Create auto serials & details
	 */
	public void createReceiptDetailsForOdetteLabels(ReceiptHeader header, String user) {

		double recQty = 0.0;
		DocumentStatus status = documentStatusRepository.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_OPEN);
		for (ReceiptBody body : header.getReceiptBodies()) {
			if (body.getPart().getIsBigPart()) {
				// Create receipt detail line equal to the qty
				for (int i = 0; i < body.getAdvisedQty().intValue(); i++) {
					createReceiptDetail(body, user, (double) 1,recQty, ConstantHelper.RECEIPT_DETAIL__SERIAL_PREFIX, status );
				}
			} else {
				createReceiptDetail(body, user, body.getAdvisedQty(),recQty, ConstantHelper.RECEIPT_DETAIL__SERIAL_PREFIX,status);
			}
		}

	}

	private ReceiptDetail createReceiptDetail( ReceiptBody receiptBody, String user, Double advQty, Double recQty, String formattedSerial, DocumentStatus status) {

		
		
		ReceiptDetail receiptDetail = new ReceiptDetail();
		receiptDetail.setAdvisedQty(setConversionFactor(receiptBody.getPart(), advQty));
		receiptDetail.setQuarantinedQty(0.0);
		receiptDetail.setReceivedQty(recQty);
		receiptDetail.setDifference(receiptDetail.getReceivedQty() - receiptDetail.getAdvisedQty());
		receiptDetail.setDocumentReference(receiptBody.getDocumentReference());
		receiptDetail.setPartNumber(receiptBody.getPartNumber());
		receiptDetail.setPart(receiptBody.getPart());
		receiptDetail.setDocumentStatus(status);
		receiptDetail.setUpdatedBy(user);
		receiptDetail.setCreatedBy(user);
		receiptDetail.setDateCreated(new Date());
		receiptDetail.setDateUpdated(new Date());

		receiptDetail.setReceiptBody(receiptBody);
		ReceiptDetail savedReceiptDetail = receiptDetailRepository.save(receiptDetail);
		//Update the Serial
		Long serialCnt = savedReceiptDetail.getId();
		if(serialCnt < 9999){
			formattedSerial += String.format("%04d", serialCnt +1 );
		}else{
			formattedSerial += (serialCnt +1) ;
		}
		savedReceiptDetail.setSerialReference(formattedSerial);
		
		return receiptDetailRepository.save(savedReceiptDetail);

	}


	/***
	 * 
	 * 
	 */
	public void stockOutInventoryForDeletedReceipt(ReceiptDetail receiptDetail, String grnNumber) {
		InventoryMaster invToDelete = inventoryMasterRepository
				.findBySerialReferenceAndPartNumber(receiptDetail.getSerialReference(), receiptDetail.getPartNumber());

		addTransactionHistory(ConstantHelper.RVS_FILE_JOB_USER, grnNumber, receiptDetail.getPartNumber(),
				invToDelete.getAvailableQty(), ConstantHelper.SA_OUT_SHORT_CODE,
				invToDelete.getCurrentLocation().getLocationCode(), receiptDetail.getSerialReference());
		inventoryMasterRepository.delete(invToDelete);
	}

	/***
	 * Check in PickBody Table if the part received has any shortages
	 * 
	 * @return
	 */
	public Boolean checkAnyShortagesForPart(String partNumber) {
		Integer count = pickBodyRepo.findByPartNumberHavingShortageQty(partNumber);
		return count > 0;
	}
	
	//This method is no longer use kept for references
	public Boolean checkPickExistsForCustomerReferenceAndPart(String custRef, String partNumber){
		boolean isExpedite = false;
		List<OrderHeader> orderHeaders = orderHeaderRepository.findByCustomerReference(custRef);
		if(orderHeaders!=null && !orderHeaders.isEmpty()){
			for(OrderHeader orderHeader: orderHeaders){
				PickBody pickBody = pickBodyRepo.findByPickHeaderAndPartNumberAndProcessed(orderHeader.getPickHeader(), partNumber,false);
				if(pickBody!=null){
						isExpedite = true;
				}
			}
		}
		return isExpedite;
		
	}

	public Boolean checkOrderExistsForCustomerReferenceAndPart(String custRef, String partNumber){
		boolean isExpedite = false;
		List<OrderBody> orders = orderBodyRepository.findByPartNumberAndCustomerReference(partNumber, custRef);
		if(orders!=null && !orders.isEmpty()){
				isExpedite = true;
		}
		return isExpedite;
		
	}
	
	private Double setConversionFactor(Part part, Double qty) {
		return part.getConversionFactor() * qty;
	}

	public static void main(String args[]) throws ParseException {

		/*String sDate6 = "20190813 164604";
		SimpleDateFormat formatter6 = new SimpleDateFormat("yyyyMMdd HHmmss");
		Date date6 = formatter6.parse(sDate6);
		System.out.println(date6);*/
		
		Integer  id = 5187 ;
		String t = "%0"+(9-(String.valueOf(id).length()))+"d";
		System.out.println(t);
		System.out.println(String.format("%09d", 3187 ));

		
		int number = 9; String str = String.format("%04d", 9); // 0009 
		System.out.printf("original number %d, numeric string with padding : %s", 9, str);

			}

}
