package com.vantec.receiving.business;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.receiving.dao.RDTDAO;
import com.vantec.receiving.dao.ReceivingDAO;
import com.vantec.receiving.entity.Company;
import com.vantec.receiving.entity.DefaultSetting;
import com.vantec.receiving.entity.DocumentStatus;
import com.vantec.receiving.entity.InventoryMaster;
import com.vantec.receiving.entity.InventoryStatus;
import com.vantec.receiving.entity.Location;
import com.vantec.receiving.entity.Pallet;
import com.vantec.receiving.entity.Part;
import com.vantec.receiving.entity.ReceiptBody;
import com.vantec.receiving.entity.ReceiptDetail;
import com.vantec.receiving.entity.ReceiptHeader;
import com.vantec.receiving.entity.TTRanOrder;
import com.vantec.receiving.entity.TransactionHistory;
import com.vantec.receiving.entity.TransactionType;
import com.vantec.receiving.entity.Vendor;
import com.vantec.receiving.exception.VantecReceivingException;
import com.vantec.receiving.model.DefaultSettingDTO;
import com.vantec.receiving.model.PartDTO;
import com.vantec.receiving.model.TransactionDetailRequest;
import com.vantec.receiving.model.ValidateResponse;
import com.vantec.receiving.repository.DocumentStatusRepository;
import com.vantec.receiving.repository.InventoryMasterRepository;
import com.vantec.receiving.repository.InventoryStatusRepository;
import com.vantec.receiving.repository.LocationRepository;
import com.vantec.receiving.repository.PalletRepository;
import com.vantec.receiving.repository.PartRepository;
import com.vantec.receiving.repository.ProductTypeRepository;
import com.vantec.receiving.repository.ReceiptBodyRepository;
import com.vantec.receiving.repository.ReceiptDetailRepository;
import com.vantec.receiving.repository.ReceiptHeaderRepository;
import com.vantec.receiving.repository.TTRanOrderRepository;
import com.vantec.receiving.repository.TransactionHistoryRepository;
import com.vantec.receiving.repository.TransactionTypeRepository;
import com.vantec.receiving.repository.UserRepository;
import com.vantec.receiving.repository.VendorRepository;
import com.vantec.receiving.service.MessageService;
import com.vantec.receiving.util.ConstantHelper;
import com.vantec.receiving.util.RDTInfoHelper;

@Service("validatingInfo")
public class RDTInfoImpl implements RDTInfo {

	@Autowired
	RDTDAO rdtDAO;

	@Autowired
	ReceivingDAO receivingDAO;

	@Autowired
	InventoryMasterRepository inventoryMasterRepository;

	@Autowired
	PartRepository partRepository;

	@Autowired
	PalletRepository palletRepository;

	@Autowired
	ProductTypeRepository productTypeRepository;

	@Autowired
	LocationRepository locationRepository;

	@Autowired
	TransactionHistoryRepository transactionHistoryRepository;

	@Autowired
	TTRanOrderRepository ttRanOrderRepository;

	@Autowired
	TransactionTypeRepository transactiontypeRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	VendorRepository vendorRepository;

	@Autowired
	ReceiptHeaderRepository receiptHeaderRepository;

	@Autowired
	ReceiptBodyRepository receiptBodyRepository;

	@Autowired
	ReceiptDetailRepository receiptDetailRepository;

	@Autowired
	InventoryStatusRepository inventoryStatusRepository;

	@Autowired
	DocumentStatusRepository documentStatusRepository;

	@Autowired
	MessageService messageService;

	@Override
	public ValidateResponse validateHaulierReference(String haulierReference) throws VantecReceivingException {

		ValidateResponse validateResponse = getValidateResponse();
		validateResponse.setStatus("FAIL");

		List<ReceiptHeader> receipts = receiptHeaderRepository.findByHaulierReference(haulierReference);

		for (ReceiptHeader receipt : receipts) {

			if (receipt.getDocumentStatus().getDocumentStatusCode().equals("OPEN")
					|| receipt.getDocumentStatus().getDocumentStatusCode().equals("COMPLETE")) {
				validateResponse.setStatus("OK");
			}

		}

		return validateResponse;
	}

	@Override
	public ValidateResponse validateFromLocation(String hashCode, String transactionTypeId) {

		ValidateResponse validateResponse = getValidateResponse();

		Location location = rdtDAO.findLocationByHashCode(hashCode);

		if (location == null) {
			validateResponse.setStatus("FAIL");
		} else {
			TransactionType transactionType = rdtDAO.findTranscationByCode(transactionTypeId);

			Boolean isValid = rdtDAO.validateFromLocationByTransaction(location, transactionType);
			if (!isValid) {
				validateResponse.setStatus("FAIL");
			}

		}
		return validateResponse;

	}

	@Override
	public ValidateResponse validateToLocation(String hashCode, String transactionTypeId) {

		ValidateResponse validateResponse = getValidateResponse();

		Location location = rdtDAO.findLocationByHashCode(hashCode);
		if (location == null) {
			validateResponse.setStatus("FAIL");
		} else {
			TransactionType transactionType = rdtDAO.findTranscationByCode(transactionTypeId);
			Boolean isValid = rdtDAO.validateToLocationByTransaction(location, transactionType);
			if (!isValid) {
				validateResponse.setStatus("FAIL");
			}

		}
		return validateResponse;
	}

	@Override
	public ValidateResponse validatePlt(String pltNumber) {
		ValidateResponse validateResponse = getValidateResponse();
		return validateResponse;
	}

	@Override
	public ValidateResponse validatePart(String partNumber) {
		ValidateResponse validateResponse = getValidateResponse();
		Part part = partRepository.findByPartNumber(partNumber);
		Date currentDate = new Date();
		if (part == null) {
			validateResponse.setStatus("FAIL");
		} else {
			if (part.getEffectiveFrom().compareTo(currentDate) >= 0
					|| part.getEffectiveTo().compareTo(currentDate) <= 0) {
				validateResponse.setStatus("FAIL");
			}
		}
		return validateResponse;
	}

	@Override
	public ValidateResponse validateSerial(String serialNumber) {

		ValidateResponse validateResponse = getValidateResponse();
		// Part part = validatingDAO.findMaxLengthOfSrial(serialNumber);
		// if(part == null){
		// validateResponse.setStatus("FAIL");
		// }
		return validateResponse;
	}

	@Override
	public List<DefaultSettingDTO> getDefaultSettings(String companyName) {
		List<DefaultSetting> defaultSettings = new ArrayList<DefaultSetting>();
		List<DefaultSettingDTO> settings = new ArrayList<DefaultSettingDTO>();
		Company company = rdtDAO.getCompany(companyName);
		if (company != null) {
			defaultSettings = rdtDAO.getDefaultSettings();
		}
		settings = RDTInfoHelper.convertDefaultSettingModel(defaultSettings);
		return settings;
	}

	@Override
	public PartDTO findPartDetails(String partNumber) {

		PartDTO partDTO = null;
		Part part = partRepository.findByPartNumber(partNumber);

		partDTO = RDTInfoHelper.convertPartModel(part);

		return partDTO;
	}

	@Override
	public ValidateResponse validatePartSerial(String partNumber, String serialReference) {
		ValidateResponse validateResponse = getValidateResponse();
		Part part = partRepository.findByPartNumber(partNumber);
		InventoryMaster inv = rdtDAO.findInvFromPartSerial(part, serialReference);

		if (inv != null) {
			validateResponse.setStatus("FAIL");
		}
		return validateResponse;
	}

	private ValidateResponse getValidateResponse() {
		ValidateResponse validateResponse = new ValidateResponse();
		validateResponse.setStatus("OK");
		return validateResponse;
	}

	@Override
	public ValidateResponse validateRanOrder(String ranOrder) {
		return null;
	}

	@Override
	public ValidateResponse validateVendorCode(String vendorCode) {
		ValidateResponse validateResponse = getValidateResponse();
		Vendor vendor = vendorRepository.findByVendorReferenceCode(vendorCode);
		if (vendor == null) {
			validateResponse.setStatus("FAIL");
		}
		return validateResponse;
	}

	@Override
	public String createVirtualPlt(String transactionTypeId) {
		TransactionType transactionType = transactiontypeRepository.findByTransactionTypeCode(transactionTypeId);
		return rdtDAO.createVirtualPlt(transactionType.getPalletType().getPalletName());
	}

	@Override
	public String createPlt(String pltNumber) {
		Pallet pallet = palletRepository.findByPltNumber(pltNumber);
		if (pallet == null) {
			rdtDAO.createPlt(pltNumber);
		}
		return pltNumber;
	}

	@SuppressWarnings("unused")
	@Override
	public Boolean updateDocument(TransactionDetailRequest txn) {

		Double transactionQuantity = txn.getQuantity();

		ReceiptHeader matchingHeader = null;
		ReceiptBody matchingBody = null;
		ReceiptDetail matchingDetail = null;

		// set part and toLocation
		txn.setPart(partRepository.findByPartNumber(txn.getPartNumber()));
		txn.setToLocation(locationRepository.findByLocationHash(txn.getToLocationHashCode()));

		// set conversion factor
		Double txnQty = setConversionFactor(txn.getQuantity(), txn.getPart().getConversionFactor());
		txn.setQuantity(txnQty);

		DocumentStatus closeStatus = documentStatusRepository.findByDocumentStatusCode("OPEN");

		List<ReceiptHeader> receiptHeaders = new ArrayList<ReceiptHeader>();
		if (txn.getDeliveryNote() == null) {
			receiptHeaders = receiptHeaderRepository.findByHaulierReferenceAndDocumentStatus(txn.getHaulierReference(),
					closeStatus);
		} else {
			ReceiptHeader header = receiptHeaderRepository.findByDeliveryNoteAndHaulierReference(txn.getDeliveryNote(),
					txn.getHaulierReference());
			receiptHeaders.add(header);
		}

		// find matching receipt document
		for (ReceiptHeader receiptHeader : receiptHeaders) {
			List<ReceiptBody> receiptBodies = receiptBodyRepository
					.findByPartNumberAndReceiptHeader(txn.getPartNumber(), receiptHeader);

			for (ReceiptBody receiptBody : receiptBodies) {
				// if(receiptBody.getDifference().compareTo(0.0) < 0){
				matchingBody = receiptBody;
				matchingDetail = receiptDetailRepository.findBySerialReferenceAndPartNumberAndReceiptBody(
						txn.getSerialReference(), txn.getPartNumber(), receiptBody);
				break;
				// }
			}
			matchingHeader = receiptHeader;
			if (matchingBody != null) {
				break;
			}

		}

		// create or update body
		ReceiptBody receiptBody = receivingDAO.saveUpdateReceiptBody(matchingHeader, matchingBody, txn);

		// create or update detail
		ReceiptDetail receiptDetail = receivingDAO.saveUpdateReceiptDetail(matchingHeader, receiptBody, matchingDetail,
				txn);

		// set receipt
		txn.setReceiptHeader(matchingHeader);
		txn.setReceiptBody(receiptBody);
		txn.setReceiptDetail(receiptDetail);

		return true;
	}

	public Double setConversionFactor(Double qty, Integer conversionFactor) {
		return qty * conversionFactor;
	}

	@Override
	public Boolean updateInventory(TransactionDetailRequest txn) {

		InventoryMaster inventory = inventoryMasterRepository
				.findBySerialReferenceAndPartNumber(txn.getSerialReference(), txn.getPartNumber());

		if (inventory == null) {
			inventory = new InventoryMaster();
			inventory.setCreatedBy(txn.getCurrentUser());
			inventory.setDateCreated(new Date());
		} else {
			// set fromLocation
			txn.setFromLocation(inventory.getCurrentLocation());
		}

		inventory.setAllocatedQty(0.0);
		inventory.setDateUpdated(new Date());
		inventory.setUpdatedBy(txn.getCurrentUser());
		inventory.setPart(inventory.getPart() != null ? inventory.getPart() : txn.getPart());
		inventory.setPartNumber(inventory.getPartNumber() != null ? inventory.getPartNumber() : txn.getPartNumber());
		inventory.setInventoryQty(inventory.getInventoryQty() != null ? inventory.getInventoryQty() + txn.getQuantity()
				: txn.getQuantity());
		inventory.setSerialReference(
				inventory.getSerialReference() != null ? inventory.getSerialReference() : txn.getSerialReference());
		inventory.setTagReference(inventory.getTagReference() != null ? inventory.getTagReference() : txn.getToPlt());
		inventory.setRanOrOrder(txn.getRanOrder() != null ? txn.getRanOrder() : "NO-RAN");

		inventory.setCurrentLocation(txn.getToLocation());
		inventory.setLocationCode(txn.getToLocation().getLocationCode());

		InventoryStatus inventoryStatus;
		if (txn.getReceiptDetail().getAdvisedQty() < inventory.getInventoryQty()) {
			inventoryStatus = inventoryStatusRepository.findByInventoryStatusCode("OVRBOOK");
		} else {
			inventoryStatus = inventoryStatusRepository.findByInventoryStatusCode("RCV");
		}
		inventory.setInventoryStatus(inventoryStatus);
		inventory.setVersion((long) 0);
		inventory.setConversionFactor(1);
		inventory.setRecordedMissing(false);
		inventory.setRequiresInspection(txn.getPart().getRequiresInspection());
		inventory.setRequiresDecant(txn.getPart().getRequiresDecant());
		inventory.setStockDate(txn.getLastStockCheckDate());
		inventory.setAvailableQty(inventory.getAvailableQty() != null ? inventory.getAvailableQty() + txn.getQuantity()
				: txn.getQuantity());
		inventory.setProductType(txn.getProductType());

		return inventoryMasterRepository.save(inventory) != null;
	}

	@Override
	public Boolean recordTransaction(TransactionDetailRequest txn) {

		TransactionHistory transactionHistory = new TransactionHistory();

		transactionHistory.setCountQty(txn.getCountQty());
		transactionHistory.setCustomerReference(txn.getReceiptHeader().getCustomerReference());
		if (txn.getFromLocation() != null) {
			transactionHistory.setFromLocationCode(txn.getFromLocation().getLocationCode());
		}
		transactionHistory.setFromPlt(txn.getFromPlt());
		transactionHistory.setPartNumber(txn.getPartNumber());
		transactionHistory.setRanOrOrder(txn.getRanOrder() != null ? txn.getRanOrder() : "NO-RAN");
		transactionHistory.setReasonCode(txn.getReasonCode());
		transactionHistory.setRequiresDecant(txn.getPart().getRequiresDecant());
		transactionHistory.setRequiresInspection(txn.getPart().getRequiresInspection());
		transactionHistory.setTxnQty(txn.getQuantity());
		if (txn.getToLocation() != null) {
			transactionHistory.setToLocationCode(txn.getToLocation().getLocationCode());
		}
		transactionHistory.setSerialReference(txn.getSerialReference());
		transactionHistory.setShortCode(txn.getTransactionTypeId());
		transactionHistory.setTransactionReferenceCode(txn.getTransactionTypeId());
		transactionHistory.setToPlt(txn.getToPlt());
		transactionHistory.setAssociatedDocumentReference(txn.getReceiptHeader().getDocumentReference());
		transactionHistory.setDateCreated(new Date());
		transactionHistory.setCreatedBy(txn.getCurrentUser());
		transactionHistory.setDateUpdated(new Date());
		transactionHistory.setUpdatedBy(txn.getCurrentUser());

		return transactionHistoryRepository.save(transactionHistory) != null;
	}

	@Override
	public Boolean completeReceipts(String haulierReference) {
		Boolean isCompleted = false;
		DocumentStatus status = receivingDAO.findDocumentStatus("COMPLETE");
		List<ReceiptHeader> receiptHeaders = receivingDAO.getAllChildHeaders(haulierReference);
		for (ReceiptHeader receiptHeader : receiptHeaders) {
			isCompleted = rdtDAO.updateRecieptStatus(receiptHeader, status);
		}
		return isCompleted;
	}

	@Override
	public Boolean closeReceipt(String documentReference, String userId) {

		ReceiptHeader receiptHeader = receiptHeaderRepository.findByDocumentReference(documentReference);
		Boolean isClosed = false;
		if (receiptHeader.getDocumentStatus().getDocumentStatusCode().equalsIgnoreCase("COMPLETE")) {

			receiptHeader.setDocumentStatus(documentStatusRepository.findByDocumentStatusCode("TRANSMITTED"));
			receiptHeader.setDateUpdated(new Date());
			receiptHeader.setUpdatedBy(userId);
			isClosed = receiptHeaderRepository.save(receiptHeader) != null;
		}

		return isClosed;
	}

	@Override
	public List<ReceiptBody> findAllPartNumbers(String documentReference) {
		List<ReceiptBody> ReceiptBodies = receiptBodyRepository.findByDocumentReference(documentReference);
		return ReceiptBodies;
	}

	@Override
	public TTRanOrder findOldestRanOrder(String partNumber) {
		List<TTRanOrder> ttRanOrders = ttRanOrderRepository.findByPartNumberAndCompletedOrderByDateCreated(partNumber,
				0);
		if (ttRanOrders.size() == 0) {
			return null;
		}
		return ttRanOrders.get(0);
	}

	@Override
	public List<ReceiptDetail> findReceiptDetails(String documentReference) {
		List<ReceiptDetail> receiptDetails = receiptDetailRepository.findByDocumentReference(documentReference);
		return receiptDetails;
	}

	@Override
	public List<ReceiptHeader> findReceiptHeaders(String haulierReference) {
		List<ReceiptHeader> receiptHeaders = receiptHeaderRepository.findByHaulierReference(haulierReference);
		return receiptHeaders;
	}

	@Override
	public InventoryMaster findInventory(String serialReference, String partNumber) {
		InventoryMaster inventory = inventoryMasterRepository.findBySerialReferenceAndPartNumber(serialReference,
				partNumber);
		return inventory;
	}

	@Override
	public void updateInventoryAndDetail(InventoryMaster inventory, ReceiptDetail receiptDetail,
			String userId) {

		if(!inventory.getInventoryStatus().getInventoryStatusCode().equalsIgnoreCase(ConstantHelper.INVENTORY_STATUS_OVERBOOKING) && !inventory.getInventoryStatus().getInventoryStatusCode().equalsIgnoreCase(ConstantHelper.INVENTORY_STATUS_QA)){
			inventory.setInventoryStatus(inventoryStatusRepository.findByInventoryStatusCode("OK"));
		}
		inventory.setDateUpdated(new Date());
		inventory.setUpdatedBy(userId);
		inventoryMasterRepository.save(inventory);

		receiptDetail.setDateUpdated(new Date());
		receiptDetail.setUpdatedBy(userId);
		receiptDetailRepository.save(receiptDetail);

	}

	@Override
	public Boolean sendReceiptMessage(String documentReference, String userId) throws IOException {
		return messageService.writeCompleteReceiptMessage(documentReference, userId);
		
	}
	// @Override
	// public Boolean closeReceipt(String documentReference) {
	// Boolean isClosed = false;
	// DocumentStatus status = receivingDAO.findDocumentStatus("CLOSED");
	// ReceiptHeader receiptHeader =
	// receiptHeaderRepository.findByDocumentReference(documentReference);
	// List<ReceiptDetail> receiptDetails =
	// receiptDetailRepository.findByDocumentReference(documentReference);
	// for(ReceiptDetail detail:receiptDetails){
	// InventoryMaster inventory =
	// inventoryMasterRepository.findBySerialReferenceAndPartNumber(detail.getSerialReference(),
	// detail.getPartNumber());
	// inventory.setInventoryStatus(inventoryStatusRepository.findByStatus("OK"));
	// inventoryMasterRepository.save(inventory);
	// }
	// if(receiptHeader.getDocumentStatus().getDocumentStatusCode().equals("COMPLETE")){
	// isClosed = rdtDAO.updateRecieptStatus(receiptHeader,status);
	// }
	// return isClosed;
	// }

	// @Override
	// public Boolean closeReceipts(String haulierReference) {
	// Boolean isClosed = false;
	// DocumentStatus status = receivingDAO.findDocumentStatus("CLOSED");
	// List<ReceiptHeader> receiptHeaders =
	// receivingDAO.getAllChildHeaders(haulierReference);
	// for(ReceiptHeader receiptHeader:receiptHeaders){
	// List<ReceiptDetail> receiptDetails =
	// receiptDetailRepository.findByDocumentReference(receiptHeader.getDocumentReference());
	// for(ReceiptDetail detail:receiptDetails){
	// InventoryMaster inventory =
	// inventoryMasterRepository.findBySerialReferenceAndPartNumber(detail.getSerialReference(),
	// detail.getPartNumber());
	// inventory.setInventoryStatus(inventoryStatusRepository.findByStatus("OK"));
	// inventoryMasterRepository.save(inventory);
	// }
	// if(receiptHeader.getDocumentStatus().getDocumentStatusCode().equals("COMPLETE")){
	// isClosed = rdtDAO.updateRecieptStatus(receiptHeader,status);
	// }
	// }
	// return isClosed;
	// }

}
