package com.vantec.receiving.business;

import java.util.List;

import com.vantec.receiving.entity.ReceiptBody;
import com.vantec.receiving.entity.ReceiptDetail;
import com.vantec.receiving.entity.ReceiptHeader;
import com.vantec.receiving.entity.RelocHeader;
import com.vantec.receiving.entity.RtsBody;
import com.vantec.receiving.entity.RtsDetail;
import com.vantec.receiving.entity.RtsHeader;
import com.vantec.receiving.entity.Vendor;
import com.vantec.receiving.model.ReceiptBodyDTO;
import com.vantec.receiving.model.ReceiptHeaderDTO;
import com.vantec.receiving.model.RelocTaskDTO;
import com.vantec.receiving.model.RtsBodyDTO;
import com.vantec.receiving.model.RtsDetailDTO;
import com.vantec.receiving.model.RtsHeaderDTO;

public interface ReceiptInfo {
	
	public ReceiptHeader createReceiptHeader(ReceiptHeaderDTO receiptHeaderDTO);
	public ReceiptBody createReceiptBody(ReceiptHeader receiptHeader, ReceiptBodyDTO receiptBodyDTO);
	public void createDetailandInventoryForOffsiteStorage(ReceiptBody receiptBody, Vendor vendor);
	public void createReceiptDetailsForOdetteLabels(ReceiptHeader header, String user);
	
	public RelocHeader createRelocHeader(RelocTaskDTO relocTaskDTO);
	public RelocHeader createRelocBody(RelocHeader relocHeader);
	public List<String>  processRelocTasks(RelocHeader relocHeader);
	
	
	public RtsHeader createRtsHeader(RtsHeaderDTO rtsHeaderDTO) ;
	public RtsBody createRtsBody(RtsHeader rtsHeader, RtsBodyDTO rtsBodyDTO) ;
	public RtsDetail createRtsDetail(RtsBody rtsBody, RtsDetailDTO rtsDetailDTO) ;
	public void processRTSBodiesAndDetails(RtsHeader header, String user, String partNumber, String rtsType, boolean sendMsg, boolean setQtyZero);
	
	public Boolean checkAnyShortagesForPart(String partNumber) ;
	public Boolean checkOrderExistsForCustomerReferenceAndPart(String orderRef, String partNumber) ;

	public void stockOutInventoryForDeletedReceipt(ReceiptDetail receiptDetail, String grnNumber);
	public void createInventoryAndTransactionHistoryForAutoRTSTasks(RtsHeader rtsHeader, String string);
	public void callAllocateForNewStock(List<String> parts);


}
