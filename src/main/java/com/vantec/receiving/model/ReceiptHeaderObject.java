package com.vantec.receiving.model;

import java.util.List;

public class ReceiptHeaderObject {
	
	private String customerReferenceCode;
	private String lifex;
	private String jisInd;
	private String expectedDeliveryDate;
	private String expectedDeliveryTime;
	private List<ReceiptBodyObject> bodies;
	
	public String getCustomerReferenceCode() {
		return customerReferenceCode;
	}
	public void setCustomerReferenceCode(String customerReferenceCode) {
		this.customerReferenceCode = customerReferenceCode;
	}
	public String getLifex() {
		return lifex;
	}
	public void setLifex(String lifex) {
		this.lifex = lifex;
	}
	public List<ReceiptBodyObject> getBodies() {
		return bodies;
	}
	public void setBodies(List<ReceiptBodyObject> bodies) {
		this.bodies = bodies;
	}
	public String getJisInd() {
		return jisInd;
	}
	public void setJisInd(String jisInd) {
		this.jisInd = jisInd;
	}
	public String getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}
	public void setExpectedDeliveryDate(String expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}
	public String getExpectedDeliveryTime() {
		return expectedDeliveryTime;
	}
	public void setExpectedDeliveryTime(String expectedDeliveryTime) {
		this.expectedDeliveryTime = expectedDeliveryTime;
	}
	
	
    
    
}
