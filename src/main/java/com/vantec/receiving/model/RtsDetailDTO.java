package com.vantec.receiving.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



@JsonIgnoreProperties
public class RtsDetailDTO {
	
	private Long id;
	private String documentReference;
	private RtsBodyDTO rtsBody;
	private int documentStatus;
	private String partNumber;
	private String serialReference;
	private String locationCode;
	private Double receivedQty;
	private Double transactedQty;
	private Date createdAt;
	private String createdBy;
	private Date lastUpdated;
	private String lastUpdatedBy;
	public RtsDetailDTO() {
	}
	
	public Double getTransactedQty() {
		return transactedQty;
	}

	public void setTransactedQty(Double transactedQty) {
		this.transactedQty = transactedQty;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDocumentReference() {
		return documentReference;
	}
	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}
	public RtsBodyDTO getRtsBody() {
		return rtsBody;
	}
	public void setRtsBody(RtsBodyDTO rtsBody) {
		this.rtsBody = rtsBody;
	}
	public int getDocumentStatus() {
		return documentStatus;
	}
	public void setDocumentStatus(int documentStatus) {
		this.documentStatus = documentStatus;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getSerialReference() {
		return serialReference;
	}
	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public Double getReceivedQty() {
		return receivedQty;
	}
	public void setReceivedQty(Double receivedQty) {
		this.receivedQty = receivedQty;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

}
