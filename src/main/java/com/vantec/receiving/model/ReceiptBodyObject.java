package com.vantec.receiving.model;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ReceiptBodyObject {
	
	
	private String partNumber;
	private String storageInfo;
	private Integer expectedQty;
	private Integer lineNo;
	private List<ReceiptDetailObject> details;
	

	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public Integer getExpectedQty() {
		return expectedQty;
	}
	public void setExpectedQty(Integer expectedQty) {
		this.expectedQty = expectedQty;
	}
	public String getStorageInfo() {
		return storageInfo;
	}
	public void setStorageInfo(String storageInfo) {
		this.storageInfo = storageInfo;
	}
	public Integer getLineNo() {
		return lineNo;
	}
	public void setLineNo(Integer lineNo) {
		this.lineNo = lineNo;
	}
	public List<ReceiptDetailObject> getDetails() {
		return details;
	}
	public void setDetails(List<ReceiptDetailObject> details) {
		this.details = details;
	}
	
	
	
	
	

}
