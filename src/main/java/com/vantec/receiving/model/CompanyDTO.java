package com.vantec.receiving.model;

import java.util.Date;

public class CompanyDTO {

	private String name;
	private String shortCode;
	private String address;
	private int qtyDecimalPlace;
	private Boolean virtualPalletLbl;
	private Boolean ipoStorage;
	private int minBoxQty;
	private int maxBoxQty;
	private int minPartNumberLength;
	private int maxPartNumberLength;
	private int minRanNumberLength;
	private int maxRanNumberLength;
	private int minSerialNumberLength;
	private int maxSerialNumberLength;
	private String interfaceInput;
	private String interfaceOutPut;
	private Boolean interfaceActive;
	private Boolean toQA;
	private Boolean toDecant;
	private Date dateCreated;
	private Date dateUpdated;
	private String createdBy;
	private String updatedBy;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getQtyDecimalPlace() {
		return qtyDecimalPlace;
	}

	public void setQtyDecimalPlace(int qtyDecimalPlace) {
		this.qtyDecimalPlace = qtyDecimalPlace;
	}

	public Boolean getVirtualPalletLbl() {
		return virtualPalletLbl;
	}

	public void setVirtualPalletLbl(Boolean virtualPalletLbl) {
		this.virtualPalletLbl = virtualPalletLbl;
	}

	public Boolean getIpoStorage() {
		return ipoStorage;
	}

	public void setIpoStorage(Boolean ipoStorage) {
		this.ipoStorage = ipoStorage;
	}

	public int getMinBoxQty() {
		return minBoxQty;
	}

	public void setMinBoxQty(int minBoxQty) {
		this.minBoxQty = minBoxQty;
	}

	public int getMaxBoxQty() {
		return maxBoxQty;
	}

	public void setMaxBoxQty(int maxBoxQty) {
		this.maxBoxQty = maxBoxQty;
	}

	public int getMinPartNumberLength() {
		return minPartNumberLength;
	}

	public void setMinPartNumberLength(int minPartNumberLength) {
		this.minPartNumberLength = minPartNumberLength;
	}

	public int getMaxPartNumberLength() {
		return maxPartNumberLength;
	}

	public void setMaxPartNumberLength(int maxPartNumberLength) {
		this.maxPartNumberLength = maxPartNumberLength;
	}

	public int getMinRanNumberLength() {
		return minRanNumberLength;
	}

	public void setMinRanNumberLength(int minRanNumberLength) {
		this.minRanNumberLength = minRanNumberLength;
	}

	public int getMaxRanNumberLength() {
		return maxRanNumberLength;
	}

	public void setMaxRanNumberLength(int maxRanNumberLength) {
		this.maxRanNumberLength = maxRanNumberLength;
	}

	public int getMinSerialNumberLength() {
		return minSerialNumberLength;
	}

	public void setMinSerialNumberLength(int minSerialNumberLength) {
		this.minSerialNumberLength = minSerialNumberLength;
	}

	public int getMaxSerialNumberLength() {
		return maxSerialNumberLength;
	}

	public void setMaxSerialNumberLength(int maxSerialNumberLength) {
		this.maxSerialNumberLength = maxSerialNumberLength;
	}

	public String getInterfaceInput() {
		return interfaceInput;
	}

	public void setInterfaceInput(String interfaceInput) {
		this.interfaceInput = interfaceInput;
	}

	public String getInterfaceOutPut() {
		return interfaceOutPut;
	}

	public void setInterfaceOutPut(String interfaceOutPut) {
		this.interfaceOutPut = interfaceOutPut;
	}

	public Boolean getInterfaceActive() {
		return interfaceActive;
	}

	public void setInterfaceActive(Boolean interfaceActive) {
		this.interfaceActive = interfaceActive;
	}

	public Boolean getToQA() {
		return toQA;
	}

	public void setToQA(Boolean toQA) {
		this.toQA = toQA;
	}

	public Boolean getToDecant() {
		return toDecant;
	}

	public void setToDecant(Boolean toDecant) {
		this.toDecant = toDecant;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
