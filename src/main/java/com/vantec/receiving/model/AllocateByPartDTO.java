package com.vantec.receiving.model;

import java.util.ArrayList;
import java.util.List;

public class AllocateByPartDTO {

	private List<String> parts= new ArrayList<String>();
	private String userId;
	public List<String> getParts() {
		return parts;
	}
	public void setParts(List<String> parts) {
		this.parts = parts;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAllParts() {
		if (parts != null)
			return String.join(", ", parts);
		else
			return "";
	}

}
