package com.vantec.receiving.model;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ReceiptHeaderRequest {
	
	private String segnam;
	private String customerReference;
	private String externalRef;
	private String receiptDate;
	private String receiptTime;
	private List<ReceiptBodyRequest> receiptBody;
	
    
	public String getCustomerReference() {
		return customerReference;
	}
	public String getSegnam() {
		return segnam;
	}

	public void setSegnam(String segnam) {
		this.segnam = segnam;
	}

	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}
	public String getExternalRef() {
		return externalRef;
	}
	public void setExternalRef(String externalRef) {
		this.externalRef = externalRef;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getReceiptTime() {
		return receiptTime;
	}
	public void setReceiptTime(String receiptTime) {
		this.receiptTime = receiptTime;
	}
	public List<ReceiptBodyRequest> getReceiptBody() {
		return receiptBody;
	}
	public void setReceiptBody(List<ReceiptBodyRequest> receiptBody) {
		this.receiptBody = receiptBody;
	}
	
	
    
	
	

}
