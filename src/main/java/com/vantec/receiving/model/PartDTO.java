package com.vantec.receiving.model;

import java.util.Date;


public class PartDTO {
	
	private String partNumber;
	private Date effectiveFrom;
	private Date effectiveTo;
	private String partDescription;
	private String vandorPartNumber;
	private String wiCode;
	private String fixedLocationCode;
	private Integer fixedLocationType;
	private Integer fixedZoneType;
	private Boolean countOnRCpt;
	private Boolean decant;
	private Boolean inspect;
	private Integer miniStock;
	private Integer shelfLife;
	private Boolean importantA;
	private String rdtMessage;
	private Integer partFamily;
	private Integer loosePcsEqualTo1;
	private Date dateCreated;
	private Date dateUpdated;
	private String createdBy;
	private String updatedBy;
	
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public Date getEffectiveFrom() {
		return effectiveFrom;
	}
	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}
	public Date getEffectiveTo() {
		return effectiveTo;
	}
	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}
	public String getPartDescription() {
		return partDescription;
	}
	public void setPartDescription(String partDescription) {
		this.partDescription = partDescription;
	}
	public String getVandorPartNumber() {
		return vandorPartNumber;
	}
	public void setVandorPartNumber(String vandorPartNumber) {
		this.vandorPartNumber = vandorPartNumber;
	}
	public String getWiCode() {
		return wiCode;
	}
	public void setWiCode(String wiCode) {
		this.wiCode = wiCode;
	}
	public String getFixedLocationCode() {
		return fixedLocationCode;
	}
	public void setFixedLocationCode(String fixedLocationCode) {
		this.fixedLocationCode = fixedLocationCode;
	}
	public Integer getFixedLocationType() {
		return fixedLocationType;
	}
	public void setFixedLocationType(Integer fixedLocationType) {
		this.fixedLocationType = fixedLocationType;
	}
	public Integer getFixedZoneType() {
		return fixedZoneType;
	}
	public void setFixedZoneType(Integer fixedZoneType) {
		this.fixedZoneType = fixedZoneType;
	}
	public Boolean getCountOnRCpt() {
		return countOnRCpt;
	}
	public void setCountOnRCpt(Boolean countOnRCpt) {
		this.countOnRCpt = countOnRCpt;
	}
	public Boolean getDecant() {
		return decant;
	}
	public void setDecant(Boolean decant) {
		this.decant = decant;
	}
	public Boolean getInspect() {
		return inspect;
	}
	public void setInspect(Boolean inspect) {
		this.inspect = inspect;
	}
	public Integer getMiniStock() {
		return miniStock;
	}
	public void setMiniStock(Integer miniStock) {
		this.miniStock = miniStock;
	}
	public Integer getShelfLife() {
		return shelfLife;
	}
	public void setShelfLife(Integer shelfLife) {
		this.shelfLife = shelfLife;
	}
	public Boolean getImportantA() {
		return importantA;
	}
	public void setImportantA(Boolean importantA) {
		this.importantA = importantA;
	}
	public String getRdtMessage() {
		return rdtMessage;
	}
	public void setRdtMessage(String rdtMessage) {
		this.rdtMessage = rdtMessage;
	}
	public Integer getPartFamily() {
		return partFamily;
	}
	public void setPartFamily(Integer partFamily) {
		this.partFamily = partFamily;
	}
	public Integer getLoosePcsEqualTo1() {
		return loosePcsEqualTo1;
	}
	public void setLoosePcsEqualTo1(Integer loosePcsEqualTo1) {
		this.loosePcsEqualTo1 = loosePcsEqualTo1;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Date getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	

}

