package com.vantec.receiving.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class ChangePnoDTO {

	private String grnReference;
	private Integer lineNo;
	private Integer newLineNo;
	private String partNumber;
	private String newPartNumber;
	public String getGrnReference() {
		return grnReference;
	}
	public void setGrnReference(String grnReference) {
		this.grnReference = grnReference;
	}
	public Integer getLineNo() {
		return lineNo;
	}
	public void setLineNo(Integer lineNo) {
		this.lineNo = lineNo;
	}
	public Integer getNewLineNo() {
		return newLineNo;
	}
	public void setNewLineNo(Integer newLineNo) {
		this.newLineNo = newLineNo;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getNewPartNumber() {
		return newPartNumber;
	}
	public void setNewPartNumber(String newPartNumber) {
		this.newPartNumber = newPartNumber;
	}
	
	
}
