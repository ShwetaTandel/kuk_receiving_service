package com.vantec.receiving.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class UnplannedRTSDTO {

	private Long id;
	private String partNumber;
	private Double pieceQty;
	private Double noOfBoxes;
	private String reasonCode;
	private Boolean notifyKUK;
	private String user;
	private String orderReference;
	
	public String getOrderReference() {
		return orderReference;
	}
	public void setOrderReference(String orderReference) {
		this.orderReference = orderReference;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public Double getPieceQty() {
		return pieceQty;
	}
	public void setPieceQty(Double pieceQty) {
		this.pieceQty = pieceQty;
	}
	public Double getNoOfBoxes() {
		return noOfBoxes;
	}
	public void setNoOfBoxes(Double noOfBoxes) {
		this.noOfBoxes = noOfBoxes;
	}
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	public Boolean getNotifyKUK() {
		return notifyKUK;
	}
	public void setNotifyKUK(Boolean notifyKUK) {
		this.notifyKUK = notifyKUK;
	}

}
