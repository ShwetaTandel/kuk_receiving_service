package com.vantec.receiving.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties
public class RtsBodyDTO  {

	private Long id;
	private String documentReference;
	private RtsHeaderDTO rtsHeader;
	private String partNumber;
	private String baanSerial;
	private String baanLocation;
	private Double qtyReceived;
	private int documentStatus;
	private Date dateTime;
	private Date createdAt;
	private String createdBy;
	private Date lastUpdated;
	private String lastUpdatedBy;
	private List<RtsDetailDTO> rtsDetails = new ArrayList<RtsDetailDTO>();
	public RtsBodyDTO() {
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDocumentReference() {
		return documentReference;
	}
	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}
	public RtsHeaderDTO getRtsHeader() {
		return rtsHeader;
	}
	public void setRtsHeader(RtsHeaderDTO rtsHeader) {
		this.rtsHeader = rtsHeader;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getBaanSerial() {
		return baanSerial;
	}
	public void setBaanSerial(String baanSerial) {
		this.baanSerial = baanSerial;
	}
	public String getBaanLocation() {
		return baanLocation;
	}
	public void setBaanLocation(String baanLocation) {
		this.baanLocation = baanLocation;
	}
	public Double getQtyReceived() {
		return qtyReceived;
	}
	public void setQtyReceived(Double qtyReceived) {
		this.qtyReceived = qtyReceived;
	}
	public int getDocumentStatus() {
		return documentStatus;
	}
	public void setDocumentStatus(int documentStatus) {
		this.documentStatus = documentStatus;
	}
	public Date getDateTime() {
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public List<RtsDetailDTO> getRtsDetails() {
		return rtsDetails;
	}
	public void setRtsDetails(List<RtsDetailDTO> rtsDetails) {
		this.rtsDetails = rtsDetails;
	}


    
}
