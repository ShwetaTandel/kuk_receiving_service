package com.vantec.receiving.model;

public class CloseDTO {
	
	
	private String documentReference;
	private String userId;
	
	private String haulierReference;
	
	public String getDocumentReference() {
		return documentReference;
	}
	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getHaulierReference() {
		return haulierReference;
	}
	public void setHaulierReference(String haulierReference) {
		this.haulierReference = haulierReference;
	}
	
	
	
	
}
