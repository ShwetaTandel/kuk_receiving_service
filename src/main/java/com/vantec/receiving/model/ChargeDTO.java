package com.vantec.receiving.model;

public class ChargeDTO {
	
	
	private String chargeName;
	private String chargeType;
	private Double stdCharge;
	private String createdBy;
	private String updatedBy;
	
	public String getChargeName() {
		return chargeName;
	}
	public void setChargeName(String chargeName) {
		this.chargeName = chargeName;
	}
	public String getChargeType() {
		return chargeType;
	}
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}
	public Double getStdCharge() {
		return stdCharge;
	}
	public void setStdCharge(Double stdCharge) {
		this.stdCharge = stdCharge;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
