package com.vantec.receiving.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class ReceiptHeaderDTO {
	
	private String grnNumber;
	private String containerId;
	private String adviceNote;
	private String grnType; 
	
	private List<String> vendors = new ArrayList<String>();
	
	private List<ReceiptBodyDTO> receiptBodyDTOs;
	
	public String getGrnNumber() {
		return grnNumber;
	}
	public void setGrnNumber(String grnNumber) {
		this.grnNumber = grnNumber;
	}
	public String getContainerId() {
		return containerId;
	}
	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}
	public List<ReceiptBodyDTO> getReceiptBodyDTO() {
		return receiptBodyDTOs;
	}
	public void setReceiptBodyDTO(List<ReceiptBodyDTO> receiptBodyDTOs) {
		this.receiptBodyDTOs = receiptBodyDTOs;
	}
	public String getAdviceNote() {
		return adviceNote;
	}
	public void setAdviceNote(String adviceNote) {
		this.adviceNote = adviceNote;
	}
	public String getGrnType() {
		return grnType;
	}
	public void setGrnType(String grnType) {
		this.grnType = grnType;
	}
	public List<String> getVendors() {
		return vendors;
	}
	public void setVendors(List<String> vendors) {
		this.vendors = vendors;
	}
	
	
	
	
	
	
}
