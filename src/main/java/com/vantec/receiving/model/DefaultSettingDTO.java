package com.vantec.receiving.model;

public class DefaultSettingDTO {

	
	private String fieldName;
	private String prefixName;
	private Integer min;
	private Integer max;
	private String systemGenerated;
	private String isVirtualPltLbl;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	

	public String getPrefixName() {
		return prefixName;
	}

	public void setPrefixName(String prefixName) {
		this.prefixName = prefixName;
	}

	public Integer getMin() {
		return min;
	}

	public void setMin(Integer min) {
		this.min = min;
	}

	public Integer getMax() {
		return max;
	}

	public void setMax(Integer max) {
		this.max = max;
	}

	public String getSystemGenerated() {
		return systemGenerated;
	}

	public void setSystemGenerated(String systemGenerated) {
		this.systemGenerated = systemGenerated;
	}

	public String getIsVirtualPltLbl() {
		return isVirtualPltLbl;
	}

	public void setIsVirtualPltLbl(String isVirtualPltLbl) {
		this.isVirtualPltLbl = isVirtualPltLbl;
	}
	
	
	
	
	
	
	

}
