package com.vantec.receiving.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vantec.receiving.entity.ProductType;


@JsonIgnoreProperties
public class ReceiptDetailDTO {
	
	private String serialReference;
	private String documentReference;
	private String ranOrder;
	private Double advisedQty;
	private Double receivedQty;
	private Double differnce;
	private String createdBy;
	private String lastUpdatedBy;
	private Long partId;
	private Long productTypeId;
	private Long receiptBodyId;
	ProductType productType;
	
	public String getSerialReference() {
		return serialReference;
	}
	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}
	public String getDocumentReference() {
		return documentReference;
	}
	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}
	public String getRanOrder() {
		return ranOrder;
	}
	public void setRanOrder(String ranOrder) {
		this.ranOrder = ranOrder;
	}
	public Double getAdvisedQty() {
		return advisedQty;
	}
	public void setAdvisedQty(Double advisedQty) {
		this.advisedQty = advisedQty;
	}
	public Double getReceivedQty() {
		return receivedQty;
	}
	public void setReceivedQty(Double receivedQty) {
		this.receivedQty = receivedQty;
	}
	public Double getDiffernce() {
		return differnce;
	}
	public void setDiffernce(Double differnce) {
		this.differnce = differnce;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Long getPartId() {
		return partId;
	}
	public void setPartId(Long partId) {
		this.partId = partId;
	}
	public Long getProductTypeId() {
		return productTypeId;
	}
	public void setProductTypeId(Long productTypeId) {
		this.productTypeId = productTypeId;
	}
	public Long getReceiptBodyId() {
		return receiptBodyId;
	}
	public void setReceiptBodyId(Long receiptBodyId) {
		this.receiptBodyId = receiptBodyId;
	}
	public ProductType getProductType() {
		return productType;
	}
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	
	
	
	
	
}
