package com.vantec.receiving.model;

import java.util.Map;

public class ReceiptDetails {
	
	private String indicator;
	private Map<String, String> details;
	
	
	public ReceiptDetails(String indicator, Map<String, String> details) {
		super();
		this.indicator = indicator;
		this.details = details;
	}
	
	public String getIndicator() {
		return indicator;
	}
	public void setIndicator(String header) {
		this.indicator = header;
	}
	public Map<String, String> getDetails() {
		return details;
	}
	public void setDetails(Map<String, String> details) {
		this.details = details;
	}
	

}
