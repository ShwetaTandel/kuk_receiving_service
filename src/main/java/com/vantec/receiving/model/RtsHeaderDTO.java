package com.vantec.receiving.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class RtsHeaderDTO {

	private Long id;
	private String orderReference;
	private String origin;
	private String orderType;
	private String position;
	private String baanLocation;
	private String machineModel;
	private String modelVariant;
	private Boolean isProcessed;
	private String reasonCode;
	private Date expectedDeliveryDate;
	private Integer documentStatus;
	private Date dateTime;
	private Date createdAt;
	private String createdBy;
	private Date lastUpdated;
	private String lastUpdatedBy;
	private boolean isFABRTS;
	private List<String> parts = new ArrayList<String>();
	
	
	public List<String> getParts() {
		return parts;
	}

	public void setParts(List<String> parts) {
		this.parts = parts;
	}

	public boolean isFABRTS() {
		return isFABRTS;
	}

	public void setFABRTS(boolean isFABRTS) {
		this.isFABRTS = isFABRTS;
	}
	private List<RtsBodyDTO> rtsBodies = new ArrayList<RtsBodyDTO>();
	public RtsHeaderDTO() {
	}
	
	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOrderReference() {
		return orderReference;
	}
	public void setOrderReference(String orderReference) {
		this.orderReference = orderReference;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getBaanLocation() {
		return baanLocation;
	}
	public void setBaanLocation(String baanLocation) {
		this.baanLocation = baanLocation;
	}
	public String getMachineModel() {
		return machineModel;
	}
	public void setMachineModel(String machineModel) {
		this.machineModel = machineModel;
	}
	public String getModelVariant() {
		return modelVariant;
	}
	public void setModelVariant(String modelVariant) {
		this.modelVariant = modelVariant;
	}
	public Boolean getIsProcessed() {
		return isProcessed;
	}
	public void setIsProcessed(Boolean isProcessed) {
		this.isProcessed = isProcessed;
	}
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	public Date getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}
	public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}
	public Integer getDocumentStatus() {
		return documentStatus;
	}
	public void setDocumentStatus(Integer documentStatus) {
		this.documentStatus = documentStatus;
	}
	public Date getDateTime() {
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public List<RtsBodyDTO> getRtsBodies() {
		return rtsBodies;
	}
	public void setRtsBodies(List<RtsBodyDTO> rtsBodies) {
		this.rtsBodies = rtsBodies;
	}

}
