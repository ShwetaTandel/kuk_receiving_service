package com.vantec.receiving.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class ReceiptBodyRequest {
	
	private String segnam;
	private String sequence;
	private String partNumber;
	private Double advisedQty;
	private String storage;
	private String jisInd;
	
	//Receipt Detail request
	private String serialReference;
	
    
	public String getSegnam() {
		return segnam;
	}
	public void setSegnam(String segnam) {
		this.segnam = segnam;
	}
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public Double getAdvisedQty() {
		return advisedQty;
	}
	public void setAdvisedQty(Double advisedQty) {
		this.advisedQty = advisedQty;
	}
	public String getStorage() {
		return storage;
	}
	public void setStorage(String storage) {
		this.storage = storage;
	}
	public String getJisInd() {
		return jisInd;
	}
	public void setJisInd(String jisInd) {
		this.jisInd = jisInd;
	}
	public String getSerialReference() {
		return serialReference;
	}
	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}
    

}
