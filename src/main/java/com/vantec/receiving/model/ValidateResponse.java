package com.vantec.receiving.model;

public class ValidateResponse {
	
	String status ;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
