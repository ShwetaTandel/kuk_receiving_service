package com.vantec.receiving.model;

import java.util.List;

public class HaulierReferenceDTO {
	
	
	List<String> documentReferences;
	
	String haulierReference;

	public List<String> getDocumentReferences() {
		return documentReferences;
	}

	public void setDocumentReference(List<String> documentReferences) {
		this.documentReferences = documentReferences;
	}

	public String getHaulierReference() {
		return haulierReference;
	}

	public void setHaulierReference(String haulierReference) {
		this.haulierReference = haulierReference;
	}

	public void setDocumentReferences(List<String> documentReferences) {
		this.documentReferences = documentReferences;
	}

	
	
	

}
