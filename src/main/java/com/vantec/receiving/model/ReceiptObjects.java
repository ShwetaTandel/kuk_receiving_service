package com.vantec.receiving.model;

import java.util.List;

public class ReceiptObjects {
	private String header;
	private List<ReceiptDetails> details;
	
	
	
	
	public ReceiptObjects(String header, List<ReceiptDetails> details) {
		super();
		this.header = header;
		this.details = details;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
}
