package com.vantec.receiving.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vantec.receiving.entity.Location;
import com.vantec.receiving.entity.Part;
import com.vantec.receiving.entity.ProductType;
import com.vantec.receiving.entity.ReceiptBody;
import com.vantec.receiving.entity.ReceiptDetail;
import com.vantec.receiving.entity.ReceiptHeader;

@JsonIgnoreProperties
public class TransactionDetailRequest {
	
	//parameter from gun
	String haulierReference;
	Double quantity;
	String partNumber;
	String serialReference;
	String currentUser;
	String deliveryNote;
	
	String toPlt;
	String fromPlt;
	String ranOrder;
	Date lastStockCheckDate;
	String toLocationHashCode;
	Double countQty;
	String reasonCode;
	String transactionTypeId;
	ProductType productType;
	
	
	//parameter from backend
	ReceiptDetail receiptDetail;
	ReceiptBody receiptBody;
	ReceiptHeader receiptHeader;
	Part part;
	Location toLocation;
	Location fromLocation;
	
	
	
	

	public String getHaulierReference() {
		return haulierReference;
	}
	public void setHaulierReference(String haulierReference) {
		this.haulierReference = haulierReference;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getSerialReference() {
		return serialReference;
	}
	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}
	public String getCurrentUser() {
		return currentUser;
	}
	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}
	public String getToPlt() {
		return toPlt;
	}
	public void setToPlt(String toPlt) {
		this.toPlt = toPlt;
	}
	public String getFromPlt() {
		return fromPlt;
	}
	public void setFromPlt(String fromPlt) {
		this.fromPlt = fromPlt;
	}
	public String getRanOrder() {
		return ranOrder;
	}
	public void setRanOrder(String ranOrder) {
		this.ranOrder = ranOrder;
	}
	public Date getLastStockCheckDate() {
		return lastStockCheckDate;
	}
	public void setLastStockCheckDate(Date lastStockCheckDate) {
		this.lastStockCheckDate = lastStockCheckDate;
	}
	public String getToLocationHashCode() {
		return toLocationHashCode;
	}
	public void setToLocationHashCode(String toLocationHashCode) {
		this.toLocationHashCode = toLocationHashCode;
	}
	public Double getCountQty() {
		return countQty;
	}
	public void setCountQty(Double countQty) {
		this.countQty = countQty;
	}
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	public String getTransactionTypeId() {
		return transactionTypeId;
	}
	public void setTransactionTypeId(String transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}
	public ProductType getProductType() {
		return productType;
	}
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	public ReceiptDetail getReceiptDetail() {
		return receiptDetail;
	}
	public void setReceiptDetail(ReceiptDetail receiptDetail) {
		this.receiptDetail = receiptDetail;
	}
	public ReceiptBody getReceiptBody() {
		return receiptBody;
	}
	public void setReceiptBody(ReceiptBody receiptBody) {
		this.receiptBody = receiptBody;
	}
	public ReceiptHeader getReceiptHeader() {
		return receiptHeader;
	}
	public void setReceiptHeader(ReceiptHeader receiptHeader) {
		this.receiptHeader = receiptHeader;
	}
	public Part getPart() {
		return part;
	}
	public void setPart(Part part) {
		this.part = part;
	}
	public Location getToLocation() {
		return toLocation;
	}
	public void setToLocation(Location toLocation) {
		this.toLocation = toLocation;
	}
	public Location getFromLocation() {
		return fromLocation;
	}
	public void setFromLocation(Location fromLocation) {
		this.fromLocation = fromLocation;
	}
	public String getDeliveryNote() {
		return deliveryNote;
	}
	public void setDeliveryNote(String deliveryNote) {
		this.deliveryNote = deliveryNote;
	}
	
}
