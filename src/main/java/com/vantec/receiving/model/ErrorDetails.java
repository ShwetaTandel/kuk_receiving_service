package com.vantec.receiving.model;

import java.util.Date;

public class ErrorDetails {
	private Date date;
	private String errorMessage;
	
	public ErrorDetails(Date date, String errorMessage) {
		super();
		this.date = date;
		this.errorMessage = errorMessage;
	}
	public Date getDate() {
		return date;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
}
