package com.vantec.receiving.model;

public class DeleteDocumentDetailRequest {
	
	String documentReference;
	String partNumber;
	String serialReference;
	String currentUser;

	public String getDocumentReference() {
		return documentReference;
	}

	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getSerialReference() {
		return serialReference;
	}

	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}

	public String getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}
	

}
