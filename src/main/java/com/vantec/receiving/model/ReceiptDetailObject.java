package com.vantec.receiving.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ReceiptDetailObject {
	
	private String serialReference;
	private Double expectedQty;
	

	public String getSerialReference() {
		return serialReference;
	}
	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}
	public Double getExpectedQty() {
		return expectedQty;
	}
	public void setExpectedQty(Double expectedQty) {
		this.expectedQty = expectedQty;
	}
	

}
