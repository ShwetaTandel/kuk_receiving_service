package com.vantec.receiving.model;

import com.vantec.receiving.entity.ChargeMaster;


public class VendorDTO {
	

	private String vendorName;
	private String vendorCode;
	private String effectiveFrom;
	private String effectiveTo;
	private String contactName;
	private String contactTel;
	private String contactEmail;
	private String street;
	private String town;
	private String country;
	private String postCode;
	private Boolean autoReceive;
	private Boolean autoPick;
	private Boolean decant;
	private Boolean inspect;
	private Integer vatPercentage;
	private Integer vatNumber;
	private String storageChargeDay;
	private Long storageChargeId;
	private Long receiptChargeId;
	private Long despatchChargeId;
	private Long decantChargeId;
	private Long inspectChargeId;
	private Long transportChargeId;
	private String reportChargeIds;
	private String createdBy;
	private String dateUpdated;
	private String updatedBy;
	
	private ChargeMaster storageCharge;
	private ChargeMaster receiptCharge;
	private ChargeMaster despatchCharge;
	private ChargeMaster decantCharge;
	private ChargeMaster inspectCharge;
	private ChargeMaster transportCharge;
	
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	public String getEffectiveFrom() {
		return effectiveFrom;
	}
	public void setEffectiveFrom(String effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}
	public String getEffectiveTo() {
		return effectiveTo;
	}
	public void setEffectiveTo(String effectiveTo) {
		this.effectiveTo = effectiveTo;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactTel() {
		return contactTel;
	}
	public void setContactTel(String contactTel) {
		this.contactTel = contactTel;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public Boolean getAutoReceive() {
		return autoReceive;
	}
	public void setAutoReceive(Boolean autoReceive) {
		this.autoReceive = autoReceive;
	}
	public Boolean getAutoPick() {
		return autoPick;
	}
	public void setAutoPick(Boolean autoPick) {
		this.autoPick = autoPick;
	}
	public Boolean getDecant() {
		return decant;
	}
	public void setDecant(Boolean decant) {
		this.decant = decant;
	}
    
	public Boolean getInspect() {
		return inspect;
	}
	public void setInspect(Boolean inspect) {
		this.inspect = inspect;
	}
	public Integer getVatPercentage() {
		return vatPercentage;
	}
	public void setVatPercentage(Integer vatPercentage) {
		this.vatPercentage = vatPercentage;
	}
	public Integer getVatNumber() {
		return vatNumber;
	}
	public void setVatNumber(Integer vatNumber) {
		this.vatNumber = vatNumber;
	}
	public String getStorageChargeDay() {
		return storageChargeDay;
	}
	public void setStorageChargeDay(String storageChargeDay) {
		this.storageChargeDay = storageChargeDay;
	}
	public Long getStorageChargeId() {
		return storageChargeId;
	}
	public void setStorageChargeId(Long storageChargeId) {
		this.storageChargeId = storageChargeId;
	}
	public Long getReceiptChargeId() {
		return receiptChargeId;
	}
	public void setReceiptChargeId(Long receiptChargeId) {
		this.receiptChargeId = receiptChargeId;
	}
	public Long getDespatchChargeId() {
		return despatchChargeId;
	}
	public void setDespatchChargeId(Long despatchChargeId) {
		this.despatchChargeId = despatchChargeId;
	}
	public Long getDecantChargeId() {
		return decantChargeId;
	}
	public void setDecantChargeId(Long decantChargeId) {
		this.decantChargeId = decantChargeId;
	}
	public Long getInspectChargeId() {
		return inspectChargeId;
	}
	public void setInspectChargeId(Long inspectChargeId) {
		this.inspectChargeId = inspectChargeId;
	}
	public Long getTransportChargeId() {
		return transportChargeId;
	}
	public void setTransportChargeId(Long transportChargeId) {
		this.transportChargeId = transportChargeId;
	}
	public String getReportChargeIds() {
		return reportChargeIds;
	}
	public void setReportChargeIds(String reportChargeIds) {
		this.reportChargeIds = reportChargeIds;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(String dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	public ChargeMaster getStorageCharge() {
		return storageCharge;
	}
	public void setStorageCharge(ChargeMaster storageCharge) {
		this.storageCharge = storageCharge;
	}
	public ChargeMaster getReceiptCharge() {
		return receiptCharge;
	}
	public void setReceiptCharge(ChargeMaster receiptCharge) {
		this.receiptCharge = receiptCharge;
	}
	public ChargeMaster getDespatchCharge() {
		return despatchCharge;
	}
	public void setDespatchCharge(ChargeMaster despatchCharge) {
		this.despatchCharge = despatchCharge;
	}
	public ChargeMaster getDecantCharge() {
		return decantCharge;
	}
	public void setDecantCharge(ChargeMaster decantCharge) {
		this.decantCharge = decantCharge;
	}
	public ChargeMaster getInspectCharge() {
		return inspectCharge;
	}
	public void setInspectCharge(ChargeMaster inspectCharge) {
		this.inspectCharge = inspectCharge;
	}
	public ChargeMaster getTransportCharge() {
		return transportCharge;
	}
	public void setTransportCharge(ChargeMaster transportCharge) {
		this.transportCharge = transportCharge;
	}
	
	
	
	
	
}
