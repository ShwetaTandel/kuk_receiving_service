package com.vantec.receiving.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class ReceiptDTO {
	
	private String grnNumber;
	private String containerId;
	private String sequenceNumber;
	private String adviceNote;
	private String partNumber;
	private String quantityAdvised;
	private String shippingReference;
	private String caseReference;
	private String supplierCode;
	private String orderNumber;
	private String position;
	private String qtyAccepted;
	private String qtyToQuarantine;
	private String inspectFlag;
	private String requiredCountFlag;
	private String newItems;
	
	public String getGrnNumber() {
		return grnNumber;
	}
	public void setGrnNumber(String grnNumber) {
		this.grnNumber = grnNumber;
	}
	public String getContainerId() {
		return containerId;
	}
	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getAdviceNote() {
		return adviceNote;
	}
	public void setAdviceNote(String adviceNote) {
		this.adviceNote = adviceNote;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getQuantityAdvised() {
		return quantityAdvised;
	}
	public void setQuantityAdvised(String quantityAdvised) {
		this.quantityAdvised = quantityAdvised;
	}
	public String getShippingReference() {
		return shippingReference;
	}
	public void setShippingReference(String shippingReference) {
		this.shippingReference = shippingReference;
	}
	public String getCaseReference() {
		return caseReference;
	}
	public void setCaseReference(String caseReference) {
		this.caseReference = caseReference;
	}
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getQtyAccepted() {
		return qtyAccepted;
	}
	public void setQtyAccepted(String qtyAccepted) {
		this.qtyAccepted = qtyAccepted;
	}
	public String getQtyToQuarantine() {
		return qtyToQuarantine;
	}
	public void setQtyToQuarantine(String qtyToQuarantine) {
		this.qtyToQuarantine = qtyToQuarantine;
	}
	public String getInspectFlag() {
		return inspectFlag;
	}
	public void setInspectFlag(String inspectFlag) {
		this.inspectFlag = inspectFlag;
	}
	public String getRequiredCountFlag() {
		return requiredCountFlag;
	}
	public void setRequiredCountFlag(String requiredCountFlag) {
		this.requiredCountFlag = requiredCountFlag;
	}
	public String getNewItems() {
		return newItems;
	}
	public void setNewItems(String newItems) {
		this.newItems = newItems;
	}
	
	
	
	
	
	
	
	

	
	
}
